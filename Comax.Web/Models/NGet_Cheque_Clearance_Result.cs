
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Comax.Web.Models
{

using System;
    
public partial class NGet_Cheque_Clearance_Result
{

    public Nullable<System.DateTime> PaymentDate { get; set; }

    public Nullable<decimal> Amount { get; set; }

    public string ChequeNo { get; set; }

    public string ChequeDate { get; set; }

    public string BankName { get; set; }

    public Nullable<long> TransactionID { get; set; }

    public string BankClearanceDate { get; set; }

    public Nullable<decimal> BankCharges { get; set; }

    public string PaymentStatus { get; set; }

}

}
