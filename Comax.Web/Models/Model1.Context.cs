﻿

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Comax.Web.Models
{

using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

using System.Data.Entity.Core.Objects;
using System.Linq;


public partial class dbSBPEntities2 : DbContext
{
    public dbSBPEntities2()
        : base("name=dbSBPEntities2")
    {

    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        throw new UnintentionalCodeFirstException();
    }


    public virtual DbSet<tblsPaymentRecord> tblsPaymentRecords { get; set; }

    public virtual DbSet<tblSPayment> tblSPayments { get; set; }

    public virtual DbSet<tblSFlat> tblSFlats { get; set; }

    public virtual DbSet<tblSProperty> tblSProperties { get; set; }

    public virtual DbSet<tblSPropertyType> tblSPropertyTypes { get; set; }

    public virtual DbSet<tblSPropertySize> tblSPropertySizes { get; set; }

    public virtual DbSet<PlanType> PlanTypes { get; set; }

    public virtual DbSet<tblSSaleFlat> tblSSaleFlats { get; set; }

    public virtual DbSet<Error> Errors { get; set; }

    public virtual DbSet<tblSPaymentCancel> tblSPaymentCancels { get; set; }

    public virtual DbSet<tblSInstallmentDetail> tblSInstallmentDetails { get; set; }

    public virtual DbSet<tblSPaymentOther> tblSPaymentOthers { get; set; }

    public virtual DbSet<Customer> Customers { get; set; }

    public virtual DbSet<tblSPaymentOtherCancel> tblSPaymentOtherCancels { get; set; }

    public virtual DbSet<PaymentMaster> PaymentMasters { get; set; }

    public virtual DbSet<LatePayment> LatePayments { get; set; }

    public virtual DbSet<PropertyTransfer> PropertyTransfers { get; set; }

    public virtual DbSet<AssuredReturnPayment> AssuredReturnPayments { get; set; }

    public virtual DbSet<AssuredReturn> AssuredReturns { get; set; }

    public virtual DbSet<RefundProperty> RefundProperties { get; set; }

    public virtual DbSet<tblBroker> tblBrokers { get; set; }

    public virtual DbSet<Agreement> Agreements { get; set; }

    public virtual DbSet<BrokerToProperty> BrokerToProperties { get; set; }

    public virtual DbSet<BrokerPayment> BrokerPayments { get; set; }

    public virtual DbSet<ModuleList> ModuleLists { get; set; }

    public virtual DbSet<UserAccess> UserAccesses { get; set; }

    public virtual DbSet<UserProperty> UserProperties { get; set; }

    public virtual DbSet<RoleAccess> RoleAccesses { get; set; }

    public virtual DbSet<PropertyRemark> PropertyRemarks { get; set; }

    public virtual DbSet<ReminderLetter> ReminderLetters { get; set; }


    public virtual int Update_Payment(Nullable<int> saleID, Nullable<int> transactionID, Nullable<System.DateTime> paymentDate, Nullable<decimal> amount, string paymentMode, string chequeNo, Nullable<System.DateTime> chequeDate, string bankName, string paymentStatus, string remarks, string bankBranch, string amtRcvdinwords, string userName, string activity)
    {

        var saleIDParameter = saleID.HasValue ?
            new ObjectParameter("SaleID", saleID) :
            new ObjectParameter("SaleID", typeof(int));


        var transactionIDParameter = transactionID.HasValue ?
            new ObjectParameter("TransactionID", transactionID) :
            new ObjectParameter("TransactionID", typeof(int));


        var paymentDateParameter = paymentDate.HasValue ?
            new ObjectParameter("PaymentDate", paymentDate) :
            new ObjectParameter("PaymentDate", typeof(System.DateTime));


        var amountParameter = amount.HasValue ?
            new ObjectParameter("Amount", amount) :
            new ObjectParameter("Amount", typeof(decimal));


        var paymentModeParameter = paymentMode != null ?
            new ObjectParameter("PaymentMode", paymentMode) :
            new ObjectParameter("PaymentMode", typeof(string));


        var chequeNoParameter = chequeNo != null ?
            new ObjectParameter("ChequeNo", chequeNo) :
            new ObjectParameter("ChequeNo", typeof(string));


        var chequeDateParameter = chequeDate.HasValue ?
            new ObjectParameter("ChequeDate", chequeDate) :
            new ObjectParameter("ChequeDate", typeof(System.DateTime));


        var bankNameParameter = bankName != null ?
            new ObjectParameter("BankName", bankName) :
            new ObjectParameter("BankName", typeof(string));


        var paymentStatusParameter = paymentStatus != null ?
            new ObjectParameter("PaymentStatus", paymentStatus) :
            new ObjectParameter("PaymentStatus", typeof(string));


        var remarksParameter = remarks != null ?
            new ObjectParameter("Remarks", remarks) :
            new ObjectParameter("Remarks", typeof(string));


        var bankBranchParameter = bankBranch != null ?
            new ObjectParameter("BankBranch", bankBranch) :
            new ObjectParameter("BankBranch", typeof(string));


        var amtRcvdinwordsParameter = amtRcvdinwords != null ?
            new ObjectParameter("AmtRcvdinwords", amtRcvdinwords) :
            new ObjectParameter("AmtRcvdinwords", typeof(string));


        var userNameParameter = userName != null ?
            new ObjectParameter("UserName", userName) :
            new ObjectParameter("UserName", typeof(string));


        var activityParameter = activity != null ?
            new ObjectParameter("Activity", activity) :
            new ObjectParameter("Activity", typeof(string));


        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Update_Payment", saleIDParameter, transactionIDParameter, paymentDateParameter, amountParameter, paymentModeParameter, chequeNoParameter, chequeDateParameter, bankNameParameter, paymentStatusParameter, remarksParameter, bankBranchParameter, amtRcvdinwordsParameter, userNameParameter, activityParameter);
    }


    public virtual int INSERT_RBookTransferFlat(string flatID, Nullable<int> saleID, Nullable<System.DateTime> saleDate, Nullable<decimal> bookingAmount, string fName, string mName, string lName, string title, string pName, string address1, string address2, string city, string distt, string state, string country, string pAN, string mobileNo, Nullable<System.DateTime> dateOfBirth, string coFName, string coMName, string coLName, string coAddress1, string coAddress2, string coCity, string coState, string coCountry, string coPAN, string coMobileNo, string alternateMobile, string landLine, string emailID, string alternateEmail, Nullable<decimal> loanAmount, string lienField, Nullable<int> bankID, Nullable<bool> isPAN, Nullable<bool> isPhoto, Nullable<bool> isAddressPf, Nullable<int> type, Nullable<long> customerID, string secCoFName, string secCoMName, string secCoLName, string secCoAddress1, string secCoAddress2, string secCoCity, string secCoState, string secCoCountry, string secCoMobileNo, string secCoPAN, string coTitle, string coPName, string secCoTitle, string secCoPName, Nullable<int> isConstruction, string appTitle, string coAppTitle, string secCoAppTitle, Nullable<bool> isRationCard, Nullable<bool> isDrivingLicence, Nullable<bool> isVoterCard, Nullable<bool> isPassPort, string photoImagePath, string addPfImagePath, string dVImagePath, string voterCardImagePath, string passportImagePath, string rationCardImagePath, string remarks, Nullable<System.DateTime> coDOB, Nullable<System.DateTime> secCoDOB, string pANImagePath, Nullable<bool> coIsPAN, Nullable<bool> coIsPhoto, Nullable<bool> coIsAddressPf, Nullable<bool> coIsRationCard, Nullable<bool> coIsDrivingLicence, Nullable<bool> coIsVoterCard, Nullable<bool> coIsPassPort, string coPhotoImagePath, string coAddPfImagePath, string coDVImagePath, string coVoterCardImagePath, string coPassportImagePath, string coRationCardImagePath, string coPANImagePath, Nullable<bool> secCoIsPAN, Nullable<bool> secCoIsPhoto, Nullable<bool> secCoIsAddressPf, Nullable<bool> secCoIsRationCard, Nullable<bool> secCoIsDrivingLicence, Nullable<bool> secCoIsVoterCard, Nullable<bool> secCoIsPassPort, string secCoPhotoImagePath, string secCoAddPfImagePath, string secCoDVImagePath, string secCoVoterCardImagePath, string secCoPassportImagePath, string secCoRationCardImagePath, string secCoPANImagePath, string bankBranch, string pinCode, string executiveName, string coPinCode, string secCoPinCode, Nullable<decimal> transferAmount, string affidavit, Nullable<int> propertyID, string paymentFor, Nullable<System.DateTime> bookingDate, Nullable<int> propertyTypeID, Nullable<int> propertySizeID, Nullable<int> planID, string userName, Nullable<bool> isChangeInstallmentPlan)
    {

        var flatIDParameter = flatID != null ?
            new ObjectParameter("FlatID", flatID) :
            new ObjectParameter("FlatID", typeof(string));


        var saleIDParameter = saleID.HasValue ?
            new ObjectParameter("SaleID", saleID) :
            new ObjectParameter("SaleID", typeof(int));


        var saleDateParameter = saleDate.HasValue ?
            new ObjectParameter("SaleDate", saleDate) :
            new ObjectParameter("SaleDate", typeof(System.DateTime));


        var bookingAmountParameter = bookingAmount.HasValue ?
            new ObjectParameter("BookingAmount", bookingAmount) :
            new ObjectParameter("BookingAmount", typeof(decimal));


        var fNameParameter = fName != null ?
            new ObjectParameter("FName", fName) :
            new ObjectParameter("FName", typeof(string));


        var mNameParameter = mName != null ?
            new ObjectParameter("MName", mName) :
            new ObjectParameter("MName", typeof(string));


        var lNameParameter = lName != null ?
            new ObjectParameter("LName", lName) :
            new ObjectParameter("LName", typeof(string));


        var titleParameter = title != null ?
            new ObjectParameter("Title", title) :
            new ObjectParameter("Title", typeof(string));


        var pNameParameter = pName != null ?
            new ObjectParameter("PName", pName) :
            new ObjectParameter("PName", typeof(string));


        var address1Parameter = address1 != null ?
            new ObjectParameter("Address1", address1) :
            new ObjectParameter("Address1", typeof(string));


        var address2Parameter = address2 != null ?
            new ObjectParameter("Address2", address2) :
            new ObjectParameter("Address2", typeof(string));


        var cityParameter = city != null ?
            new ObjectParameter("City", city) :
            new ObjectParameter("City", typeof(string));


        var disttParameter = distt != null ?
            new ObjectParameter("Distt", distt) :
            new ObjectParameter("Distt", typeof(string));


        var stateParameter = state != null ?
            new ObjectParameter("State", state) :
            new ObjectParameter("State", typeof(string));


        var countryParameter = country != null ?
            new ObjectParameter("Country", country) :
            new ObjectParameter("Country", typeof(string));


        var pANParameter = pAN != null ?
            new ObjectParameter("PAN", pAN) :
            new ObjectParameter("PAN", typeof(string));


        var mobileNoParameter = mobileNo != null ?
            new ObjectParameter("MobileNo", mobileNo) :
            new ObjectParameter("MobileNo", typeof(string));


        var dateOfBirthParameter = dateOfBirth.HasValue ?
            new ObjectParameter("DateOfBirth", dateOfBirth) :
            new ObjectParameter("DateOfBirth", typeof(System.DateTime));


        var coFNameParameter = coFName != null ?
            new ObjectParameter("CoFName", coFName) :
            new ObjectParameter("CoFName", typeof(string));


        var coMNameParameter = coMName != null ?
            new ObjectParameter("CoMName", coMName) :
            new ObjectParameter("CoMName", typeof(string));


        var coLNameParameter = coLName != null ?
            new ObjectParameter("CoLName", coLName) :
            new ObjectParameter("CoLName", typeof(string));


        var coAddress1Parameter = coAddress1 != null ?
            new ObjectParameter("CoAddress1", coAddress1) :
            new ObjectParameter("CoAddress1", typeof(string));


        var coAddress2Parameter = coAddress2 != null ?
            new ObjectParameter("CoAddress2", coAddress2) :
            new ObjectParameter("CoAddress2", typeof(string));


        var coCityParameter = coCity != null ?
            new ObjectParameter("CoCity", coCity) :
            new ObjectParameter("CoCity", typeof(string));


        var coStateParameter = coState != null ?
            new ObjectParameter("CoState", coState) :
            new ObjectParameter("CoState", typeof(string));


        var coCountryParameter = coCountry != null ?
            new ObjectParameter("CoCountry", coCountry) :
            new ObjectParameter("CoCountry", typeof(string));


        var coPANParameter = coPAN != null ?
            new ObjectParameter("CoPAN", coPAN) :
            new ObjectParameter("CoPAN", typeof(string));


        var coMobileNoParameter = coMobileNo != null ?
            new ObjectParameter("CoMobileNo", coMobileNo) :
            new ObjectParameter("CoMobileNo", typeof(string));


        var alternateMobileParameter = alternateMobile != null ?
            new ObjectParameter("AlternateMobile", alternateMobile) :
            new ObjectParameter("AlternateMobile", typeof(string));


        var landLineParameter = landLine != null ?
            new ObjectParameter("LandLine", landLine) :
            new ObjectParameter("LandLine", typeof(string));


        var emailIDParameter = emailID != null ?
            new ObjectParameter("EmailID", emailID) :
            new ObjectParameter("EmailID", typeof(string));


        var alternateEmailParameter = alternateEmail != null ?
            new ObjectParameter("AlternateEmail", alternateEmail) :
            new ObjectParameter("AlternateEmail", typeof(string));


        var loanAmountParameter = loanAmount.HasValue ?
            new ObjectParameter("LoanAmount", loanAmount) :
            new ObjectParameter("LoanAmount", typeof(decimal));


        var lienFieldParameter = lienField != null ?
            new ObjectParameter("LienField", lienField) :
            new ObjectParameter("LienField", typeof(string));


        var bankIDParameter = bankID.HasValue ?
            new ObjectParameter("BankID", bankID) :
            new ObjectParameter("BankID", typeof(int));


        var isPANParameter = isPAN.HasValue ?
            new ObjectParameter("IsPAN", isPAN) :
            new ObjectParameter("IsPAN", typeof(bool));


        var isPhotoParameter = isPhoto.HasValue ?
            new ObjectParameter("IsPhoto", isPhoto) :
            new ObjectParameter("IsPhoto", typeof(bool));


        var isAddressPfParameter = isAddressPf.HasValue ?
            new ObjectParameter("IsAddressPf", isAddressPf) :
            new ObjectParameter("IsAddressPf", typeof(bool));


        var typeParameter = type.HasValue ?
            new ObjectParameter("Type", type) :
            new ObjectParameter("Type", typeof(int));


        var customerIDParameter = customerID.HasValue ?
            new ObjectParameter("CustomerID", customerID) :
            new ObjectParameter("CustomerID", typeof(long));


        var secCoFNameParameter = secCoFName != null ?
            new ObjectParameter("SecCoFName", secCoFName) :
            new ObjectParameter("SecCoFName", typeof(string));


        var secCoMNameParameter = secCoMName != null ?
            new ObjectParameter("SecCoMName", secCoMName) :
            new ObjectParameter("SecCoMName", typeof(string));


        var secCoLNameParameter = secCoLName != null ?
            new ObjectParameter("SecCoLName", secCoLName) :
            new ObjectParameter("SecCoLName", typeof(string));


        var secCoAddress1Parameter = secCoAddress1 != null ?
            new ObjectParameter("SecCoAddress1", secCoAddress1) :
            new ObjectParameter("SecCoAddress1", typeof(string));


        var secCoAddress2Parameter = secCoAddress2 != null ?
            new ObjectParameter("SecCoAddress2", secCoAddress2) :
            new ObjectParameter("SecCoAddress2", typeof(string));


        var secCoCityParameter = secCoCity != null ?
            new ObjectParameter("SecCoCity", secCoCity) :
            new ObjectParameter("SecCoCity", typeof(string));


        var secCoStateParameter = secCoState != null ?
            new ObjectParameter("SecCoState", secCoState) :
            new ObjectParameter("SecCoState", typeof(string));


        var secCoCountryParameter = secCoCountry != null ?
            new ObjectParameter("SecCoCountry", secCoCountry) :
            new ObjectParameter("SecCoCountry", typeof(string));


        var secCoMobileNoParameter = secCoMobileNo != null ?
            new ObjectParameter("SecCoMobileNo", secCoMobileNo) :
            new ObjectParameter("SecCoMobileNo", typeof(string));


        var secCoPANParameter = secCoPAN != null ?
            new ObjectParameter("SecCoPAN", secCoPAN) :
            new ObjectParameter("SecCoPAN", typeof(string));


        var coTitleParameter = coTitle != null ?
            new ObjectParameter("CoTitle", coTitle) :
            new ObjectParameter("CoTitle", typeof(string));


        var coPNameParameter = coPName != null ?
            new ObjectParameter("CoPName", coPName) :
            new ObjectParameter("CoPName", typeof(string));


        var secCoTitleParameter = secCoTitle != null ?
            new ObjectParameter("SecCoTitle", secCoTitle) :
            new ObjectParameter("SecCoTitle", typeof(string));


        var secCoPNameParameter = secCoPName != null ?
            new ObjectParameter("SecCoPName", secCoPName) :
            new ObjectParameter("SecCoPName", typeof(string));


        var isConstructionParameter = isConstruction.HasValue ?
            new ObjectParameter("IsConstruction", isConstruction) :
            new ObjectParameter("IsConstruction", typeof(int));


        var appTitleParameter = appTitle != null ?
            new ObjectParameter("AppTitle", appTitle) :
            new ObjectParameter("AppTitle", typeof(string));


        var coAppTitleParameter = coAppTitle != null ?
            new ObjectParameter("CoAppTitle", coAppTitle) :
            new ObjectParameter("CoAppTitle", typeof(string));


        var secCoAppTitleParameter = secCoAppTitle != null ?
            new ObjectParameter("SecCoAppTitle", secCoAppTitle) :
            new ObjectParameter("SecCoAppTitle", typeof(string));


        var isRationCardParameter = isRationCard.HasValue ?
            new ObjectParameter("IsRationCard", isRationCard) :
            new ObjectParameter("IsRationCard", typeof(bool));


        var isDrivingLicenceParameter = isDrivingLicence.HasValue ?
            new ObjectParameter("IsDrivingLicence", isDrivingLicence) :
            new ObjectParameter("IsDrivingLicence", typeof(bool));


        var isVoterCardParameter = isVoterCard.HasValue ?
            new ObjectParameter("IsVoterCard", isVoterCard) :
            new ObjectParameter("IsVoterCard", typeof(bool));


        var isPassPortParameter = isPassPort.HasValue ?
            new ObjectParameter("IsPassPort", isPassPort) :
            new ObjectParameter("IsPassPort", typeof(bool));


        var photoImagePathParameter = photoImagePath != null ?
            new ObjectParameter("PhotoImagePath", photoImagePath) :
            new ObjectParameter("PhotoImagePath", typeof(string));


        var addPfImagePathParameter = addPfImagePath != null ?
            new ObjectParameter("AddPfImagePath", addPfImagePath) :
            new ObjectParameter("AddPfImagePath", typeof(string));


        var dVImagePathParameter = dVImagePath != null ?
            new ObjectParameter("DVImagePath", dVImagePath) :
            new ObjectParameter("DVImagePath", typeof(string));


        var voterCardImagePathParameter = voterCardImagePath != null ?
            new ObjectParameter("VoterCardImagePath", voterCardImagePath) :
            new ObjectParameter("VoterCardImagePath", typeof(string));


        var passportImagePathParameter = passportImagePath != null ?
            new ObjectParameter("PassportImagePath", passportImagePath) :
            new ObjectParameter("PassportImagePath", typeof(string));


        var rationCardImagePathParameter = rationCardImagePath != null ?
            new ObjectParameter("RationCardImagePath", rationCardImagePath) :
            new ObjectParameter("RationCardImagePath", typeof(string));


        var remarksParameter = remarks != null ?
            new ObjectParameter("Remarks", remarks) :
            new ObjectParameter("Remarks", typeof(string));


        var coDOBParameter = coDOB.HasValue ?
            new ObjectParameter("CoDOB", coDOB) :
            new ObjectParameter("CoDOB", typeof(System.DateTime));


        var secCoDOBParameter = secCoDOB.HasValue ?
            new ObjectParameter("SecCoDOB", secCoDOB) :
            new ObjectParameter("SecCoDOB", typeof(System.DateTime));


        var pANImagePathParameter = pANImagePath != null ?
            new ObjectParameter("PANImagePath", pANImagePath) :
            new ObjectParameter("PANImagePath", typeof(string));


        var coIsPANParameter = coIsPAN.HasValue ?
            new ObjectParameter("CoIsPAN", coIsPAN) :
            new ObjectParameter("CoIsPAN", typeof(bool));


        var coIsPhotoParameter = coIsPhoto.HasValue ?
            new ObjectParameter("CoIsPhoto", coIsPhoto) :
            new ObjectParameter("CoIsPhoto", typeof(bool));


        var coIsAddressPfParameter = coIsAddressPf.HasValue ?
            new ObjectParameter("CoIsAddressPf", coIsAddressPf) :
            new ObjectParameter("CoIsAddressPf", typeof(bool));


        var coIsRationCardParameter = coIsRationCard.HasValue ?
            new ObjectParameter("CoIsRationCard", coIsRationCard) :
            new ObjectParameter("CoIsRationCard", typeof(bool));


        var coIsDrivingLicenceParameter = coIsDrivingLicence.HasValue ?
            new ObjectParameter("CoIsDrivingLicence", coIsDrivingLicence) :
            new ObjectParameter("CoIsDrivingLicence", typeof(bool));


        var coIsVoterCardParameter = coIsVoterCard.HasValue ?
            new ObjectParameter("CoIsVoterCard", coIsVoterCard) :
            new ObjectParameter("CoIsVoterCard", typeof(bool));


        var coIsPassPortParameter = coIsPassPort.HasValue ?
            new ObjectParameter("CoIsPassPort", coIsPassPort) :
            new ObjectParameter("CoIsPassPort", typeof(bool));


        var coPhotoImagePathParameter = coPhotoImagePath != null ?
            new ObjectParameter("CoPhotoImagePath", coPhotoImagePath) :
            new ObjectParameter("CoPhotoImagePath", typeof(string));


        var coAddPfImagePathParameter = coAddPfImagePath != null ?
            new ObjectParameter("CoAddPfImagePath", coAddPfImagePath) :
            new ObjectParameter("CoAddPfImagePath", typeof(string));


        var coDVImagePathParameter = coDVImagePath != null ?
            new ObjectParameter("CoDVImagePath", coDVImagePath) :
            new ObjectParameter("CoDVImagePath", typeof(string));


        var coVoterCardImagePathParameter = coVoterCardImagePath != null ?
            new ObjectParameter("CoVoterCardImagePath", coVoterCardImagePath) :
            new ObjectParameter("CoVoterCardImagePath", typeof(string));


        var coPassportImagePathParameter = coPassportImagePath != null ?
            new ObjectParameter("CoPassportImagePath", coPassportImagePath) :
            new ObjectParameter("CoPassportImagePath", typeof(string));


        var coRationCardImagePathParameter = coRationCardImagePath != null ?
            new ObjectParameter("CoRationCardImagePath", coRationCardImagePath) :
            new ObjectParameter("CoRationCardImagePath", typeof(string));


        var coPANImagePathParameter = coPANImagePath != null ?
            new ObjectParameter("CoPANImagePath", coPANImagePath) :
            new ObjectParameter("CoPANImagePath", typeof(string));


        var secCoIsPANParameter = secCoIsPAN.HasValue ?
            new ObjectParameter("SecCoIsPAN", secCoIsPAN) :
            new ObjectParameter("SecCoIsPAN", typeof(bool));


        var secCoIsPhotoParameter = secCoIsPhoto.HasValue ?
            new ObjectParameter("SecCoIsPhoto", secCoIsPhoto) :
            new ObjectParameter("SecCoIsPhoto", typeof(bool));


        var secCoIsAddressPfParameter = secCoIsAddressPf.HasValue ?
            new ObjectParameter("SecCoIsAddressPf", secCoIsAddressPf) :
            new ObjectParameter("SecCoIsAddressPf", typeof(bool));


        var secCoIsRationCardParameter = secCoIsRationCard.HasValue ?
            new ObjectParameter("SecCoIsRationCard", secCoIsRationCard) :
            new ObjectParameter("SecCoIsRationCard", typeof(bool));


        var secCoIsDrivingLicenceParameter = secCoIsDrivingLicence.HasValue ?
            new ObjectParameter("SecCoIsDrivingLicence", secCoIsDrivingLicence) :
            new ObjectParameter("SecCoIsDrivingLicence", typeof(bool));


        var secCoIsVoterCardParameter = secCoIsVoterCard.HasValue ?
            new ObjectParameter("SecCoIsVoterCard", secCoIsVoterCard) :
            new ObjectParameter("SecCoIsVoterCard", typeof(bool));


        var secCoIsPassPortParameter = secCoIsPassPort.HasValue ?
            new ObjectParameter("SecCoIsPassPort", secCoIsPassPort) :
            new ObjectParameter("SecCoIsPassPort", typeof(bool));


        var secCoPhotoImagePathParameter = secCoPhotoImagePath != null ?
            new ObjectParameter("SecCoPhotoImagePath", secCoPhotoImagePath) :
            new ObjectParameter("SecCoPhotoImagePath", typeof(string));


        var secCoAddPfImagePathParameter = secCoAddPfImagePath != null ?
            new ObjectParameter("SecCoAddPfImagePath", secCoAddPfImagePath) :
            new ObjectParameter("SecCoAddPfImagePath", typeof(string));


        var secCoDVImagePathParameter = secCoDVImagePath != null ?
            new ObjectParameter("SecCoDVImagePath", secCoDVImagePath) :
            new ObjectParameter("SecCoDVImagePath", typeof(string));


        var secCoVoterCardImagePathParameter = secCoVoterCardImagePath != null ?
            new ObjectParameter("SecCoVoterCardImagePath", secCoVoterCardImagePath) :
            new ObjectParameter("SecCoVoterCardImagePath", typeof(string));


        var secCoPassportImagePathParameter = secCoPassportImagePath != null ?
            new ObjectParameter("SecCoPassportImagePath", secCoPassportImagePath) :
            new ObjectParameter("SecCoPassportImagePath", typeof(string));


        var secCoRationCardImagePathParameter = secCoRationCardImagePath != null ?
            new ObjectParameter("SecCoRationCardImagePath", secCoRationCardImagePath) :
            new ObjectParameter("SecCoRationCardImagePath", typeof(string));


        var secCoPANImagePathParameter = secCoPANImagePath != null ?
            new ObjectParameter("SecCoPANImagePath", secCoPANImagePath) :
            new ObjectParameter("SecCoPANImagePath", typeof(string));


        var bankBranchParameter = bankBranch != null ?
            new ObjectParameter("BankBranch", bankBranch) :
            new ObjectParameter("BankBranch", typeof(string));


        var pinCodeParameter = pinCode != null ?
            new ObjectParameter("PinCode", pinCode) :
            new ObjectParameter("PinCode", typeof(string));


        var executiveNameParameter = executiveName != null ?
            new ObjectParameter("ExecutiveName", executiveName) :
            new ObjectParameter("ExecutiveName", typeof(string));


        var coPinCodeParameter = coPinCode != null ?
            new ObjectParameter("CoPinCode", coPinCode) :
            new ObjectParameter("CoPinCode", typeof(string));


        var secCoPinCodeParameter = secCoPinCode != null ?
            new ObjectParameter("SecCoPinCode", secCoPinCode) :
            new ObjectParameter("SecCoPinCode", typeof(string));


        var transferAmountParameter = transferAmount.HasValue ?
            new ObjectParameter("TransferAmount", transferAmount) :
            new ObjectParameter("TransferAmount", typeof(decimal));


        var affidavitParameter = affidavit != null ?
            new ObjectParameter("affidavit", affidavit) :
            new ObjectParameter("affidavit", typeof(string));


        var propertyIDParameter = propertyID.HasValue ?
            new ObjectParameter("PropertyID", propertyID) :
            new ObjectParameter("PropertyID", typeof(int));


        var paymentForParameter = paymentFor != null ?
            new ObjectParameter("PaymentFor", paymentFor) :
            new ObjectParameter("PaymentFor", typeof(string));


        var bookingDateParameter = bookingDate.HasValue ?
            new ObjectParameter("BookingDate", bookingDate) :
            new ObjectParameter("BookingDate", typeof(System.DateTime));


        var propertyTypeIDParameter = propertyTypeID.HasValue ?
            new ObjectParameter("PropertyTypeID", propertyTypeID) :
            new ObjectParameter("PropertyTypeID", typeof(int));


        var propertySizeIDParameter = propertySizeID.HasValue ?
            new ObjectParameter("PropertySizeID", propertySizeID) :
            new ObjectParameter("PropertySizeID", typeof(int));


        var planIDParameter = planID.HasValue ?
            new ObjectParameter("PlanID", planID) :
            new ObjectParameter("PlanID", typeof(int));


        var userNameParameter = userName != null ?
            new ObjectParameter("UserName", userName) :
            new ObjectParameter("UserName", typeof(string));


        var isChangeInstallmentPlanParameter = isChangeInstallmentPlan.HasValue ?
            new ObjectParameter("IsChangeInstallmentPlan", isChangeInstallmentPlan) :
            new ObjectParameter("IsChangeInstallmentPlan", typeof(bool));


        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("INSERT_RBookTransferFlat", flatIDParameter, saleIDParameter, saleDateParameter, bookingAmountParameter, fNameParameter, mNameParameter, lNameParameter, titleParameter, pNameParameter, address1Parameter, address2Parameter, cityParameter, disttParameter, stateParameter, countryParameter, pANParameter, mobileNoParameter, dateOfBirthParameter, coFNameParameter, coMNameParameter, coLNameParameter, coAddress1Parameter, coAddress2Parameter, coCityParameter, coStateParameter, coCountryParameter, coPANParameter, coMobileNoParameter, alternateMobileParameter, landLineParameter, emailIDParameter, alternateEmailParameter, loanAmountParameter, lienFieldParameter, bankIDParameter, isPANParameter, isPhotoParameter, isAddressPfParameter, typeParameter, customerIDParameter, secCoFNameParameter, secCoMNameParameter, secCoLNameParameter, secCoAddress1Parameter, secCoAddress2Parameter, secCoCityParameter, secCoStateParameter, secCoCountryParameter, secCoMobileNoParameter, secCoPANParameter, coTitleParameter, coPNameParameter, secCoTitleParameter, secCoPNameParameter, isConstructionParameter, appTitleParameter, coAppTitleParameter, secCoAppTitleParameter, isRationCardParameter, isDrivingLicenceParameter, isVoterCardParameter, isPassPortParameter, photoImagePathParameter, addPfImagePathParameter, dVImagePathParameter, voterCardImagePathParameter, passportImagePathParameter, rationCardImagePathParameter, remarksParameter, coDOBParameter, secCoDOBParameter, pANImagePathParameter, coIsPANParameter, coIsPhotoParameter, coIsAddressPfParameter, coIsRationCardParameter, coIsDrivingLicenceParameter, coIsVoterCardParameter, coIsPassPortParameter, coPhotoImagePathParameter, coAddPfImagePathParameter, coDVImagePathParameter, coVoterCardImagePathParameter, coPassportImagePathParameter, coRationCardImagePathParameter, coPANImagePathParameter, secCoIsPANParameter, secCoIsPhotoParameter, secCoIsAddressPfParameter, secCoIsRationCardParameter, secCoIsDrivingLicenceParameter, secCoIsVoterCardParameter, secCoIsPassPortParameter, secCoPhotoImagePathParameter, secCoAddPfImagePathParameter, secCoDVImagePathParameter, secCoVoterCardImagePathParameter, secCoPassportImagePathParameter, secCoRationCardImagePathParameter, secCoPANImagePathParameter, bankBranchParameter, pinCodeParameter, executiveNameParameter, coPinCodeParameter, secCoPinCodeParameter, transferAmountParameter, affidavitParameter, propertyIDParameter, paymentForParameter, bookingDateParameter, propertyTypeIDParameter, propertySizeIDParameter, planIDParameter, userNameParameter, isChangeInstallmentPlanParameter);
    }

}

}

