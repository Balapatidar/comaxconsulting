﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Comax.Web.Models
{
    public class PropertyModel
    {
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
    }
    public class PropertyListModel
    {
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
    }
}