
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Comax.Web.Models
{

using System;
    
public partial class GetRightsByUserID_Result
{

    public int MenuID { get; set; }

    public string MenuName { get; set; }

    public string MenuLink { get; set; }

    public Nullable<int> ParentID { get; set; }

    public Nullable<int> MenuOrder { get; set; }

    public Nullable<int> MenuLevel { get; set; }

    public Nullable<int> IsDeleted { get; set; }

    public Nullable<int> IsPopup { get; set; }

    public Nullable<int> IsSold { get; set; }

    public string ParentIMG { get; set; }

    public bool IsAdd { get; set; }

    public bool IsEdit { get; set; }

    public bool IsDelete { get; set; }

    public bool IsView { get; set; }

    public bool IsPrint { get; set; }

    public bool IsExport { get; set; }

}

}
