
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Comax.Web.Models
{

using System;
    
public partial class GetEventsByBlock_Result
{

    public int EventID { get; set; }

    public string EventName { get; set; }

    public string EventStatus { get; set; }

    public Nullable<System.DateTime> DueDate { get; set; }

    public int TotalAmount { get; set; }

    public Nullable<decimal> AmtPer { get; set; }

}

}
