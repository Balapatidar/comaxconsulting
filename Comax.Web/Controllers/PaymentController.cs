﻿using Comax.Web.Code;
using Comax.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Hosting;
using AutoMapper;
using System.Globalization;

namespace Comax.Web.Controllers
{
   
    public class PaymentController : BaseController
    {
        dbSBPEntities1 SbpEntity = new dbSBPEntities1();
        DataFunctions obj = new DataFunctions();
        // GET: Index
          [MyAuthorize]
        public ActionResult Index(int? id)
        {
            ViewBag.ID = id;
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            return View();
        }
          [MyAuthorize]
        public ActionResult OtherPayments()
        {
            return View();
        }
          [MyAuthorize]
        public ActionResult Details(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Details(FormCollection collection, int id)
        {
            return View();
        }
          [MyAuthorize]
        public ActionResult Edit(int id)
        {
            ViewBag.ID = id;
            return View();
        }
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            return View();
        }
        // GET: Payment
        [MyAuthorize]
        public ActionResult Payment()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Payment(FormCollection collection)
        {
            return View();
        }

        [MyAuthorize]
        public ActionResult Search()
        {
            Session["PropertyName"] = "Property Name";
            DateTime datef = new DateTime();
            DateTime datet = new DateTime();
            // Get this month records
            datef = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            datet = datef.AddMonths(1);
            dbSBPEntities2 context = new dbSBPEntities2();
            var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(FormCollection collection)
        {
            try
            {
                string proName = collection["newCust.PropertyID"];
                string proType = collection["newCust.PropertyTypeID"];
                string proSize = collection["newCust.PropertySizeID"];
                string protypename = collection["protypename"];
                int pid = 0, ptype = 0, psize = 0;
                if (proName == "? undefined:undefined ?" || proName == "All") proName = "All"; else pid = Convert.ToInt32(proName);
                if (proType == "? undefined:undefined ?" || proType == "All") { proType = "All"; Session["PropertyName"] = "Property Name"; } else { ptype = Convert.ToInt32(proType); Session["PropertyName"] = protypename; }
                if (proSize == "? undefined:undefined ?" || proSize == "All") proSize = "All"; else psize = Convert.ToInt32(proSize);
                dbSBPEntities2 context = new dbSBPEntities2();
                string flat = collection["search1"];
                if (flat == "0") // Customer Search.
                {
                    #region Custom Search
                    string srch = collection["search1"];
                    string search = collection["searchby"];
                    string soryby = collection["sortby"];
                    string datefrom = collection["datefrom"];
                    string dateto = collection["dateto"];
                    string searchtext = collection["searchtext"];


                    DateTime datef = new DateTime();
                    DateTime datet = new DateTime();
                    // Date.
                    if (datefrom != "" && dateto != "")
                    {
                        datef = Convert.ToDateTime(datefrom);
                        datet = Convert.ToDateTime(dateto);
                    }
                    if (search == "All")
                    {
                        if (soryby == "All")
                        {
                            // Get this month records
                            datef = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                            datet = datef.AddMonths(1);
                            var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            return View(model);
                        }
                        else
                        {
                            var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            return View(model);
                        }
                    }
                    else if (search == "FlatName")
                    {
                        if (soryby == "All")
                        {
                            var model = context.tblSPayments.Where(p => p.FlatName.Contains(searchtext)).OrderByDescending(o => o.PaymentID);
                            return View(model);
                        }
                        else
                        {
                            // Date.
                            var model = context.tblSPayments.Where(p => p.FlatName.Contains(searchtext) && p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            return View(model);
                        }
                    }
                    else if (search == "Customer Name")
                    {
                        if (soryby == "All")
                        {
                            var model = context.tblSPayments.Where(p => p.CustomerName.Contains(searchtext)).OrderByDescending(o => o.PaymentID);
                            return View(model);
                        }
                        else
                        {
                            // Date.
                            var model = context.tblSPayments.Where(p => p.CustomerName.Contains(searchtext) && p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            return View(model);
                        }
                    }
                    else if (search == "Cheque No")
                    {
                        if (soryby == "All")
                        {
                            var model = context.tblSPayments.Where(p => p.ChequeNo.Contains(searchtext)).OrderByDescending(o => o.PaymentID);
                            return View(model);
                        }
                        else
                        {
                            // Date.
                            var model = context.tblSPayments.Where(p => p.ChequeNo.Contains(searchtext) && p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            return View(model);
                        }
                    }
                    else if (search == "This Month")
                    {
                        datef = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        datet = datef.AddMonths(1);
                        // Date.
                        var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                        return View(model);
                    }
                    else if (search == "Last 7 Days")
                    {
                        datet = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        datef = datet.AddDays(-7);
                        // Date.
                        var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                        return View(model);
                    }
                    else
                    {
                        // Get this month records
                        datef = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        datet = datef.AddMonths(1);
                        var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                        return View(model);
                    }
                    #endregion
                }
                else
                {
                    // Property Search.
                    if (proName != "All" && proType!="All" && proSize!="All")
                    {
                        // Search by property name, type and size
                        var model1 = (from sale in
                                          context.tblSSaleFlats
                                      join pay in context.tblSPayments on sale.SaleID equals pay.SaleID
                                      where sale.PropertyID.Value == pid && sale.PropertyTypeID==ptype && sale.PropertySizeID==psize
                                      select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName =pay.CustomerName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentID = pay.PaymentID }).AsEnumerable();
                        List<tblSPayment> py = new List<tblSPayment>();
                        foreach (var v in model1)
                        {
                            py.Add(new tblSPayment { TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentID = v.PaymentID });
                        }
                        return View(py);
                    }
                    else if(proName != "All" && proType!="All" && proSize=="All")
                    {
                        // Search by Name, and Type.
                        var model1 = (from sale in
                                          context.tblSSaleFlats
                                      join pay in context.tblSPayments on sale.SaleID equals pay.SaleID
                                      where sale.PropertyID.Value == pid && sale.PropertyTypeID == ptype
                                      select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName =pay.CustomerName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentID = pay.PaymentID }).AsEnumerable();
                        List<tblSPayment> py = new List<tblSPayment>();
                        foreach (var v in model1)
                        {
                            py.Add(new tblSPayment { TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentID = v.PaymentID });
                        }
                        return View(py);
                    }
                    else if(proName != "All" && proType=="All" && proSize=="All")
                    {
                        // Search by name.
                        var model1 = (from sale in
                                          context.tblSSaleFlats
                                      join pay in context.tblSPayments on sale.SaleID equals pay.SaleID
                                      where sale.PropertyID.Value == pid
                                      select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName =pay.CustomerName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentID = pay.PaymentID }).AsEnumerable();
                        List<tblSPayment> py = new List<tblSPayment>();
                        foreach (var v in model1)
                        {
                            py.Add(new tblSPayment { TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentID = v.PaymentID });
                        }
                        return View(py);
                    }
                }

                return View();
            }
            catch (Exception ex)
            {
                dbSBPEntities2 context = new dbSBPEntities2();

                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return View();
            }
        }

        [MyAuthorize]
        public ActionResult EditPayment()
        {
            Session["PropertyName"] = "Property Name";
            DateTime datef = new DateTime();
            DateTime datet = new DateTime();
            // Get this month records
            datef = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            datet = datef.AddMonths(1);
            dbSBPEntities2 context = new dbSBPEntities2();
            var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
            return View(model);
        }

        [MyAuthorize]
        public ActionResult Clearance(string id)
        {
            ViewBag.ID = id;
            return View();
        }

        [MyAuthorize]
        public ActionResult ViewFlat()
        {
            return View();
        }
        #region JsonMethod
        // Search flat 
        public JsonResult GetFlatList(string flatname, string pid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var Flat = obj.GetDataTable("Select * from tblsflat where FlatName='" + flatname + "' and PID='" + pid + "' and Status=1");
            var v = Flat.AsEnumerable().ToList();

            return Json(new { Result = (from i in v select new { FlatID = i["FlatID"], FlatName = i["FlatName"] }) }, JsonRequestBehavior.AllowGet);
        }


        //Get proptyname and  Pid
        public JsonResult GetPidProptyname(string saleid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var Flat = obj.GetDataTable("SELECT     tblSSaleFlat.SaleID AS Expr1, tblSSaleFlat.FlatID , tblSFlat.FlatName, tblSFlat.PID FROM         tblSSaleFlat INNER JOIN tblSFlat ON tblSSaleFlat.FlatID = tblSFlat.FlatID where  tblSSaleFlat.SaleID = " + saleid + "");
            var v = Flat.AsEnumerable().ToList();

            return Json(new { Result = (from i in v select new { FlatID = i["FlatID"], FlatName = i["FlatName"], Pid = i["PID"] }) }, JsonRequestBehavior.AllowGet);
        }



        // GET Payment by saleid
        public JsonResult GetPayment(string saleid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var payment = obj.GetDataTable("Select * from tblSPayment where Saleid='" + saleid + "'");
            var v = payment.AsEnumerable().ToList();
            return Json(new { Payments = (from p in v select new { TransactionID = p["TransactionID"], InstallmentNo = p["InstallmentNo"], PaymentNo = p["PaymentNo"], PaymentDate = p["PaymentDate"], TotalAmount = p["TotalAmount"], Amount = p["Amount"] }) }, JsonRequestBehavior.AllowGet);
        }
        public string GetOtherPaymentByTransactionID(string tid)
        {
            int id=Convert.ToInt32(tid);
            dbSBPEntities2 context = new dbSBPEntities2();
            var m = context.tblSPaymentOthers.Where(p => p.TransactionID == id).FirstOrDefault();
            Mapper.CreateMap<tblSPaymentOther, PaymentOtherModel>();
            var model= Mapper.Map<tblSPaymentOther, PaymentOtherModel>(m);
            string pdate = "";
            if (m.PaymentDate.Value != null)
                pdate = Convert.ToDateTime(m.PaymentDate.Value).ToString("dd/MM/yyyy");
            string cdate = "";
            if (m.ChequeDate.Value != null)
                pdate = Convert.ToDateTime(m.ChequeDate.Value).ToString("dd/MM/yyyy");
            model.PaymentDateSt = pdate;
            model.ChequeDateSt = cdate;
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }
        public string GetOtherPaymentBySaleID(string sid)
        {
            int id = Convert.ToInt32(sid);
            dbSBPEntities2 context = new dbSBPEntities2();
            var m = context.tblSPaymentOthers.Where(p => p.SaleID == id).AsEnumerable();
            List<PaymentOtherModel> payment = new List<PaymentOtherModel>();
            foreach(var pay in m)
            {
                string pdate = "";
                if (pay.PaymentDate.Value != null)
                    pdate = Convert.ToDateTime(pay.PaymentDate.Value).ToString("dd/MM/yyyy");
                string cdate = "";
                if (pay.ChequeDate != null)
                    pdate = Convert.ToDateTime(pay.ChequeDate.Value).ToString("dd/MM/yyyy");
                Mapper.CreateMap<tblSPaymentOther, PaymentOtherModel>();
                var model = Mapper.Map<tblSPaymentOther, PaymentOtherModel>(pay);
                model.PaymentDateSt = pdate;
                model.ChequeDateSt = cdate;
                payment.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(payment);
        }
        public string GetServiceTaxPaymentBySaleID(string sid)
        {
            int id = Convert.ToInt32(sid);
            dbSBPEntities2 context = new dbSBPEntities2();
            var m = context.tblSPaymentOthers.Where(p => p.SaleID == id && p.PaymentFor.Contains("Service Tax")).AsEnumerable();
            List<PaymentOtherModel> payment = new List<PaymentOtherModel>();
            foreach (var pay in m)
            {
                string pdate = "";
                if (pay.PaymentDate.Value != null)
                    pdate = Convert.ToDateTime(pay.PaymentDate.Value).ToString("dd/MM/yyyy");
                string cdate = "";
                if (pay.ChequeDate != null)
                    pdate = Convert.ToDateTime(pay.ChequeDate.Value).ToString("dd/MM/yyyy");
                Mapper.CreateMap<tblSPaymentOther, PaymentOtherModel>();
                var model = Mapper.Map<tblSPaymentOther, PaymentOtherModel>(pay);
                model.PaymentDateSt = pdate;
                model.ChequeDateSt = cdate;
                payment.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(payment);
        }
        public string GetTransferPaymentBySaleID(string sid)
        {
            int id = Convert.ToInt32(sid);
            dbSBPEntities2 context = new dbSBPEntities2();
            var m = context.tblSPaymentOthers.Where(p => p.SaleID == id && p.PaymentFor.Contains("Transfer Fee")).AsEnumerable();
            List<PaymentOtherModel> payment = new List<PaymentOtherModel>();
            foreach (var pay in m)
            {
                string pdate = "";
                if (pay.PaymentDate.Value != null)
                    pdate = Convert.ToDateTime(pay.PaymentDate.Value).ToString("dd/MM/yyyy");
                string cdate = "";
                if (pay.ChequeDate != null)
                    pdate = Convert.ToDateTime(pay.ChequeDate.Value).ToString("dd/MM/yyyy");
                Mapper.CreateMap<tblSPaymentOther, PaymentOtherModel>();
                var model = Mapper.Map<tblSPaymentOther, PaymentOtherModel>(pay);
                model.PaymentDateSt = pdate;
                model.ChequeDateSt = cdate;
                payment.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(payment);
        }

        public string GetLateChargeBySaleID(string sid)
        {
            int id = Convert.ToInt32(sid);
            dbSBPEntities2 context = new dbSBPEntities2();
            var m = context.LatePayments.Where(p => p.SaleID == id).AsEnumerable();
            List<LatePaymentModel> payment = new List<LatePaymentModel>();
            foreach (var pay in m)
            {
                string pdate = "";
                if (pay.CrDate.Value != null)
                    pdate = Convert.ToDateTime(pay.CrDate.Value).ToString("dd/MM/yyyy");
                string cdate = "";
                if (pay.DueDate != null)
                    cdate = Convert.ToDateTime(pay.DueDate.Value).ToString("dd/MM/yyyy");
                Mapper.CreateMap<LatePayment, LatePaymentModel>();
                var model = Mapper.Map<LatePayment, LatePaymentModel>(pay);
                model.CrDateSt = pdate;
                model.DueDateSt = cdate;
                payment.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(payment);
        }
    

        public string GetLateFeePaymentBySaleID(string sid)
        {
            int id = Convert.ToInt32(sid);
            dbSBPEntities2 context = new dbSBPEntities2();
            var m = context.tblSPaymentOthers.Where(p => p.SaleID == id && p.PaymentFor.Contains("Late Payment Charges")).AsEnumerable();
            List<PaymentOtherModel> payment = new List<PaymentOtherModel>();
            foreach (var pay in m)
            {
                string pdate = "";
                if (pay.PaymentDate.Value != null)
                    pdate = Convert.ToDateTime(pay.PaymentDate.Value).ToString("dd/MM/yyyy");
                string cdate = "";
                if (pay.ChequeDate != null)
                    cdate = Convert.ToDateTime(pay.ChequeDate.Value).ToString("dd/MM/yyyy");
                Mapper.CreateMap<tblSPaymentOther, PaymentOtherModel>();
                var model = Mapper.Map<tblSPaymentOther, PaymentOtherModel>(pay);
                model.PaymentDateSt = pdate;
                model.ChequeDateSt = cdate;
                payment.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(payment);
        }
        public string GetMaintanancePaymentBySaleID(string sid)
        {
            int id = Convert.ToInt32(sid);
            dbSBPEntities2 context = new dbSBPEntities2();
            var m = context.tblSPaymentOthers.Where(p => p.SaleID == id && p.PaymentFor.Contains("Maintanance Fee")).AsEnumerable();
            List<PaymentOtherModel> payment = new List<PaymentOtherModel>();
            foreach (var pay in m)
            {
                string pdate = "";
                if (pay.PaymentDate.Value != null)
                    pdate = Convert.ToDateTime(pay.PaymentDate.Value).ToString("dd/MM/yyyy");
                string cdate = "";
                if (pay.ChequeDate != null)
                    pdate = Convert.ToDateTime(pay.ChequeDate.Value).ToString("dd/MM/yyyy");
                Mapper.CreateMap<tblSPaymentOther, PaymentOtherModel>();
                var model = Mapper.Map<tblSPaymentOther, PaymentOtherModel>(pay);
                model.PaymentDateSt = pdate;
                model.ChequeDateSt = cdate;
                payment.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(payment);
        }
        public string GetInterestPaymentBySaleID(string sid)
        {
            int id = Convert.ToInt32(sid);
            dbSBPEntities2 context = new dbSBPEntities2();
            var m = context.tblSPaymentOthers.Where(p => p.SaleID == id && p.PaymentFor.Contains("Clearance Charges")).AsEnumerable();
            List<PaymentOtherModel> payment = new List<PaymentOtherModel>();
            foreach (var pay in m)
            {
                string pdate = "";
                if (pay.PaymentDate.Value != null)
                    pdate = Convert.ToDateTime(pay.PaymentDate.Value).ToString("dd/MM/yyyy");
                string cdate = "";
                if (pay.ChequeDate != null)
                    pdate = Convert.ToDateTime(pay.ChequeDate.Value).ToString("dd/MM/yyyy");
                Mapper.CreateMap<tblSPaymentOther, PaymentOtherModel>();
                var model = Mapper.Map<tblSPaymentOther, PaymentOtherModel>(pay);
                model.PaymentDateSt = pdate;
                model.ChequeDateSt = cdate;
                payment.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(payment);
        }
        public string GetMiscPaymentBySaleID(string sid)
        {
            int id = Convert.ToInt32(sid);
            dbSBPEntities2 context = new dbSBPEntities2();
            var m = context.tblSPaymentOthers.Where(p => p.SaleID == id && p.PaymentFor.Contains("Misc Amount")).AsEnumerable();
            List<PaymentOtherModel> payment = new List<PaymentOtherModel>();
            foreach (var pay in m)
            {
                string pdate = "";
                if (pay.PaymentDate.Value != null)
                    pdate = Convert.ToDateTime(pay.PaymentDate.Value).ToString("dd/MM/yyyy");
                string cdate = "";
                if (pay.ChequeDate != null)
                    pdate = Convert.ToDateTime(pay.ChequeDate.Value).ToString("dd/MM/yyyy");
                Mapper.CreateMap<tblSPaymentOther, PaymentOtherModel>();
                var model = Mapper.Map<tblSPaymentOther, PaymentOtherModel>(pay);
                model.PaymentDateSt = pdate;
                model.ChequeDateSt = cdate;
                payment.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(payment);
        }
        public JsonResult GetOtherPaymentGroupedBySaleID(string sid)
        {

            var Flat = obj.GetDataTable("Select * from vw_tblsPaymentOthers where SaleID='" + sid + "'");
            var v = Flat.AsEnumerable().ToList();

            return Json(new { GroupPay = (from i in v select new { SaleID = i["SaleID"], TAmount = i["TAmount"], PaymentFor=i["PaymentFor"] }) }, JsonRequestBehavior.AllowGet);
        }
        public string GetOtherPaymentByPaymentFor(string paymentFor)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            var m = context.tblSPaymentOthers.Where(p => p.PaymentFor == paymentFor).AsEnumerable();
            List<PaymentOtherModel> payment = new List<PaymentOtherModel>();
            foreach (var pay in m)
            {
                string pdate = "";
                if (pay.PaymentDate.Value != null)
                    pdate = Convert.ToDateTime(pay.PaymentDate.Value).ToString("dd/MM/yyyy");
                string cdate = "";
                if (pay.ChequeDate.Value != null)
                    pdate = Convert.ToDateTime(pay.ChequeDate.Value).ToString("dd/MM/yyyy");
                Mapper.CreateMap<tblSPaymentOther, PaymentOtherModel>();
                var model = Mapper.Map<tblSPaymentOther, PaymentOtherModel>(pay);
                model.PaymentDateSt = pdate;
                model.ChequeDateSt = cdate;
                payment.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(payment);
        }
        // GET Payment by paymnetid
        public JsonResult GetPaymentbyID(string paymentid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var payment = obj.GetDataTable("Select * from tblSPayment where PaymentID='" + paymentid + "'");
            var v = payment.AsEnumerable().ToList();
            return Json(new { Payments = (from p in v select new { TransactionID = p["TransactionID"], SaleID = p["SaleID"], InstallmentNo = p["InstallmentNo"], PaymentNo = p["PaymentNo"], PaymentMode = p["PaymentMode"], PaymentDate = p["PaymentDate"], TotalAmount = p["TotalAmount"], Amount = p["Amount"], FlatName = p["FlatName"] }) }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPaymentbyTID(string transactionid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var payment = obj.GetDataTable("Select * from tblSPayment where Transactionid='" + transactionid + "'");
            var v = payment.AsEnumerable().ToList();
            return Json(new { Payments = (from p in v select new { Remarks = p["Remarks"], PaymentID = p["PaymentID"], TransactionID = p["TransactionID"], SaleID = p["SaleID"], InstallmentNo = p["InstallmentNo"], PaymentNo = p["PaymentNo"], PaymentMode = p["PaymentMode"], CheqeNo=p["ChequeNo"],ChequeDate=p["ChequeDate"],BankName=p["BankName"],BankBranch=p["BankBranch"],BankCharges=p["BankCharges"],BankClearanceDate=p["BankClearanceDate"], PaymentDate = p["PaymentDate"], TotalAmount = p["TotalAmount"], Amount = p["Amount"], FlatName = p["FlatName"], DueAmount = p["DueAmount"], IsReceipt = p["IsReceipt"], ClearanceCharge = p["ClearanceCharge"], RefundRemark = p["RefundRemark"], IsBounce = p["IsBounce"] }) }, JsonRequestBehavior.AllowGet);
        }
        public string GetPaymentbyTIDSt(string transactionid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            int tid=Convert.ToInt32(transactionid);
            var pay= context.tblSPayments.Where(p => p.TransactionID == tid).FirstOrDefault();
            if (pay != null)
            {
                Mapper.CreateMap<tblSPayment, PaymentModel>();
                var model = Mapper.Map<tblSPayment, PaymentModel>(pay);
                if (model.PaymentDate != null)
                    model.PaymentDateSt = Convert.ToDateTime(model.PaymentDate).ToString("dd/MM/yyyy");
                if (model.ChequeDate != null)
                    model.ChequeDateSt = Convert.ToDateTime(model.ChequeDate).ToString("dd/MM/yyyy");
                if (model.BankClearanceDate != null)
                    model.BankClearanceDateSt = Convert.ToDateTime(model.BankClearanceDate).ToString("dd/MM/yyyy");
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        // GET Total Payment 
        public JsonResult GetTotalPayment(string saleid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var payment = obj.GetDataTable("select sum(Amount) as TotalAmount from tblSPayment where saleid='" + saleid + "'");
            var v = payment.AsEnumerable().ToList();
            return Json(new { TotalPaid = v[0]["TotalAmount"] }, JsonRequestBehavior.AllowGet);
        }
        // GET Total Payment 
        public JsonResult GetTotalOPayment(string saleid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var payment = obj.GetDataTable("select sum(Amount) as TotalAmount from tblSPaymentOthers where saleid='" + saleid + "'");
            var v = payment.AsEnumerable().ToList();
            return Json(new { TotalPaid = v[0]["TotalAmount"] }, JsonRequestBehavior.AllowGet);
        }
        // GET Total Service Tax 
        public JsonResult GetTotalServiceTax(string saleid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var payment = obj.GetDataTable("select sum(ServiceTaxAmount) as TotalAmount from tblSPayment where saleid='" + saleid + "'");
            var v = payment.AsEnumerable().ToList();
            string amt = v[0]["TotalAmount"].ToString();
            return Json(amt, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFlatSaleBySaleID(string saleid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var payment = obj.GetDataTable("Select s.SaleID,PlanType.PlanTypeName,s.FlatID,s.Aggrement,s.SaleDate,s.SaleRate,s.ServiceTaxPer,s.ServiceTaxAmount,s.BookingAmount,s.CustomerID,s.PropertyID,s.PropertyTypeID,s.PropertySizeID,s.PlanID,s.PaymentFor, c.* from tblssaleflat s inner join Customer c on s.CustomerID=c.CustomerID  INNER JOIN PlanType  ON s.PlanID = PlanType.PlanID where s.saleid='" + saleid + "'");
            var v = payment.AsEnumerable().ToList();
            return Json(new { Sale = (from s in v select new { SaleID = s["SaleID"], SaleRate = s["SaleRate"], ServiceTaxPer = s["ServiceTaxPer"], ServiceTaxAmount = s["ServiceTaxAmount"], SaleDate = s["SaleDate"], BookingAmount = s["BookingAmount"], CustomerName = s["AppTitle"] + " " + s["FName"] + " " + s["MName"] + " " + s["LName"], PName = s["PName"], Mobile = s["MobileNo"], EmailID = s["EmailID"], CustomerID = s["CustomerID"], PropertyID = s["PropertyID"], PropertyTypeID = s["PropertyTypeID"], PropertySizeID = s["PropertySizeID"], FlatID = s["FlatID"], PlanID = s["PlanID"], PlanType = s["PlanTypeName"] }) }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFlatSale(string flatid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var payment = obj.GetDataTable("Select s.SaleID,PlanType.PlanTypeName,s.FlatID,s.Aggrement,s.SaleDate,s.SaleRate,s.ServiceTaxPer,s.ServiceTaxAmount,s.BookingAmount,s.CustomerID,s.PropertyID,s.PropertyTypeID,s.PropertySizeID,s.PlanID,s.PaymentFor, c.* from tblssaleflat s inner join Customer c on s.CustomerID=c.CustomerID INNER JOIN PlanType  ON s.PlanID = PlanType.PlanID where s.FlatID='" + flatid + "'");
            var v = payment.AsEnumerable().ToList();
            return Json(new { Sale = (from s in v select new { SaleID = s["SaleID"], SaleRate = s["SaleRate"], ServiceTaxPer = s["ServiceTaxPer"], ServiceTaxAmount = s["ServiceTaxAmount"], SaleDate = s["SaleDate"], BookingAmount = s["BookingAmount"], CustomerName = s["AppTitle"] + " " + s["FName"] + " " + s["MName"] + " " + s["LName"], PName = s["PName"], Mobile = s["MobileNo"], EmailID = s["EmailID"], CustomerID = s["CustomerID"], PaymentFor = s["PaymentFor"], PropertyID = s["PropertyID"], PropertyTypeID = s["PropertyTypeID"], PropertySizeID = s["PropertySizeID"], FlatID = s["FlatID"], PlanID = s["PlanID"], PlanType = s["PlanTypeName"] }) }, JsonRequestBehavior.AllowGet);
        }

        // GET all Installments
        public JsonResult GetInstallment(string saleid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var installment = obj.GetDataTable("select  * from tblSInstallmentDetail where SaleID='" + saleid + "'");
            var v = installment.AsEnumerable().ToList();
            return Json(new { Installments = (from s in v select new { InstallmentID = s["InstallmentID"], InstallmentNo = s["InstallmentNo"], DueDate = s["DueDate"], DueAmount = s["DueAmount"], TotalAmount = s["TotalAmount"] }) }, JsonRequestBehavior.AllowGet);
        }
        public string GetCurrentDueAmount(string saleid,string date)
        {
            DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
            dtinfo.DateSeparator = "/";
            dtinfo.ShortDatePattern = "dd/MM/yyyy";
            DataTable todue = obj.GetDataTable("select  Sum(TotalAmount) as tol from tblSInstallmentDetail where SaleID='" + saleid + "' and DueDate<='"+Convert.ToDateTime(date,dtinfo)+"'");
            DataRow rw;
            if (todue.Rows.Count > 0)
            {
                rw = todue.Rows[0];
                return rw["tol"].ToString();
            }
            else return "0";
        }

        public string GetTotalCurrentDueAmount(string saleid, string date)
        {
            string toduamount ="0";
            DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
            dtinfo.DateSeparator = "/";
            dtinfo.ShortDatePattern = "dd/MM/yyyy";
            DataTable todue = obj.GetDataTable("select  Sum(TotalAmount) as tol from tblSInstallmentDetail where SaleID='" + saleid + "' and DueDate<='" + Convert.ToDateTime(date, dtinfo) + "'");
            DataRow rw;
            if (todue.Rows.Count > 0)
            {

                DataTable Tbldue = obj.GetDataTable("select sum(Amount) as paidamount  from dbo.tblSPayment where SaleID='" + saleid + "'");
                toduamount =Convert.ToString((Convert.ToDouble(todue.Rows[0]["tol"]) - Convert.ToDouble(Tbldue.Rows[0]["paidamount"])));
                return toduamount;
            }
            else return "0";
        }

        //select sum(TotalAmount) from dbo.tblSPayment where SaleID=950

        public string GetInstallmentForDue(string saleid)
        {
            List<FlatSaleModel> model = new List<FlatSaleModel>();
            //List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var installment = obj.GetDataTable("select  * from tblSInstallmentDetail where SaleID='" + saleid + "'");
            var v = installment.AsEnumerable().ToList();
            foreach (var install in v)
            {
                if (install["DueDate"].ToString() == "")
                {
                    model.Add(new FlatSaleModel { InstallmentID = install["InstallmentID"].ToString(), InstallmentNo = install["InstallmentNo"].ToString(),DueDate=null, DueDateST = null, DueAmount = Convert.ToDecimal(install["DueAmount"]), TotalAmount = Convert.ToDecimal(install["TotalAmount"]), EventName = Convert.ToString(install["EventName"]) });
                }
                else
                {
                    model.Add(new FlatSaleModel { InstallmentID = install["InstallmentID"].ToString(), InstallmentNo = install["InstallmentNo"].ToString(),DueDate= Convert.ToDateTime(install["DueDate"]), DueDateST = Convert.ToDateTime(install["DueDate"]).ToString("dd/MM/yyyy"), DueAmount = Convert.ToDecimal(install["DueAmount"]), TotalAmount = Convert.ToDecimal(install["TotalAmount"]), EventName = Convert.ToString(install["EventName"]) });
                }
               
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            //return Json(new { Installments = (from s in v select new { InstallmentID = s["InstallmentID"], InstallmentNo = s["InstallmentNo"], DueDate = s["DueDate"], DueAmount = s["DueAmount"], TotalAmount = s["TotalAmount"] }) }, JsonRequestBehavior.AllowGet);
        }


        public string InsertPaymentDetails(string InstallmentNo, string Saleid, string Flatname, string DueAmount, string DueDate, string PaymentMode, string ChequeNo, string ChequeDate, string BankName, string BankBranch, string Remarks, string PayDate, string Amtrcvdinwrds, string ReceivedAmount, string IsPrint, string IsEmailSent, string EmailTo, string CustomerName,string CustomerID,bool chkInterest)
        {
            try
            {
                decimal TotalreceivedAmount = Convert.ToDecimal(ReceivedAmount);
                decimal InterestAmount = 0;
                string PaymentNumber = "0";
                int MaxTransactionID = 0;

                MaxTransactionID = Convert.ToInt32(obj.GetMax("TransactionID", "tblSPayment")) + 1;
                bool IsPrintReceipt = Convert.ToBoolean(IsPrint);
                bool IsSentEmail = Convert.ToBoolean(IsEmailSent);

                bool bl = SavePayment(Convert.ToDecimal(DueAmount), Saleid, Flatname, InstallmentNo, InterestAmount, Convert.ToDecimal(ReceivedAmount), PaymentMode, Amtrcvdinwrds, PaymentNumber, MaxTransactionID, ChequeNo, ChequeDate, BankName, BankBranch, Remarks, PayDate, IsPrintReceipt, IsSentEmail, CustomerName,CustomerID,DueDate,chkInterest);
                
                PrintReceipt re = new PrintReceipt();
                ReceiptModel model = new ReceiptModel();
                model.TransactionID = MaxTransactionID;
                model.ToEmailID = EmailTo;
                string filename = "";
                if (IsPrintReceipt)
                {
                    filename = re.GenerateReceipt(model);
                }
                if (IsSentEmail && filename != "")
                {
                    string Subject = "Receipt Detail";
                    re.SendMailfinal("info@sbpgroups.in", Subject, model.ToEmailID, model.ToEmailID, filename);
                    // Send email
                }
                if (bl)
                    return filename.Trim('~');
                else return "No";
            }
            catch(Exception ex)
            {
                Helper h = new Helper();
                h.LogException(ex);
                return "No";
            }
            
        }
        protected bool SavePayment(decimal DueAmount, string Saleid, string FlatName, string InstallmentNo, decimal InterestAmount, decimal PayAmount, string PaymentMode, String Amtrcvdinwrds, String PaymentNo, int TransactionID, string ChequeNo, string ChequeDate, string BankName, string BankBranch, string Remarks, string PayDate, bool IsPrint, bool IsEmailSent, string CustomerName,string CustomerID,string DueDate,bool chkInterest)
        {
            try
            {
                System.Globalization.DateTimeFormatInfo dtinfo = new System.Globalization.DateTimeFormatInfo();
                dtinfo.ShortDatePattern = "dd/MM/yyyy";
                dtinfo.DateSeparator = "/";
                Hashtable htPayment = new Hashtable();
                htPayment.Add("InstallmentNo", InstallmentNo);
                htPayment.Add("SaleID", Convert.ToInt32(Saleid));
                htPayment.Add("PaymentDate", Convert.ToDateTime(PayDate, dtinfo));
                if (DueDate == "") DueDate = DateTime.Now.ToString("dd/MM/yyyy");
                htPayment.Add("DueDate", Convert.ToDateTime(DueDate, dtinfo));
                htPayment.Add("DueAmount", DueAmount);
                htPayment.Add("TotalAmount", DueAmount);
                htPayment.Add("Amount", PayAmount);
                htPayment.Add("PaymentMode", PaymentMode);
                if (PaymentMode == "Cash" || PaymentMode == "Transfer Entry")
                {
                    htPayment.Add("PaymentStatus", "Clear");
                }
                else
                {
                    htPayment.Add("PaymentStatus", "Pending");
                    htPayment.Add("ChequeNo", ChequeNo);
                    htPayment.Add("ChequeDate", Convert.ToDateTime(ChequeDate,dtinfo));
                    htPayment.Add("BankName", BankName);
                    htPayment.Add("BankBranch", BankBranch);
                }
                htPayment.Add("CustomerName", CustomerName);
                htPayment.Add("Remarks", Remarks);
                htPayment.Add("AmtRcvdinWords", Amtrcvdinwrds);
                htPayment.Add("Activity", "Add");
                htPayment.Add("PaymentNo", PaymentNo);
                htPayment.Add("IsReceipt", IsPrint);
                htPayment.Add("FlatName", FlatName);
                htPayment.Add("TransactionID", TransactionID);
                htPayment.Add("CustomerID",Convert.ToInt32(CustomerID));
                htPayment.Add("CreatedBy", User.Identity.Name);
                htPayment.Add("Interest", chkInterest);
                if (obj.ExecuteProcedure("Insert_Payment", htPayment))
                {
                    return true;
                }
                else return false;
            }
            catch(Exception ex)
            {
                Helper h = new Helper();
                h.LogException(ex);
                return false;
            }
        }
        public string InsertOtherPayments(string InstallmentNo, string Saleid, string Flatname, string PaymentMode, string ChequeNo, string ChequeDate, string BankName, string BankBranch, string Remarks, string PayDate, string Amtrcvdinwrds, string ReceivedAmount, string IsPrint, string IsEmailSent, string EmailTo, string CustomerName, string CustomerID)
        {
            try
            {
                decimal TotalreceivedAmount = Convert.ToDecimal(ReceivedAmount);
                string PaymentNumber = "0";
                string Msg = "";
                int MaxTransactionID = 0;
                MaxTransactionID = Convert.ToInt32(obj.GetMax("TransactionID", "tblSPaymentOthers")) + 1;
                bool IsPrintReceipt = Convert.ToBoolean(IsPrint);
                bool IsSentEmail = Convert.ToBoolean(IsEmailSent);

                try
                {
                    System.Globalization.DateTimeFormatInfo dtinfo = new System.Globalization.DateTimeFormatInfo();
                    dtinfo.ShortDatePattern = "dd/MM/yyyy";
                    dtinfo.DateSeparator = "/";
                    Hashtable htPayment = new Hashtable();
                    htPayment.Add("SaleID", Convert.ToInt32(Saleid));
                    htPayment.Add("InstallmentNo", InstallmentNo);
                    htPayment.Add("PaymentDate", Convert.ToDateTime(PayDate, dtinfo));
                    htPayment.Add("Amount",Convert.ToDecimal(ReceivedAmount));
                    htPayment.Add("PaymentMode", PaymentMode);
                    if (PaymentMode == "Cash" || PaymentMode == "Transfer Entry")
                    {
                        htPayment.Add("PaymentStatus", "Clear");
                    }
                    else
                    {
                        htPayment.Add("PaymentStatus", "Pending");
                        htPayment.Add("ChequeNo", ChequeNo);
                        htPayment.Add("ChequeDate", Convert.ToDateTime(ChequeDate, dtinfo));
                        htPayment.Add("BankName", BankName);
                        htPayment.Add("BankBranch", BankBranch);
                    }
                    htPayment.Add("CustomerName", CustomerName);
                    htPayment.Add("Remarks", Remarks);
                    htPayment.Add("AmtRcvdinWords", Amtrcvdinwrds);
                    htPayment.Add("Activity", "Add");
                    htPayment.Add("PaymentNo", PaymentNumber);
                    htPayment.Add("IsReceipt",  Convert.ToBoolean(IsPrint));
                    htPayment.Add("FlatName", Flatname);
                    htPayment.Add("TransactionID", MaxTransactionID);
                    htPayment.Add("CustomerID", Convert.ToInt32(CustomerID));
                    htPayment.Add("CreatedBy", User.Identity.Name);
                    if (obj.ExecuteProcedure("Insert_PaymentOther", htPayment))
                    {
                        Msg= "Payment Saved";
                    }
                    else
                        Msg= "No";
                }
                catch (Exception ex)
                {
                    Helper h = new Helper();
                    h.LogException(ex);
                    Msg= "Error in Payment Submission";
                }
                PrintReceipt re = new PrintReceipt();
                ReceiptModel model = new ReceiptModel();
                model.TransactionID = MaxTransactionID;
                model.ToEmailID = EmailTo;
                string filename = "";
                if (IsPrintReceipt)
                {
                    filename = re.GenerateReceiptOtherPayment(model, InstallmentNo);
                }
                if (IsSentEmail && filename != "")
                {
                    string Subject = "Receipt Detail";
                    re.SendMailfinal("info@sbpgroups.in", Subject, model.ToEmailID, model.ToEmailID, filename);
                    // Send email
                }
                return filename.Trim('~');
            }
            catch (Exception ex)
            {
                Helper h = new Helper();
                h.LogException(ex);
                return "No";
            }

        }
        public EmptyResult UpdatePaymentDetails(string TransactionID, string InstallmentNo, string Saleid, string Flatname, string DueAmount, string DueDate, string PaymentMode, string ChequeNo, string ChequeDate, string BankName, string BankBranch, string Remarks, string PayDate, string Amtrcvdinwrds, string ReceivedAmount, string IsPrint)
        {
            decimal TotalreceivedAmount = Convert.ToDecimal(ReceivedAmount);
            decimal InterestAmount = 0;
            string PaymentNumber = "0";
            bool IsPrintReceipt = false;
            if(IsPrint=="")
                IsPrintReceipt = false;
            else
             IsPrintReceipt = Convert.ToBoolean(IsPrint);
           // bool IsSentEmail = Convert.ToBoolean(IsEmailSent);

            if (!String.IsNullOrEmpty(ReceivedAmount))
            {
                UpdatePayment(Convert.ToDecimal(DueAmount), Saleid, Flatname, InstallmentNo, InterestAmount, Convert.ToDecimal(ReceivedAmount), PaymentMode, Amtrcvdinwrds, PaymentNumber, TransactionID, ChequeNo, ChequeDate, BankName, BankBranch, Remarks, PayDate, IsPrintReceipt);
            }
            return new EmptyResult();
        }
        protected void UpdatePayment(decimal DueAmount, string Saleid, string FlatName, string InstallmentNo, decimal InterestAmount, decimal PayAmount, string PaymentMode, String Amtrcvdinwrds, String PaymentNo, string TransactionID, string ChequeNo, string ChequeDate, string BankName, string BankBranch, string Remarks, string PayDate, bool IsPrint)
        {
            System.Globalization.DateTimeFormatInfo dtinfo = new System.Globalization.DateTimeFormatInfo();
            dtinfo.ShortDatePattern = "dd/MM/yyyy";
            dtinfo.DateSeparator = "/";
            Hashtable htPayment = new Hashtable();
            htPayment.Add("TransactionID",Convert.ToInt32(TransactionID));
            htPayment.Add("SaleID", Convert.ToInt32(Saleid));
            htPayment.Add("PaymentDate", Convert.ToDateTime(PayDate, dtinfo));
            htPayment.Add("Amount", PayAmount);
            htPayment.Add("PaymentMode", PaymentMode);
            if (PaymentMode == "Cash" || PaymentMode == "Transfer Entry")
            {
                htPayment.Add("PaymentStatus", "Clear");
            }
            else
            {
                htPayment.Add("PaymentStatus", "Pending");
                htPayment.Add("ChequeNo", ChequeNo);
                htPayment.Add("ChequeDate", Convert.ToDateTime(ChequeDate,dtinfo));
                htPayment.Add("BankName", BankName);
                htPayment.Add("BankBranch", BankBranch);
            }
           // htPayment.Add("CustomerName", "");
            htPayment.Add("Remarks", Remarks);
            htPayment.Add("AmtRcvdinWords", Amtrcvdinwrds);
            htPayment.Add("Activity", "Edit");
            htPayment.Add("UserName", User.Identity.Name);
            if (obj.ExecuteProcedure("Update_Payment", htPayment))
            {
                string S = "";
            }
        }

        public string UpdateClearanceDetails(string TransactionID, string ChargeAmount, string Remarks, string ClearanceDate,string IsBounce)
        {
            System.Globalization.DateTimeFormatInfo dtinfo = new System.Globalization.DateTimeFormatInfo();
            dtinfo.ShortDatePattern = "dd/MM/yyyy";
            dtinfo.DateSeparator = "/";
            Hashtable htPayment = new Hashtable();
            htPayment.Add("TransactionID", TransactionID);
            htPayment.Add("ChargeAmount", ChargeAmount);
            htPayment.Add("Remarks", Remarks);
            htPayment.Add("ModifyBy", User.Identity.Name);
            htPayment.Add("ClearanceDate",Convert.ToDateTime(ClearanceDate,dtinfo));
            htPayment.Add("IsBounce",IsBounce);
            if (obj.ExecuteProcedure("Update_Clearance", htPayment))
            {
                return "Yes";
            }
            return "No";
        }
        // Save Payment record.
        private void SavePaymentRecord(tblsPaymentRecord record)
        {
            using (dbSBPEntities1 context = new dbSBPEntities1())
            {
                context.tblsPaymentRecords.Add(record);
                context.SaveChanges();
            }
        }


        protected void SavePaymentDetail(decimal DueAmount, string SaleID, string flatname, string InstallmentNo, decimal InterestAmount, decimal PayAmount, string PaymentMode, String Amtrcvdinwrds, String PaymentNo, string TransactionID, string ChequeNo, string ChequeDate, string BankName, string BankBranch, string Remarks, string PayDate, bool IsPrint, bool IsEmailSent)
        {
            System.Globalization.DateTimeFormatInfo dtinfo = new System.Globalization.DateTimeFormatInfo();
            dtinfo.DateSeparator = "/";
            dtinfo.ShortDatePattern = "MM/dd/yyyy";

            Hashtable htPayment = new Hashtable();
            htPayment.Add("InstallmentNo", InstallmentNo);
            htPayment.Add("SaleID", SaleID);
            htPayment.Add("PaymentDate", Convert.ToDateTime(PayDate, dtinfo));
            htPayment.Add("DueAmount", DueAmount);
            htPayment.Add("TotalAmount", PayAmount);
            htPayment.Add("Amount", PayAmount);
            htPayment.Add("PaymentMode", PaymentMode);
            htPayment.Add("Remarks", Remarks);
            htPayment.Add("AmtRcvdinWords", Amtrcvdinwrds);
            htPayment.Add("PaymentNo", PaymentNo);
            htPayment.Add("IsReceipt", IsPrint);
            htPayment.Add("Activity", "Add");
            htPayment.Add("TransactionID", TransactionID);
            htPayment.Add("FlatName", flatname);

            if (PaymentMode == "1" || PaymentMode == "7")
            {
                htPayment.Add("PaymentStatus", "Clear");
            }
            else
            {

                htPayment.Add("PaymentStatus", "Pending");
                htPayment.Add("ChequeNo", ChequeNo);
                htPayment.Add("ChequeDate", Convert.ToDateTime(ChequeDate).Date);
                htPayment.Add("BankName", BankName);
                htPayment.Add("BankBranch", BankBranch);
            }

            //  htPayment.Add("CustomerName", lblCustomer.Text.Trim());

            if (obj.ExecuteProcedure("Insert_PaymentDetail", htPayment))
            {
                String MaxID = obj.GetMax("PaymentID", "tblSPaymentDetail");
                Session["MaxID"] = MaxID;
            }
        }

        public string SearchPayment(string PropertyID,string PropertyTypeID,string PropertySizeID,string PropertyTypeName,string search1,string searchby,string sortby,string datefrom,string dateto,string searchtext)
        {
            try
            {
                string proName = PropertyID;
                string proType = PropertyTypeID;
                string proSize = PropertySizeID;
                string protypename = PropertyTypeName;
                int pid = 0, ptype = 0, psize = 0;
                if (proName == "? undefined:undefined ?" || proName == "All") proName = "All"; else pid = Convert.ToInt32(proName);
                if (proType == "? undefined:undefined ?" || proType == "All") { proType = "All"; Session["PropertyName"] = "Property Name"; } else { ptype = Convert.ToInt32(proType); Session["PropertyName"] = protypename; }
                if (proSize == "? undefined:undefined ?" || proSize == "All") proSize = "All"; else psize = Convert.ToInt32(proSize);
                dbSBPEntities2 context = new dbSBPEntities2();
                string flat = search1;
                if (flat == "0") // Customer Search.
                {
                    #region Custom Search
                    string srch = search1;
                    string search = searchby;
                    string soryby = sortby;


                    DateTime datef = new DateTime();
                    DateTime datet = new DateTime();
                    // Date.
                    if (datefrom != "" && dateto != "")
                    {
                        datef = Convert.ToDateTime(datefrom);
                        datet = Convert.ToDateTime(dateto);
                    }
                    if (search == "All")
                    {
                        if (soryby == "All")
                        {
                            // Get this month records
                            datef = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                            datet = datef.AddMonths(1);
                            var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            List<PaymentModel> model1 = new List<PaymentModel>();
                            foreach (var v in model)
                            {
                                string bdate = "";
                                if (v.PaymentDate != null)
                                    bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                                model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID ,PaymentStatus=v.PaymentStatus});
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                        }
                        else
                        {
                            var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            List<PaymentModel> model1 = new List<PaymentModel>();
                            foreach (var v in model)
                            {
                                string bdate = "";
                                if (v.PaymentDate != null)
                                    bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                                model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID,PaymentStatus=v.PaymentStatus });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                        }
                    }
                    else if (search == "ReceiptNo")
                    {
                        if (soryby == "All")
                        {
                            var model = context.tblSPayments.Where(p => p.PaymentNo.Contains(searchtext)).OrderByDescending(o => o.PaymentID);
                            List<PaymentModel> model1 = new List<PaymentModel>();
                            foreach (var v in model)
                            {
                                string bdate = "";
                                if (v.PaymentDate != null)
                                    bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                                model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model1);

                        }
                        else
                        {
                            // Date.
                            var model = context.tblSPayments.Where(p => p.PaymentNo.Contains(searchtext) && p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            List<PaymentModel> model1 = new List<PaymentModel>();
                            foreach (var v in model)
                            {
                                string bdate = "";
                                if (v.PaymentDate != null)
                                    bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                                model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                        }
                    }
                   
                    else if (search == "FlatName")
                    {
                        if (soryby == "All")
                        {
                            var model = context.tblSPayments.Where(p => p.FlatName.Contains(searchtext)).OrderByDescending(o => o.PaymentID);
                            List<PaymentModel> model1 = new List<PaymentModel>();
                            foreach (var v in model)
                            {
                                string bdate = "";
                                if (v.PaymentDate != null)
                                    bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                                model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model1);

                        }
                        else
                        {
                            // Date.
                            var model = context.tblSPayments.Where(p => p.FlatName.Contains(searchtext) && p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            List<PaymentModel> model1 = new List<PaymentModel>();
                            foreach (var v in model)
                            {
                                string bdate = "";
                                if (v.PaymentDate != null)
                                    bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                                model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                        }
                    }
                    else if (search == "Customer Name")
                    {
                        if (soryby == "All")
                        {
                            var model = context.tblSPayments.Where(p => p.CustomerName.Contains(searchtext)).OrderByDescending(o => o.PaymentID);
                            List<PaymentModel> model1 = new List<PaymentModel>();
                            foreach (var v in model)
                            {
                                string bdate = "";
                                if (v.PaymentDate != null)
                                    bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                                model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                        }
                        else
                        {
                            // Date.
                            var model = context.tblSPayments.Where(p => p.CustomerName.Contains(searchtext) && p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            List<PaymentModel> model1 = new List<PaymentModel>();
                            foreach (var v in model)
                            {
                                string bdate = "";
                                if (v.PaymentDate != null)
                                    bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                                model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                        }
                    }
                    else if (search == "Cheque No")
                    {
                        if (soryby == "All")
                        {
                            var model = context.tblSPayments.Where(p => p.ChequeNo.Contains(searchtext)).OrderByDescending(o => o.PaymentID);
                            List<PaymentModel> model1 = new List<PaymentModel>();
                            foreach (var v in model)
                            {
                                string bdate = "";
                                if (v.PaymentDate != null)
                                    bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                                model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                        }
                        else
                        {
                            // Date.
                            var model = context.tblSPayments.Where(p => p.ChequeNo.Contains(searchtext) && p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                            List<PaymentModel> model1 = new List<PaymentModel>();
                            foreach (var v in model)
                            {
                                string bdate = "";
                                if (v.PaymentDate != null)
                                    bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                                model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                        }
                    }
                    else if (search == "This Month")
                    {
                        datef = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        datet = datef.AddMonths(1);
                        // Date.
                        var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                        List<PaymentModel> model1 = new List<PaymentModel>();
                        foreach (var v in model)
                        {
                            string bdate = "";
                            if (v.PaymentDate != null)
                                bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                            model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                    }
                    else if (search == "Last 7 Days")
                    {
                        datet = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        datef = datet.AddDays(-7);
                        // Date.
                        var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                        List<PaymentModel> model1 = new List<PaymentModel>();
                        foreach (var v in model)
                        {
                            string bdate = "";
                            if (v.PaymentDate != null)
                                bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                            model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                    }
                    else
                    {
                        // Get this month records
                        datef = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        datet = datef.AddMonths(1);
                        var model = context.tblSPayments.Where(p => p.PaymentDate >= datef && p.PaymentDate <= datet).OrderByDescending(o => o.PaymentID);
                        List<PaymentModel> model1 = new List<PaymentModel>();
                        foreach (var v in model)
                        {
                            string bdate = "";
                            if (v.PaymentDate != null)
                                bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                            model1.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.SaleID, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID, PaymentStatus = v.PaymentStatus });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model1);
                    }
                    #endregion
                }
                else
                {
                    // Property Search.
                    if (proName != "All" && proType != "All" && proSize != "All")
                    {
                        // Search by property name, type and size
                        var model1 = (from sale in
                                          context.tblSSaleFlats
                                      join cust in context.Customers on sale.CustomerID equals cust.CustomerID
                                      join pay in context.tblSPayments on sale.SaleID equals pay.SaleID
                                      where sale.PropertyID.Value == pid && sale.PropertyTypeID == ptype && sale.PropertySizeID == psize
                                      select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName = cust.AppTitle+" "+ cust.FName + " " + cust.LName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentNo = pay.PaymentNo, PaymentID = pay.PaymentID,PaymentStatus=pay.PaymentStatus }).AsEnumerable();
                        List<PaymentModel> model = new List<PaymentModel>();
                        foreach (var v in model1)
                        {
                            string bdate = "";
                            if (v.PaymentDate != null)
                                bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                            model.Add(new PaymentModel {PaymentDateSt=bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID,PaymentStatus=v.PaymentStatus });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (proName != "All" && proType != "All" && proSize == "All")
                    {
                        // Search by Name, and Type.
                        var model1 = (from sale in
                                          context.tblSSaleFlats
                                      join cust in context.Customers on sale.CustomerID equals cust.CustomerID
                                      join pay in context.tblSPayments on sale.SaleID equals pay.SaleID
                                      where sale.PropertyID.Value == pid && sale.PropertyTypeID == ptype
                                      select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName =cust.AppTitle+" "+ cust.FName + " " + cust.LName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentNo = pay.PaymentNo, PaymentID = pay.PaymentID, PaymentStatus = pay.PaymentStatus }).AsEnumerable();
                        List<PaymentModel> model = new List<PaymentModel>();
                        foreach (var v in model1)
                        {
                            string bdate = "";
                            if (v.PaymentDate != null)
                                bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                            model.Add(new PaymentModel {PaymentDateSt=bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID,PaymentStatus=v.PaymentStatus });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (proName != "All" && proType == "All" && proSize == "All")
                    {
                        // Search by name.
                        var model1 = (from sale in
                                          context.tblSSaleFlats join cust in context.Customers on sale.CustomerID equals cust.CustomerID
                                      join pay in context.tblSPayments on sale.SaleID equals pay.SaleID
                                      where sale.PropertyID.Value == pid
                                      select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName =cust.AppTitle+ " "+ cust.FName + " " + cust.LName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentNo = pay.PaymentNo, PaymentID = pay.PaymentID, PaymentStatus = pay.PaymentStatus }).AsEnumerable();
                        List<PaymentModel> model = new List<PaymentModel>();
                        foreach (var v in model1)
                        {
                            string bdate = "";
                            if (v.PaymentDate != null)
                                bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                            model.Add(new PaymentModel {PaymentDateSt=bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentNo = v.PaymentNo, PaymentID = v.PaymentID,PaymentStatus=v.PaymentStatus });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                }

                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
            catch (Exception ex)
            {
                dbSBPEntities2 context = new dbSBPEntities2();

                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        public string EditSearchPayment(string PropertyID,string searchtext)
        {
            try
            {
                string proName = PropertyID;
               
                int pid = 0, ptype = 0, psize = 0;
                if (proName == "? undefined:undefined ?" || proName == "All") proName = "All"; else pid = Convert.ToInt32(proName);
                dbSBPEntities2 context = new dbSBPEntities2();
                // Search by name.
                var model1 = (from sale in
                                  context.tblSSaleFlats join cust in context.Customers on sale.CustomerID equals cust.CustomerID
                              join pay in context.tblSPayments on sale.SaleID equals pay.SaleID
                              where sale.PropertyID.Value == pid && pay.FlatName==searchtext
                              select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName =pay.CustomerName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentID = pay.PaymentID, InstallmentNo = pay.InstallmentNo, PaymentNo = pay.PaymentNo,PaymentStatus=pay.PaymentStatus,ChequeDate=pay.ChequeDate,ChequeNo=pay.ChequeNo,BankName=pay.BankName,BranchName=pay.BankBranch, CreatedBy=pay.CreatedBy}).AsEnumerable();
                List<PaymentModel> model = new List<PaymentModel>();
                foreach (var v in model1)
                {
                    string bdate = "";
                    if (v.PaymentDate != null)
                        bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                    string cdate = "";
                    if (v.ChequeDate!= null)
                        cdate = Convert.ToDateTime(v.ChequeDate).ToString("dd/MM/yyyy");
                    // Set Color
                    if (v.InstallmentNo == "Advance Booking Amount")
                        model.Add(new PaymentModel {PaymentDateSt=bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentID = v.PaymentID, PaymentNo = v.PaymentNo, InstallmentNo = "red" ,PaymentStatus=v.PaymentStatus, ChequeDateSt=cdate,BankName=v.BankName,BankBranch=v.BranchName,ChequeNo=v.ChequeNo, CreatedBy=v.CreatedBy});
                    else
                        model.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentID = v.PaymentID, PaymentNo = v.PaymentNo, InstallmentNo = "white", PaymentStatus = v.PaymentStatus, ChequeDateSt = cdate, BankName = v.BankName, BankBranch = v.BranchName, ChequeNo = v.ChequeNo, CreatedBy = v.CreatedBy });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
            catch (Exception ex)
            {
                dbSBPEntities2 context = new dbSBPEntities2();

                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        public string SearchPaymentBySaleID(string saleid)
        {
            try
            {
                int? sid=Convert.ToInt32(saleid);

                int pid = 0, ptype = 0, psize = 0;
                dbSBPEntities2 context = new dbSBPEntities2();
                // Search by name.
                var model1 = (from sale in
                                  context.tblSSaleFlats
                              join cust in context.Customers on sale.CustomerID equals cust.CustomerID
                              join pay in context.tblSPayments on sale.SaleID equals pay.SaleID
                              where sale.SaleID==sid
                              select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName = pay.CustomerName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentID = pay.PaymentID, InstallmentNo = pay.InstallmentNo, PaymentNo = pay.PaymentNo, PaymentStatus = pay.PaymentStatus, ChequeDate = pay.ChequeDate, ChequeNo = pay.ChequeNo, BankName = pay.BankName, BranchName = pay.BankBranch, CreatedBy = pay.CreatedBy }).AsEnumerable();
                List<PaymentModel> model = new List<PaymentModel>();
                foreach (var v in model1)
                {
                    string bdate = "";
                    if (v.PaymentDate != null)
                        bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                    string cdate = "";
                    if (v.ChequeDate != null)
                        cdate = Convert.ToDateTime(v.ChequeDate).ToString("dd/MM/yyyy");
                    // Set Color
                    if (v.InstallmentNo == "Advance Booking Amount")
                        model.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentID = v.PaymentID, PaymentNo = v.PaymentNo, InstallmentNo = "red", PaymentStatus = v.PaymentStatus, ChequeDateSt = cdate, BankName = v.BankName, BankBranch = v.BranchName, ChequeNo = v.ChequeNo, CreatedBy = v.CreatedBy });
                    else
                        model.Add(new PaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentID = v.PaymentID, PaymentNo = v.PaymentNo, InstallmentNo = "white", PaymentStatus = v.PaymentStatus, ChequeDateSt = cdate, BankName = v.BankName, BankBranch = v.BranchName, ChequeNo = v.ChequeNo, CreatedBy = v.CreatedBy });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
            catch (Exception ex)
            {
                dbSBPEntities2 context = new dbSBPEntities2();

                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
        #endregion


       
    }
}