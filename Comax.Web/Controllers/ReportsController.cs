﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Comax.Web.Models;
using Comax.Web.Code;

namespace Comax.Web.Controllers
{
    
    public class ReportsController : Controller
    {
        //
        // GET: /Reports/
        DataFunctions obj = new DataFunctions();
        
        dbSBPEntities1 SbpEntity = new dbSBPEntities1();
         [MyAuthorize]
        public ActionResult FlatWisePendingPayments()
        {
            return View();
        }

        public JsonResult GetProperty()
        {
            int Uid = 1;
            return Json(SbpEntity.NUsp_GetProperty(Uid).ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFlatWisePendingPayment(int pID, string dt)
        {
            DateTime dtt = new DateTime();
            dtt = Convert.ToDateTime(dt);
            int i=0;
            return Json(SbpEntity.NGet_PendingPayment1(pID, dtt,i).ToList(), JsonRequestBehavior.AllowGet);
        }
        //public JsonResult GetFlatWisePendingPayment(int pID, string dt)
        //{
        //    Hashtable ht = new Hashtable();
        //    DataTable dtb = new DataTable();             
        //        ht.Add("DueDate",dt);
        //        ht.Add("PID", Convert.ToInt32( pID));

        //        dtb=obj.GetDataTableFromProcedure("NGet_PendingPaymentold", ht);

        //        List<NGet_PendingPaymentOld_Result> customers = new List<NGet_PendingPaymentOld_Result>();
        //    if(dtb.Rows.Count==0)
        //        dtb = obj.GetDataTableFromProcedure("NGet_PendingPayment1", ht);
        //    if (dtb.Rows.Count == 0)
        //        dtb = obj.GetDataTableFromProcedure("NGet_PendingPayment1", ht);
        //        if (dtb.Rows.Count > 0)
        //        {
        //            for(int i=0;i<dtb.Rows.Count;i++)
        //            {
        //                NGet_PendingPaymentOld_Result customer = new NGet_PendingPaymentOld_Result();
        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["Address"])))
        //                customer.Address =Convert.ToString( dtb.Rows[i]["Address"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["Address1"])))
        //                    customer.Address1 = Convert.ToString(dtb.Rows[i]["Address1"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["Address2"])))
        //                    customer.Address2 = Convert.ToString(dtb.Rows[i]["Address2"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["BlockName"])))
        //                    customer.BlockName = Convert.ToString(dtb.Rows[i]["BlockName"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["City"])))
        //                    customer.City = Convert.ToString(dtb.Rows[i]["City"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["CompanyAddress"])))
        //                    customer.CompanyAddress = Convert.ToString(dtb.Rows[i]["CompanyAddress"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["CompanyLocation"])))
        //                    customer.CompanyLocation = Convert.ToString(dtb.Rows[i]["CompanyLocation"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["CompanyName"])))
        //                    customer.CompanyName = Convert.ToString(dtb.Rows[i]["CompanyName"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["dRmdCnt"])))
        //                customer.dRmdCnt = Convert.ToDateTime(dtb.Rows[i]["dRmdCnt"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["Dueamount"])))
        //                customer.Dueamount = Convert.ToDecimal(dtb.Rows[i]["Dueamount"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["DueDate"])))
        //                customer.DueDate = Convert.ToDateTime(dtb.Rows[i]["DueDate"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["FlatID"])))
        //                customer.FlatID = Convert.ToInt32(dtb.Rows[i]["FlatID"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["Flatname"])))
        //                customer.Flatname = Convert.ToString(dtb.Rows[i]["Flatname"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["FloorName"])))
        //                customer.FloorName = Convert.ToString(dtb.Rows[i]["FloorName"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["ID"])))
        //                customer.ID = Convert.ToInt32(dtb.Rows[i]["ID"]);
        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["IsCommercial"])))
        //                customer.IsCommercial = Convert.ToInt32(dtb.Rows[i]["IsCommercial"]);


        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["LastRemDate"])))
        //                customer.LastRemDate = Convert.ToDateTime(dtb.Rows[i]["LastRemDate"]);
        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["LDuedate"])))
        //                customer.LDuedate = Convert.ToDateTime(dtb.Rows[i]["LDuedate"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["Location"])))
        //                customer.Location = Convert.ToString(dtb.Rows[i]["Location"]);
        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["Mobileno"])))
        //                customer.Mobileno = Convert.ToString(dtb.Rows[i]["Mobileno"]);



        //                    if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["Owner"])))
        //                customer.Owner = Convert.ToString(dtb.Rows[i]["Owner"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["PaidAmount"])))
        //                customer.PaidAmount = Convert.ToDecimal(dtb.Rows[i]["PaidAmount"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["PID"])))
        //                customer.PID = Convert.ToInt32(dtb.Rows[i]["PID"]);
        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["PinCode"])))
        //                customer.PinCode = Convert.ToInt32(dtb.Rows[i]["PinCode"]);
        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["PName"])))
        //                customer.PName = Convert.ToString(dtb.Rows[i]["PName"]);


        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["ReminderCount"])))
        //                customer.ReminderCount = Convert.ToInt32(dtb.Rows[i]["ReminderCount"]);
        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["ReminderDate1"])))
        //                customer.ReminderDate1 = Convert.ToDateTime(dtb.Rows[i]["ReminderDate1"]);
        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["ReminderDate2"])))
        //                customer.ReminderDate2 = Convert.ToDateTime(dtb.Rows[i]["ReminderDate2"]);
        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["ReminderDate3"])))
        //                customer.ReminderDate3 = Convert.ToDateTime(dtb.Rows[i]["ReminderDate3"]);
        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["SaleDate"])))
        //                customer.SaleDate = Convert.ToDateTime(dtb.Rows[i]["SaleDate"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["SaleID"])))
        //                customer.SaleID = Convert.ToInt32(dtb.Rows[i]["SaleID"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["Salerate"])))
        //                customer.Salerate = Convert.ToDecimal(dtb.Rows[i]["Salerate"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["State"])))
        //                customer.State = Convert.ToString(dtb.Rows[i]["State"]);

        //                if (!string.IsNullOrEmpty(Convert.ToString(dtb.Rows[i]["Towername"])))
        //                customer.Towername = Convert.ToString(dtb.Rows[i]["Towername"]);
        //                customers.Add(customer);
        //            }
        //        }




        //        return Json(customers, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult ExportToExcel(string PID, string dt)
        {
            IEnumerable<NGet_PendingPayment1_Result> item1 = null;
            Int32 i=0;
            //PropertyNameList item2 = new PropertyNameList();
            //item1 = SbpEntity.NGet_PendingPayment1(Convert.ToInt32(PID), Convert.ToDateTime(dt));
            //System.Tuple<IEnumerable<NGet_PendingPayment1_Result>, PropertyNameList> PropertyResultEntity = Tuple.Create(item1, item2);
            var grid = new GridView();
            var dataSource = SbpEntity.NGet_PendingPayment1(Convert.ToInt32(PID), Convert.ToDateTime(dt),i);
            var nGet_PendingPayment1_Result = from eventsList in dataSource

                                              select new
                                              {
                                                  Owner = eventsList.Owner,
                                                  Towername = eventsList.Towername,
                                                  Flatname = eventsList.Flatname,
                                                  Salerate = eventsList.Salerate,
                                                  PaidAmount = eventsList.PaidAmount,
                                                  Dueamount = eventsList.Dueamount,
                                                  BlockName = eventsList.BlockName,
                                                  ReminderCount = eventsList.ReminderCount,
                                              };

            grid.DataSource = nGet_PendingPayment1_Result;
            grid.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=ExcelFile.xls");
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return View(Response);
        }
    }

}