﻿using AutoMapper;
using Comax.Web.Areas.Installment.Models;
using Comax.Web.Code;
using Comax.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Comax.Web.Controllers
{
   
    public class OtherPaymentController : BaseController
    {
        #region ActionView Method

        DataFunctions obj = new DataFunctions();
        // GET: OtherPayment
        [MyAuthorize]
        public ActionResult Index()
        {
            Session["PropertyName"] = "PropertyName";
            return View();
        }
        [MyAuthorize]
        public ActionResult Clearance(int id)
        {
            ViewBag.ID = id;
            return View();
        }

        [MyAuthorize]
        public ActionResult Details(int id)
        {
            ViewBag.ID = id;
            return View();
        }
        [MyAuthorize]
        public ActionResult Edit(int id)
        {
            ViewBag.ID = id;
            return View();
        }

        [MyAuthorize]
        public ActionResult ViewCancelPayments()
        {
            return View();
        }
        [MyAuthorize]
        public ActionResult PaymentCancel(int id)
        {
            ViewBag.TransactionID = id;
            dbSBPEntities2 context = new dbSBPEntities2();
            var model = context.tblSPaymentOtherCancels.Where(p => p.TransactionID == id).FirstOrDefault();
            PaymentCancelModel m = new PaymentCancelModel();
            var pay = context.tblSPaymentOthers.Where(py => py.TransactionID == id).FirstOrDefault();
            ViewBag.ReceiptNo = pay.PaymentNo;
            if (model == null)
            {

                m.TransactionID = id;
                m.Status = "New Payment";
                m.Amount = pay.Amount;
                m.FlatName = pay.FlatName;
                m.CustomerName = pay.CustomerName;
                ViewBag.Status = "Cancel Payment";
            }
            else
            {
                Mapper.CreateMap<tblSPaymentOtherCancel, PaymentCancelModel>();
                m = Mapper.Map<tblSPaymentOtherCancel, PaymentCancelModel>(model);
                m.TransactionID = id;
                if (m.Status != "Canceled")
                {
                    ViewBag.Status = "Cancel Payment";
                    m.Amount = pay.Amount;
                }
                else
                {
                    ViewBag.Status = "Undo Cancel Payment";
                }
                m.FlatName = pay.FlatName;
                m.CustomerName = pay.CustomerName;
            }
            return View(m);
        }

        [MyAuthorize]
        public ActionResult RefundPayment(int id)
        {
            ViewBag.ID = id;
            return View();
        }
        [MyAuthorize]
        public ActionResult BackupReceipt()
        {
            Session["propertyName"] = "Property Name";
            return View();
        }

        public ActionResult BackupReceiptPrintAction(string id)
        {
            ViewBag.ID = id;
            return View();
        }

        public ActionResult BackupReceiptPrintDataAction(string id)
        {
            ViewBag.ID = id;
            return View();
        }


        #endregion

        #region JSON Services
        public string EditSearchPayment(string PropertyID, string searchtext, string paymentfor)
        {
            try
            {
                string proName = PropertyID;

                int pid = 0, ptype = 0, psize = 0;
                if (proName == "? undefined:undefined ?" || proName == "All") proName = "All"; else pid = Convert.ToInt32(proName);
                dbSBPEntities2 context = new dbSBPEntities2();
                // Search by name.
                List<OtherPaymentModel> model = new List<OtherPaymentModel>();

                if (paymentfor == "All")
                {
                    var model1 = (from sale in
                                      context.tblSSaleFlats
                                  join pay in context.tblSPaymentOthers on sale.SaleID equals pay.SaleID
                                  where sale.PropertyID.Value == pid && pay.FlatName.Contains(searchtext)
                                  select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName = sale.FName + " " + sale.LName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentID = pay.PaymentOtherID, PaymentFor = pay.PaymentFor, PaymentNo = pay.PaymentNo, PaymentStatus = pay.PaymentStatus }).AsEnumerable();
                    foreach (var v in model1)
                    {
                        string bdate = "";
                        if (v.PaymentDate != null)
                            bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                        // Set Color
                        model.Add(new OtherPaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentOtherID = v.PaymentID, PaymentFor = v.PaymentFor, PaymentNo = v.PaymentNo, PaymentStatus = v.PaymentStatus });
                    }
                }
                else
                {
                    var model1 = (from sale in
                                      context.tblSSaleFlats
                                  join pay in context.tblSPaymentOthers on sale.SaleID equals pay.SaleID
                                  where sale.PropertyID.Value == pid && pay.FlatName.Contains(searchtext) && pay.PaymentFor.Contains(paymentfor)
                                  select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName = sale.FName + " " + sale.LName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentID = pay.PaymentOtherID, PaymentFor = pay.PaymentFor, PaymentNo = pay.PaymentNo, PaymentStatus = pay.PaymentStatus }).AsEnumerable();
                    foreach (var v in model1)
                    {
                        string bdate = "";
                        if (v.PaymentDate != null)
                            bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                        // Set Color
                        model.Add(new OtherPaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentOtherID = v.PaymentID, PaymentFor = v.PaymentFor, PaymentNo = v.PaymentNo, PaymentStatus = v.PaymentStatus });
                    }
                }

                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
            catch (Exception ex)
            {
                dbSBPEntities2 context = new dbSBPEntities2();

                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }

        public string UpdateClearanceDetails(string TransactionID, string ChargeAmount, string Remarks, string ClearanceDate, string IsBounce)
        {
            System.Globalization.DateTimeFormatInfo dtinfo = new System.Globalization.DateTimeFormatInfo();
            dtinfo.ShortDatePattern = "dd/MM/yyyy";
            dtinfo.DateSeparator = "/";
            Hashtable htPayment = new Hashtable();
            htPayment.Add("TransactionID", TransactionID);
            htPayment.Add("ChargeAmount", ChargeAmount);
            htPayment.Add("Remarks", Remarks);
            htPayment.Add("ModifyBy", User.Identity.Name);
            htPayment.Add("ClearanceDate", Convert.ToDateTime(ClearanceDate, dtinfo));
            htPayment.Add("IsBounce", IsBounce);
            if (obj.ExecuteProcedure("Update_ClearanceOther", htPayment))
            {
                return "Yes";
            }
            return "No";
        }

        public EmptyResult UpdatePaymentDetails(string TransactionID, string InstallmentNo, string Saleid, string Flatname, string DueAmount, string DueDate, string PaymentMode, string ChequeNo, string ChequeDate, string BankName, string BankBranch, string Remarks, string PayDate, string Amtrcvdinwrds, string ReceivedAmount, string IsPrint)
        {
            decimal TotalreceivedAmount = Convert.ToDecimal(ReceivedAmount);
            decimal totAmt = 0;
            decimal PayAmt = 0;
            decimal InterestAmount = 0;
            string PaymentNumber = "0";
            decimal TotalAmount = 0;

            bool IsPrintReceipt = false;
            if (IsPrint == "")
                IsPrintReceipt = false;
            else
                IsPrintReceipt = Convert.ToBoolean(IsPrint);
            // bool IsSentEmail = Convert.ToBoolean(IsEmailSent);

            if (!String.IsNullOrEmpty(ReceivedAmount))
            {
                UpdatePayment(Convert.ToDecimal(DueAmount), Saleid, Flatname, InstallmentNo, InterestAmount, Convert.ToDecimal(ReceivedAmount), PaymentMode, Amtrcvdinwrds, PaymentNumber, TransactionID, ChequeNo, ChequeDate, BankName, BankBranch, Remarks, PayDate, IsPrintReceipt);
            }
            return new EmptyResult();
        }
        protected void UpdatePayment(decimal DueAmount, string Saleid, string FlatName, string InstallmentNo, decimal InterestAmount, decimal PayAmount, string PaymentMode, String Amtrcvdinwrds, String PaymentNo, string TransactionID, string ChequeNo, string ChequeDate, string BankName, string BankBranch, string Remarks, string PayDate, bool IsPrint)
        {
            System.Globalization.DateTimeFormatInfo dtinfo = new System.Globalization.DateTimeFormatInfo();
            dtinfo.ShortDatePattern = "dd/MM/yyyy";
            dtinfo.DateSeparator = "/";
            Hashtable htPayment = new Hashtable();
            htPayment.Add("TransactionID", Convert.ToInt32(TransactionID));
            htPayment.Add("SaleID", Convert.ToInt32(Saleid));
            htPayment.Add("PaymentDate", Convert.ToDateTime(PayDate, dtinfo));
            htPayment.Add("Amount", PayAmount);
            htPayment.Add("PaymentMode", PaymentMode);
            if (PaymentMode == "Cash" || PaymentMode == "Transfer Entry")
            {
                htPayment.Add("PaymentStatus", "1");
            }
            else
            {
                htPayment.Add("PaymentStatus", "2");
                htPayment.Add("ChequeNo", ChequeNo);
                htPayment.Add("ChequeDate", Convert.ToDateTime(ChequeDate, dtinfo));
                htPayment.Add("BankName", BankName);
                htPayment.Add("BankBranch", BankBranch);
            }
            // htPayment.Add("CustomerName", "");
            htPayment.Add("Remarks", Remarks);
            htPayment.Add("AmtRcvdinWords", Amtrcvdinwrds);
            htPayment.Add("Activity", "Edit");
            htPayment.Add("UserName", User.Identity.Name);
            if (obj.ExecuteProcedure("Update_PaymentOther", htPayment))
            {
                string S = "";
            }
        }

        public string PaymentCancelSearch(string propertyName, string search, string propertyid, string propertySubTypeID, string proSize, string datefrom, string dateto, string searchtext)
        {
            try
            {
                DateTime datef = new DateTime();
                DateTime datet = new DateTime();

                // Date.
                if (datefrom != "" && dateto != "")
                {
                    datef = Convert.ToDateTime(datefrom);
                    datet = Convert.ToDateTime(dateto);
                }
                else
                {
                    datef = DateTime.Now.AddMonths(-1);
                    datet = DateTime.Now;
                }

                if (propertyid == "? undefined:undefined ?" || propertyid == "All" || propertyid == "") propertyid = "0";
                if (propertySubTypeID == "? undefined:undefined ?" || propertySubTypeID == "All" || propertySubTypeID == "") { propertySubTypeID = "0"; Session["propertyName"] = "Property Name"; }
                else { Session["propertyName"] = propertyName; }
                if (proSize == "" || proSize == "? undefined:undefined ?" || proSize == "All") proSize = "0";
                int pid = Convert.ToInt32(propertyid);
                int ptypeid = Convert.ToInt32(propertySubTypeID);
                int psize = Convert.ToInt32(proSize);
                dbSBPEntities2 context = new dbSBPEntities2();
                if (propertyid == "0") // All Properties
                {
                    if (search == "All")
                    {
                        var md = (from pay in context.tblSPaymentOthers join can in context.tblSPaymentOtherCancels on pay.TransactionID equals can.TransactionID where can.CancelDate >= datef && can.CancelDate <= datet select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        // By default showing last one month sales in all properties
                    }
                    else if (search == "SubType")
                    {
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyTypeID == ptypeid select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);

                        //var model = context.tblSSaleFlats.Where(p => p.SaleDate >= datef && p.SaleDate <= datet).OrderBy(o => o.SaleID);
                        //return View(model);
                    }
                    else if (search == "FlatName")
                    {
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where pay.FlatName.Contains(searchtext) select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "Customer Name")
                    {
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where pay.CustomerName.Contains(searchtext) select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "CancelDate")
                    {

                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);

                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where can.CancelDate >= dtFrom && can.CancelDate <= dtTo select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "PaymentDate")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "This Month")
                    {
                        DateTime dtFrom = DateTime.Now.AddMonths(-1);
                        DateTime dtTo = DateTime.Now;

                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where can.CancelDate >= dtFrom && can.CancelDate <= dtTo select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "Last 7 Days")
                    {

                        DateTime dtFrom = DateTime.Now.AddDays(-7);
                        DateTime dtTo = DateTime.Now;

                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                }
                else // Search by Property id
                {
                    if (search == "All")
                    {
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "SubType")
                    {
                        if (ptypeid != 0) // With PropertyType
                        {
                            if (psize == 0) // all Sizes
                            {
                                var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                                List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                                foreach (var v in md)
                                {
                                    string CDate = "", UCDate = "";
                                    if (v.cancel.CancelDate != null)
                                        CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                                    if (v.cancel.UnCancelDate != null)
                                        UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                                    model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                                }
                                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            }
                            else
                            {
                                var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && sale.PropertySizeID == psize select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                                List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                                foreach (var v in md)
                                {
                                    string CDate = "", UCDate = "";
                                    if (v.cancel.CancelDate != null)
                                        CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                                    if (v.cancel.UnCancelDate != null)
                                        UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                                    model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                                }
                                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            }
                        }
                        else
                        {
                            // All Types of property
                            if (psize == 0) // All Sizes
                            {
                                var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                                List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                                foreach (var v in md)
                                {
                                    string CDate = "", UCDate = "";
                                    if (v.cancel.CancelDate != null)
                                        CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                                    if (v.cancel.UnCancelDate != null)
                                        UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                                    model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                                }
                                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            }
                            else
                            {
                                var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                                List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                                foreach (var v in md)
                                {
                                    string CDate = "", UCDate = "";
                                    if (v.cancel.CancelDate != null)
                                        CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                                    if (v.cancel.UnCancelDate != null)
                                        UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                                    model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                                }
                                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            }

                        }
                    }
                    else if (search == "FlatName")
                    {
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.FlatName.Contains(searchtext) select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "Customer Name")
                    {
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.CustomerName.Contains(searchtext) select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "CancelDate")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid && can.CancelDate >= dtFrom && can.CancelDate <= dtTo select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "PaymentDate")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "This Month")
                    {
                        DateTime dtFrom = DateTime.Now.AddMonths(-1);
                        DateTime dtTo = DateTime.Now;
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid && can.CancelDate >= dtFrom && can.CancelDate <= dtTo select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "Last 7 Days")
                    {
                        DateTime dtFrom = DateTime.Now.AddDays(-7);
                        DateTime dtTo = DateTime.Now;
                        var md = (from can in context.tblSPaymentOtherCancels join pay in context.tblSPaymentOthers on can.TransactionID equals pay.TransactionID join sale in context.tblSSaleFlats on can.SaleID equals sale.SaleID where sale.PropertyID == pid && can.CancelDate >= dtFrom && can.CancelDate <= dtTo select new { cancel = can, pay = pay, FlatName = pay.FlatName });
                        List<PaymentCancelModel> model = new List<PaymentCancelModel>();
                        foreach (var v in md)
                        {
                            string CDate = "", UCDate = "";
                            if (v.cancel.CancelDate != null)
                                CDate = v.cancel.CancelDate.Value.ToString("dd/MM/yyyy");
                            if (v.cancel.UnCancelDate != null)
                                UCDate = v.cancel.UnCancelDate.Value.ToString("dd/MM/yyyy");
                            model.Add(new PaymentCancelModel { CancelDateSt = CDate, UnCancelDateSt = UCDate, PaymentCancelID = v.cancel.PaymentOtherCancelID, FlatName = v.FlatName, TransactionID = v.pay.TransactionID, SaleID = v.cancel.SaleID, Amount = v.cancel.Amount, CancelDate = v.cancel.CancelDate, UnCancelDate = v.cancel.UnCancelDate, Remarks = v.cancel.Remarks, UnCancelRemark = v.cancel.UnCancelRemark, CancelBy = v.cancel.CancelBy, UnCancelBy = v.cancel.UnCancelBy, Status = v.cancel.Status, CustomerName = v.pay.CustomerName });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
            catch (Exception ex)
            {
                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }

        public string PaymentCancelSave(tblSPaymentOtherCancel model)
        {
            using (dbSBPEntities2 context = new dbSBPEntities2())
            {
                try
                {
                    //var payment= context.tblSPaymentCancels.Where(p => p.TransactionID == model.TransactionID).FirstOrDefault();
                    //if(payment!=null)  // Check it is already Canceled or not.
                    //{
                    if (model.Status == "Canceled") // Undo Cancel
                    {
                        DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
                        var payment = context.tblSPaymentOthers.Where(p => p.TransactionID == model.TransactionID).FirstOrDefault();
                        var cpayment = context.tblSPaymentOtherCancels.Where(p => p.TransactionID == model.TransactionID).FirstOrDefault();
                        cpayment.UnCancelBy = User.Identity.Name;
                        cpayment.UnCancelDate = model.CancelDate;
                        cpayment.UnCancelRemark = model.Remarks;
                        cpayment.Status = "Uncanceled";// Canceled/UnCanceled

                        context.Entry(cpayment).State = EntityState.Modified;
                        int i = context.SaveChanges();
                        if (i > 0)
                        {
                            payment.Amount = cpayment.Amount;
                            payment.Activity = "Uncanceled";
                            context.Entry(payment).State = EntityState.Modified;
                            i = context.SaveChanges();
                            if (i > 0)
                                return "Uncanceled";
                            else return "No";
                        }
                        //}
                        return "No";
                    }
                    else if (model.Status == "Uncanceled") //Cancel Existing Transaction.
                    {
                        DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
                        var payment = context.tblSPaymentOthers.Where(p => p.TransactionID == model.TransactionID).FirstOrDefault();
                        var cpayment = context.tblSPaymentOtherCancels.Where(p => p.TransactionID == model.TransactionID).FirstOrDefault();
                        cpayment.CancelBy = User.Identity.Name;
                        cpayment.CancelDate = model.CancelDate;
                        cpayment.Remarks = model.Remarks;
                        cpayment.Status = "Canceled";// Canceled/UnCanceled

                        context.Entry(cpayment).State = EntityState.Modified;
                        int i = context.SaveChanges();
                        if (i > 0)
                        {
                            payment.Amount = 0;
                            payment.Activity = "Canceled";
                            context.Entry(payment).State = EntityState.Modified;
                            i = context.SaveChanges();
                            if (i > 0)
                                return "Canceled";
                            else return "No";
                        }
                        //}
                        return "No";
                    }
                    else // Add New Installment.
                    {
                        DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
                        var payment = context.tblSPaymentOthers.Where(p => p.TransactionID == model.TransactionID).FirstOrDefault();
                        model.SaleID = payment.SaleID;
                        model.Amount = payment.Amount;
                        model.Status = "Canceled";// Canceled/UnCanceled
                        model.CancelBy = User.Identity.Name;
                        context.tblSPaymentOtherCancels.Add(model);
                        int i = context.SaveChanges();
                        if (i > 0)
                        {
                            payment.Amount = 0;
                            payment.Activity = "Canceled";
                            context.Entry(payment).State = EntityState.Modified;
                            i = context.SaveChanges();
                            if (i > 0)
                                return "Canceled";
                            else return "No";
                        }
                        //}
                        return "No";
                    }
                }
                catch (Exception ex)
                {
                    Helper h = new Helper();
                    h.LogExceptionNo(ex, "AIMC1", User.Identity.Name);
                    return "No";
                }
            }
        }

        #endregion

        #region Backup Receipt Other Paymnet

        public string SearchBackupReceipt(string propertyName, string search, string propertyid, string propertySubTypeID, string proSize, string datefrom, string dateto, string searchtext, string paymenttype)
        {
            try
            {
                DateTime datef = new DateTime();
                DateTime datet = new DateTime();

                // Date.
                if (datefrom != "" && dateto != "")
                {
                    datef = Convert.ToDateTime(datefrom);
                    datet = Convert.ToDateTime(dateto);
                }
                else
                {
                    datef = DateTime.Now.AddMonths(-1);
                    datet = DateTime.Now;
                }

                if (paymenttype == "? undefined:undefined ?" || paymenttype == "All" || paymenttype == "") paymenttype = "All";
                if (propertyid == "? undefined:undefined ?" || propertyid == "All" || propertyid == "") propertyid = "0";
                if (propertySubTypeID == "? undefined:undefined ?" || propertySubTypeID == "All" || propertySubTypeID == "") { propertySubTypeID = "0"; Session["propertyName"] = "Property Name"; }
                else { Session["propertyName"] = propertyName; }
                if (proSize == "" || proSize == "? undefined:undefined ?" || proSize == "All") proSize = "0";
                int pid = Convert.ToInt32(propertyid);
                int ptypeid = Convert.ToInt32(propertySubTypeID);
                int psize = Convert.ToInt32(proSize);
                dbSBPEntities2 context = new dbSBPEntities2();
                if (paymenttype == "All")
                {
                    if (propertyid == "0") // All Properties
                    {
                        if (search == "All")
                        {
                            DateTime dtFrom = DateTime.Now.AddDays(-7);
                            DateTime dtTo = DateTime.Now;
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            // By default showing last 7th days in all properties
                        }
                        else if (search == "SubType")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyTypeID == ptypeid && sale.PropertySizeID == psize && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            //var model = context.tblSSaleFlats.Where(p => p.SaleDate >= datef && p.SaleDate <= datet).OrderBy(o => o.SaleID);
                            //return View(model);
                        }
                        else if (search == "FlatName")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.FlatName.Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "ReceiptNo")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentNo.Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "Customer Name")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where (sale.FName + " " + sale.LName).Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "PaymentDate")
                        {

                            DateTime dtFrom = Convert.ToDateTime(datefrom);
                            DateTime dtTo = Convert.ToDateTime(dateto);
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }

                        else if (search == "Last 7 Days")
                        {

                            DateTime dtFrom = DateTime.Now.AddDays(-7);
                            DateTime dtTo = DateTime.Now;

                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                    }
                    else // Search by Property id
                    {
                        if (search == "All")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "SubType")
                        {
                            if (ptypeid != 0)
                            {
                                if (psize == 0)
                                {
                                    var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && pay.PaymentNo != "0" select new { pay = pay });

                                    List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                                    foreach (var v in md)
                                    {
                                        string bc = "", cd = "";
                                        if (v.pay.BankClearanceDate == null) bc = "";
                                        else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                        if (v.pay.ChequeDate == null) cd = "";
                                        else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                        model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                                    }
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                                }
                                else
                                {
                                    var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && sale.PropertySizeID == psize && pay.PaymentNo != "0" select new { pay = pay });

                                    List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                                    foreach (var v in md)
                                    {
                                        string bc = "", cd = "";
                                        if (v.pay.BankClearanceDate == null) bc = "";
                                        else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                        if (v.pay.ChequeDate == null) cd = "";
                                        else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                        model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                                    }
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                                }
                            }
                            else
                            {

                                if (psize == 0)
                                {
                                    var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.PaymentNo != "0" select new { pay = pay });

                                    List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                                    foreach (var v in md)
                                    {
                                        string bc = "", cd = "";
                                        if (v.pay.BankClearanceDate == null) bc = "";
                                        else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                        if (v.pay.ChequeDate == null) cd = "";
                                        else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                        model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                                    }
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                                }
                                else
                                {

                                    var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && pay.PaymentNo != "0" select new { pay = pay });

                                    List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                                    foreach (var v in md)
                                    {
                                        string bc = "", cd = "";
                                        if (v.pay.BankClearanceDate == null) bc = "";
                                        else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                        if (v.pay.ChequeDate == null) cd = "";
                                        else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                        model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                                    }
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                                }

                            }
                        }
                        else if (search == "FlatName")
                        {

                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.FlatName.Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "ReceiptNo")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.PaymentNo.Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "Customer Name")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.CustomerName.Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "PaymentDate")
                        {
                            DateTime dtFrom = Convert.ToDateTime(datefrom);
                            DateTime dtTo = Convert.ToDateTime(dateto);
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "Last 7 Days")
                        {
                            DateTime dtFrom = DateTime.Now.AddDays(-7);
                            DateTime dtTo = DateTime.Now;
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where sale.PropertyID == pid && pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                    }
                }
                else  // Selected any payment type
                {
                    if (propertyid == "0") // All Properties
                    {
                        if (search == "All")
                        {
                            DateTime dtFrom = DateTime.Now.AddDays(-7);
                            DateTime dtTo = DateTime.Now;
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor==paymenttype && sale.PropertyID == pid && pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            // By default showing last 7th days in all properties
                        }
                        else if (search == "SubType")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyTypeID == ptypeid && sale.PropertySizeID == psize && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            //var model = context.tblSSaleFlats.Where(p => p.SaleDate >= datef && p.SaleDate <= datet).OrderBy(o => o.SaleID);
                            //return View(model);
                        }
                        else if (search == "FlatName")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && pay.FlatName.Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "ReceiptNo")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && pay.PaymentNo.Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "Customer Name")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && (sale.FName + " " + sale.LName).Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "PaymentDate")
                        {

                            DateTime dtFrom = Convert.ToDateTime(datefrom);
                            DateTime dtTo = Convert.ToDateTime(dateto);
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }

                        else if (search == "Last 7 Days")
                        {

                            DateTime dtFrom = DateTime.Now.AddDays(-7);
                            DateTime dtTo = DateTime.Now;

                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                    }
                    else // Search by Property id
                    {
                        if (search == "All")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyID == pid && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "SubType")
                        {
                            if (ptypeid != 0)
                            {
                                if (psize == 0)
                                {
                                    var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && pay.PaymentNo != "0" select new { pay = pay });

                                    List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                                    foreach (var v in md)
                                    {
                                        string bc = "", cd = "";
                                        if (v.pay.BankClearanceDate == null) bc = "";
                                        else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                        if (v.pay.ChequeDate == null) cd = "";
                                        else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                        model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                                    }
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                                }
                                else
                                {
                                    var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && sale.PropertySizeID == psize && pay.PaymentNo != "0" select new { pay = pay });

                                    List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                                    foreach (var v in md)
                                    {
                                        string bc = "", cd = "";
                                        if (v.pay.BankClearanceDate == null) bc = "";
                                        else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                        if (v.pay.ChequeDate == null) cd = "";
                                        else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                        model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                                    }
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                                }
                            }
                            else
                            {

                                if (psize == 0)
                                {
                                    var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyID == pid && pay.PaymentNo != "0" select new { pay = pay });

                                    List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                                    foreach (var v in md)
                                    {
                                        string bc = "", cd = "";
                                        if (v.pay.BankClearanceDate == null) bc = "";
                                        else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                        if (v.pay.ChequeDate == null) cd = "";
                                        else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                        model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                                    }
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                                }
                                else
                                {

                                    var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && pay.PaymentNo != "0" select new { pay = pay });

                                    List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                                    foreach (var v in md)
                                    {
                                        string bc = "", cd = "";
                                        if (v.pay.BankClearanceDate == null) bc = "";
                                        else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                        if (v.pay.ChequeDate == null) cd = "";
                                        else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                        model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                                    }
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                                }

                            }
                        }
                        else if (search == "FlatName")
                        {

                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyID == pid && pay.FlatName.Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "ReceiptNo")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyID == pid && pay.PaymentNo.Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "Customer Name")
                        {
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyID == pid && pay.CustomerName.Contains(searchtext) && pay.PaymentNo != "0" select new { pay = pay });

                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "PaymentDate")
                        {
                            DateTime dtFrom = Convert.ToDateTime(datefrom);
                            DateTime dtTo = Convert.ToDateTime(dateto);
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyID == pid && pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                        else if (search == "Last 7 Days")
                        {
                            DateTime dtFrom = DateTime.Now.AddDays(-7);
                            DateTime dtTo = DateTime.Now;
                            var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.PaymentFor == paymenttype && sale.PropertyID == pid && pay.PaymentDate >= dtFrom && pay.PaymentDate <= dtTo && pay.PaymentNo != "0" select new { pay = pay });
                            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                            foreach (var v in md)
                            {
                                string bc = "", cd = "";
                                if (v.pay.BankClearanceDate == null) bc = "";
                                else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                                if (v.pay.ChequeDate == null) cd = "";
                                else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                                model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                            }
                            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        }
                    }
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
            catch (Exception ex)
            {
                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }

        public string ExportAndMailReceipt(string[] transids, string emailid)
        {
            // Export and mail excel sheet data.
            try
            {
                DataTable dt = new DataTable();
                string stStr = string.Empty;
                // stStr += stStr + "<table><tr><td><b>PaymentDate</b></td><td><b>Amount </b></td><td><b>CustomerName</b></td><td><b>Property Name</b></td><td><b>ReceiptNo</b></td><td><b>PaymentMode</b></td><td><b>ChequeNo</b></td><td><b>BankName</b></td><td><b>ChequeDate</b></td><td><b>Status</b></td></tr>";
                foreach (string tid in transids)
                {
                    Hashtable ht = new Hashtable();
                    ht.Add("TransactionID", tid);
                    string l_FileName = "";
                    dt = obj.GetDataTableFromProcedure("Get_PaymentOther", ht);
                    if (dt.Rows.Count > 0)
                    {
                        stStr += @"<tr><td>" + Convert.ToDateTime(dt.Rows[0]["PaymentDate"]).ToString("dd/MM/yyyy") + "</td><td>" + dt.Rows[0]["Amount"] + "</td><td>" + dt.Rows[0]["CustomerName"] + "</td><td>" + dt.Rows[0]["FlatName"] + "</td><td>" + dt.Rows[0]["PaymentNo"] + "</td><td>" + dt.Rows[0]["PaymentMode"] + "</td><td>" + dt.Rows[0]["ChequeNo"] + "</td><td>" + dt.Rows[0]["BankName"] + "</td><td>" + dt.Rows[0]["ChequeDate"] + "</td><td>" + dt.Rows[0]["PaymentStatus"] + "</td></tr>";
                        // stStr += @"     " + Convert.ToDateTime(dt.Rows[0]["PaymentDate"]).ToString("dd/MM/yyyy") + "        " + dt.Rows[0]["Amount"] + "        " + dt.Rows[0]["CustomerName"] + "      " + dt.Rows[0]["FlatName"] + "      " + dt.Rows[0]["PaymentNo"] + "     " + dt.Rows[0]["PaymentMode"] + "       " + dt.Rows[0]["ChequeNo"] + "      " + dt.Rows[0]["BankName"] + "      " + dt.Rows[0]["ChequeDate"] + "        " + dt.Rows[0]["PaymentStatus"] + "     \n";
                    }
                }

                string htmlText = GridBody2();
                htmlText = htmlText.Replace("<%Data%>", stStr);
                string filename = "ReceiptData_" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString() + ".xls";
                System.IO.File.WriteAllText(Server.MapPath("~/PDF/Temp/" + filename), htmlText);
                //  string tfile = ExportGrid(transids);
                SendMail sm = new SendMail();
                sm.BackupReceiptMailDataFile("Payment Receipt from SBP Groups", "", emailid, filename);
                return "Yes";
            }
            catch (Exception ex)
            {
                Helper h = new Helper();
                h.LogExceptionNo(ex, "IME&M", User.Identity.Name);
                return "No";
            }
        }
        public string BackupReceiptSendMail(string[] transids, string emailid)
        {
            try
            {
                string[] tfiles = GeneratePDFReceipt(transids);
                SendMail sm = new SendMail();
                sm.BackupReceiptMail("Payment Receipt from SBP Groups", "", emailid, tfiles);
                return "Yes";
            }
            catch (Exception ex)
            {
                Helper h = new Helper();
                h.LogExceptionNo(ex, "IME&M", User.Identity.Name);
                return "No";
            }
        }
        public string BackupReceiptPrint(string transids)
        {
            try
            {
                string[] tids = transids.Split(',');
                dbSBPEntities2 context = new dbSBPEntities2();

                List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                foreach (string s in tids)
                {
                    int tid = Convert.ToInt32(s);
                    var pay = context.tblSPaymentOthers.Where(p => p.TransactionID == tid).FirstOrDefault();
                    var cust = context.Customers.Where(c => c.SaleID == pay.SaleID && c.SaleStatus == true).FirstOrDefault();
                    int? pid = context.tblSSaleFlats.Where(ss => ss.SaleID == pay.SaleID).FirstOrDefault().PropertyID;
                    var Property = context.tblSProperties.Where(p => p.PID == pid).FirstOrDefault();

                    string bc = "", cd = "";
                    if (pay.BankClearanceDate == null) bc = "";
                    else bc = pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                    if (pay.ChequeDate == null) cd = "";
                    else cd = pay.ChequeDate.Value.ToString("dd/MM/yyyy");

                    model.Add(new PaymentPayDateModel { Activity = pay.Activity, Amount = pay.Amount, AmtRcvdinWords = pay.AmtRcvdinWords, BankBranch = pay.BankBranch, BankCharges = pay.BankCharges, BankClearanceDate = bc, BankName = pay.BankName, ChequeDate = cd, ChequeNo = pay.ChequeNo, ClearanceCharge = pay.ClearanceCharge, CrDate = pay.CrDate, CreatedBy = pay.CreatedBy, CustomerName = pay.CustomerName, DueAmount = 0, FlatName = pay.FlatName, InstallmentNo = pay.PaymentFor, InterestAmount = 0, IsBounce = pay.IsBounce, IsReceipt = pay.IsReceipt, isrefund = pay.isrefund, ModifyBy = pay.ModifyBy, ModifyDate = pay.ModifyDate, PaymentDate = pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = pay.PaymentOtherID, PaymentMode = pay.PaymentMode, PaymentNo = pay.PaymentNo, PaymentStatus = pay.PaymentStatus, RecordStatus = pay.RecordStatus, RefundRemark = pay.RefundRemark, SaleID = pay.SaleID, ServiceTaxAmount = 0, TotalAmount = pay.Amount, TransactionID = pay.TransactionID, TransferDate = pay.TransferDate, CustomerAddress = cust.Address1 + " " + cust.Address2 + " " + cust.City + " " + cust.State + " " + cust.PinCode, CoCustomerName = cust.CoAppTitle + " " + cust.CoFName + " " + cust.CoMName + " " + cust.CoLName, CoCustomerAddress = cust.CoAddress1 + " " + cust.CoAddress2 + " " + cust.CoCity + " " + cust.CoState + " " + cust.CoCountry + " " + cust.CoPinCode, PropertyAddress = Property.Address, Remarks = pay.Remarks, PropertyName = Property.PName, PropertyLocation = Property.Location });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
            catch (Exception ex)
            {
                Helper h = new Helper();
                h.LogExceptionNo(ex, "IME&M", User.Identity.Name);
                return "No";
            }
        }
        public string BackupReceiptPrintSingle(string tid)
        {
            try
            {
                dbSBPEntities2 context = new dbSBPEntities2();

                List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
                int tids = Convert.ToInt32(tid);
                var pay = context.tblSPaymentOthers.Where(p => p.TransactionID == tids).FirstOrDefault();
                //var pop = (from sale in context.tblSSaleFlats join p in context.tblSProperties on sale.PropertyID equals p.PID where sale.SaleID == pay.SaleID select new { PropertyName=p.PName, PID=p.PID, PropertyAddress=p.Address+ " "+p.Village+" "+p.Tehsil }).AsEnumerable();
                var cust = context.Customers.Where(c => c.SaleID == pay.SaleID && c.SaleStatus == true).FirstOrDefault();
                int? pid = context.tblSSaleFlats.Where(s => s.SaleID == pay.SaleID).FirstOrDefault().PropertyID;
                var Property = context.tblSProperties.Where(p => p.PID == pid).FirstOrDefault();

                string bc = "", cd = "";
                if (pay.BankClearanceDate == null) bc = "";
                else bc = pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                if (pay.ChequeDate == null) cd = "";
                else cd = pay.ChequeDate.Value.ToString("dd/MM/yyyy");

                model.Add(new PaymentPayDateModel { Activity = pay.Activity, Amount = pay.Amount, AmtRcvdinWords = pay.AmtRcvdinWords, BankBranch = pay.BankBranch, BankCharges = pay.BankCharges, BankClearanceDate = bc, BankName = pay.BankName, ChequeDate = cd, ChequeNo = pay.ChequeNo, ClearanceCharge = pay.ClearanceCharge, CrDate = pay.CrDate, CreatedBy = pay.CreatedBy, CustomerName = pay.CustomerName, DueAmount = 0, FlatName = pay.FlatName, InstallmentNo = pay.PaymentFor, InterestAmount = 0, IsBounce = pay.IsBounce, IsReceipt = pay.IsReceipt, isrefund = pay.isrefund, ModifyBy = pay.ModifyBy, ModifyDate = pay.ModifyDate, PaymentDate = pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = pay.PaymentOtherID, PaymentMode = pay.PaymentMode, PaymentNo = pay.PaymentNo, PaymentStatus = pay.PaymentStatus, RecordStatus = pay.RecordStatus, RefundRemark = pay.RefundRemark, SaleID = pay.SaleID, ServiceTaxAmount = 0, TotalAmount = pay.Amount, TransactionID = pay.TransactionID, TransferDate = pay.TransferDate, CustomerAddress = cust.Address1 + " " + cust.Address2 + " " + cust.City + " " + cust.State + " " + cust.PinCode, CoCustomerName = cust.CoAppTitle + " " + cust.CoFName + " " + cust.CoMName + " " + cust.CoLName, CoCustomerAddress = cust.CoAddress1 + " " + cust.CoAddress2 + " " + cust.CoCity + " " + cust.CoState + " " + cust.CoCountry + " " + cust.CoPinCode, PropertyAddress = Property.Address, Remarks = pay.Remarks, PropertyName = Property.PName, PropertyLocation = Property.Location });
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
            catch (Exception ex)
            {
                Helper h = new Helper();
                h.LogExceptionNo(ex, "IME&M", User.Identity.Name);
                return "No";
            }
        }
        public string SearchBackupReceiptData(string tids)
        {
            string[] ids = tids.Split(',');
            dbSBPEntities2 context = new dbSBPEntities2();
            List<PaymentPayDateModel> model = new List<PaymentPayDateModel>();
            foreach (string id in ids)
            {
                int tid = Convert.ToInt32(id);
                var md = (from pay in context.tblSPaymentOthers join sale in context.tblSSaleFlats on pay.SaleID equals sale.SaleID where pay.TransactionID == tid select new { pay = pay });
                foreach (var v in md)
                {
                    string bc = "", cd = "";
                    if (v.pay.BankClearanceDate == null) bc = "";
                    else bc = v.pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                    if (v.pay.ChequeDate == null) cd = "";
                    else cd = v.pay.ChequeDate.Value.ToString("dd/MM/yyyy");
                    model.Add(new PaymentPayDateModel { Activity = v.pay.Activity, Amount = v.pay.Amount, AmtRcvdinWords = v.pay.AmtRcvdinWords, BankBranch = v.pay.BankBranch, BankCharges = v.pay.BankCharges, BankClearanceDate = bc, BankName = v.pay.BankName, ChequeDate = cd, ChequeNo = v.pay.ChequeNo, ClearanceCharge = v.pay.ClearanceCharge, CrDate = v.pay.CrDate, CreatedBy = v.pay.CreatedBy, CustomerName = v.pay.CustomerName, FlatName = v.pay.FlatName, InstallmentNo = v.pay.PaymentFor, IsBounce = v.pay.IsBounce, IsReceipt = v.pay.IsReceipt, isrefund = v.pay.isrefund, ModifyBy = v.pay.ModifyBy, ModifyDate = v.pay.ModifyDate, PaymentDate = v.pay.PaymentDate.Value.ToString("dd/MM/yyyy"), PaymentID = v.pay.PaymentOtherID, PaymentMode = v.pay.PaymentMode, PaymentNo = v.pay.PaymentNo, PaymentStatus = v.pay.PaymentStatus, RecordStatus = v.pay.RecordStatus, RefundRemark = v.pay.RefundRemark, SaleID = v.pay.SaleID, TransactionID = v.pay.TransactionID, TransferDate = v.pay.TransferDate });
                }
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }

        [HttpPost]
        public ActionResult ExportTest(FormCollection collection)
        {
            string ids = collection["hidtid"];
            string[] transids = ids.Split(',');
            DataTable dt = new DataTable();
            string stStr = string.Empty;
            // stStr += stStr + "<table><tr><td><b>PaymentDate</b></td><td><b>Amount </b></td><td><b>CustomerName</b></td><td><b>Property Name</b></td><td><b>ReceiptNo</b></td><td><b>PaymentMode</b></td><td><b>ChequeNo</b></td><td><b>BankName</b></td><td><b>ChequeDate</b></td><td><b>Status</b></td></tr>";
            foreach (string tid in transids)
            {
                Hashtable ht = new Hashtable();
                ht.Add("TransactionID", tid);
                string l_FileName = "";
                dt = obj.GetDataTableFromProcedure("Get_PaymentOther", ht);
                if (dt.Rows.Count > 0)
                {
                    stStr += @"<tr><td>" + Convert.ToDateTime(dt.Rows[0]["PaymentDate"]).ToString("dd/MM/yyyy") + "</td><td>" + dt.Rows[0]["Amount"] + "</td><td>" + dt.Rows[0]["CustomerName"] + "</td><td>" + dt.Rows[0]["FlatName"] + "</td><td>" + dt.Rows[0]["PaymentNo"] + "</td><td>" + dt.Rows[0]["PaymentMode"] + "</td><td>" + dt.Rows[0]["ChequeNo"] + "</td><td>" + dt.Rows[0]["BankName"] + "</td><td>" + dt.Rows[0]["ChequeDate"] + "</td><td>" + dt.Rows[0]["Activity"] + "</td></tr>";
                }
            }

            string htmlText = GridBody();
            htmlText = htmlText.Replace("<%Data%>", stStr);
            ExcelExport(htmlText);
            return View();
        }
        public void ExcelExport(string html)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
            "attachment;filename=Receipts.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            //GridApprove.AllowPaging = false;
            //GridApprove.DataSource = (DataTable)ViewState["dtapprove"];
            //GridApprove.DataBind();
            //GridApprove.RenderControl(hw);
            hw.Write(html);
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            System.IO.File.WriteAllText(Server.MapPath("~/PDF/df.xls"), html);
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        private string GridBody()
        {
            String htmlText = @"     <br />
                <div style='text-align: center'>
<h2>SBP GROUPS</h2>
<p><b>Kharar, Mohali</b></p>
<p> <b>New Delhi</b></p>
<p><b>Ph. 011-43111111</b></p>
<p><b>Fax. 011-43111111</b></p><br/>
<h3>RECEIPT</h3>
                    <br />
                </div>
           <table>
<tr><td><b>PaymentDate</b></td><td><b>Amount </b></td><td><b>CustomerName</b></td><td><b>Property Name</b></td><td><b>ReceiptNo</b></td><td><b>PaymentMode</b></td><td><b>ChequeNo</b></td><td><b>BankName</b></td><td><b>ChequeDate</b></td><td><b>Status</b></td></tr>
 <%Data%> 
</table>
<br/>
               
";
            return htmlText;
        }
        private string GridBody2()
        {
            String htmlText = @"     <br />
              
                                 SBP GROUPS
                              Kharar, Mohali   
                                New Delhi   
                             Ph. 011-43111111  
                             Fax. 011-43111111
                             
                                 RECEIPT
                 
  <table>
<tr><td><b>PaymentDate</b></td><td><b>Amount </b></td><td><b>CustomerName</b></td><td><b>Property Name</b></td><td><b>ReceiptNo</b></td><td><b>PaymentMode</b></td><td><b>ChequeNo</b></td><td><b>BankName</b></td><td><b>ChequeDate</b></td><td><b>Status</b></td></tr>
 <%Data%> 
</table>

               
";
            return htmlText;
        }
        private string[] GeneratePDFReceipt(string[] transids)
        {
            DirectoryInfo dir = new DirectoryInfo(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "\\PDF\\Temp\\");
            try
            {
                foreach (FileInfo fi in dir.GetFiles())
                {
                    fi.Delete();
                }
            }
            catch
            { }
            finally
            { }
            DataTable dt = new DataTable();
            string stStr = string.Empty;
            dbSBPEntities2 context = new dbSBPEntities2();
            string[] rid = new string[transids.Length];

            for (int i = 0; i < transids.Length; i++)
            {
                int tids = Convert.ToInt32(transids[i]);
                var pay = context.tblSPaymentOthers.Where(p => p.TransactionID == tids).FirstOrDefault();
                //var pop = (from sale in context.tblSSaleFlats join p in context.tblSProperties on sale.PropertyID equals p.PID where sale.SaleID == pay.SaleID select new { PropertyName=p.PName, PID=p.PID, PropertyAddress=p.Address+ " "+p.Village+" "+p.Tehsil }).AsEnumerable();
                var cust = context.Customers.Where(c => c.SaleID == pay.SaleID && c.SaleStatus == true).FirstOrDefault();
                int? pid = context.tblSSaleFlats.Where(s => s.SaleID == pay.SaleID).FirstOrDefault().PropertyID;
                var Property = context.tblSProperties.Where(p => p.PID == pid).FirstOrDefault();
                string bc = "", cd = "";
                if (pay.BankClearanceDate == null) bc = "";
                else bc = pay.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                if (pay.ChequeDate == null) cd = "";
                else cd = pay.ChequeDate.Value.ToString("dd/MM/yyyy");

                string htmlText = ReceiptBody();
                htmlText = htmlText.Replace("<%FName%>", pay.CustomerName).Replace("<%FAddress%>", cust.Address1 + " " + cust.Address2 + " " + cust.City + " " + cust.State + " " + cust.PinCode).Replace("<%CName%>", cust.CoAppTitle + " " + cust.CoFName + " " + cust.CoMName + " " + cust.CoLName).Replace("<%PropertyUnitAddress%>", pay.FlatName).Replace("<%PaymentDetails%>", pay.PaymentMode + " " + pay.ChequeNo + " " + pay.BankName + " " + pay.BankBranch + " " + pay.ChequeDate).Replace("<%InstallmentNo%>", pay.PaymentFor).Replace("<%Amount%>", pay.Amount.Value.ToString()).Replace("<%AmountWord%>", pay.AmtRcvdinWords).Replace("<%PaymentNo%>", pay.PaymentNo).Replace("<%PropertyAddress%>", Property.Address).Replace("<%PropertyName%>", Property.PName).Replace("<%PropertyLocation%>", Property.Location);
                stStr += htmlText;
                TestPDF.HtmlToPdfBuilder h2p = new TestPDF.HtmlToPdfBuilder(iTextSharp.text.PageSize.A4);
                string filename = "Receipt(" + Property.ReceiptPrefix + "_" + pay.FlatName + ")";
                string dfile = h2p.HTMLToPdfTemp(htmlText, filename);
                rid[i] = filename;
            }
            // TestPDF.HtmlToPdfBuilder h3p = new TestPDF.HtmlToPdfBuilder(iTextSharp.text.PageSize.A4);

            // Merger all pdf.
            //  string tfile = h3p.MergePDFs(transids);

            // delete all pdf.

            // h3p.DeleteAllFiles(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "\\PDF\\Receipts\\");
            return rid;
        }
        private string ReceiptBody()
        {
            String htmlText = @"   <div><br />
        <div style='text-align: center'>
           <br/>
           <br/>
           <br/>
           <br/>
           <br/>
           <br/>
           <br/>
           <br/>
            <h3>RECEIPT: <%PaymentNo%> </h3>
            <br />
        </div>

        Received with thanks from <br />

        First Allottee: <%FName%> <br />
        <%FAddress%>
        <br />

        Co Allottee(s): &nbsp; <%CName%> <br />
        <p>Payment in the respect to Property : <%PropertyUnitAddress%> </p>

        <p> Vide <%PaymentDetails%> </p><br />
        <table style='width:100%;'>
            <tr style='border:1px solid black'><td>Installment Description</td><td>Amount (Rs)</td>
            <tr>
            <tr style='border:1px solid black'><td> <%InstallmentNo%> </td><td> <%Amount%> </td></tr>
            <tr><td> </td> <td>&nbsp;  </td></tr>
        </table>
        Rupees <%AmountWord%><b style='text-align:right; float:right;'>Total Amount: <%Amount%> </b>
        <br />
       <p>Property at: <%PropertyAddress%> , <b> <%PropertyName%> </b><br/>
Location: <%PropertyLocation%>      
</p>
        <table>
            <tr>
                <td>
                    <ul>
                        <li>Receipt is valid subject to realisation of cheque.</li>
                       
                    </ul>
                </td>
                <td>
                   
                </td>
            </tr>
        </table>
 <p><b>for SBP GROUPS. </b></p>
        <br />
        <br />
        <br />
        <br />
        <br />
        <table>
            <tr>
                <td>
                    <br />
                    (Prepared By)<br />
                    Sunita.d
                </td>
                <td><p><b> </b></p> </td>
            </tr>
        </table>
        <br />

        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
               
";
            return htmlText;
        }
        #endregion

        #region GetOPayment Services
        public string GetPaymentbyTID(string transactionid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            int tid = Convert.ToInt32(transactionid);
            var model = context.tblSPaymentOthers.Where(t => t.TransactionID == tid).FirstOrDefault();
            Mapper.CreateMap<tblSPaymentOther, OtherPaymentModel>();
            var md = Mapper.Map<tblSPaymentOther, OtherPaymentModel>(model);
            md.PaymentDateSt = model.PaymentDate.Value.ToString("dd/MM/yyyy");
            if (model.ChequeDate != null)
                md.ChequeDateSt = model.ChequeDate.Value.ToString("dd/MM/yyyy");
            if (model.BankClearanceDate != null)
                md.BankClearanceDateSt = model.BankClearanceDate.Value.ToString("dd/MM/yyyy");
            return Newtonsoft.Json.JsonConvert.SerializeObject(md);
        }
        public string GetOtherPaymentMaster()
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            var model = context.PaymentMasters.OrderBy(p => p.PaymentMasterID).ToList();
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }

        // Get Servitex Charged Details
        public string GetOPaymentServiceTaxCharged(string saleid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            int sid = Convert.ToInt32(saleid);
            var payment = context.tblSPayments.Where(p => p.SaleID == sid && p.ServiceTaxAmount > 0).ToList();
            List<PaymentModel> md = new List<PaymentModel>();
            foreach (var pay in payment)
            {
                Mapper.CreateMap<tblSPayment, PaymentModel>();
                var model = Mapper.Map<tblSPayment, PaymentModel>(pay);
                if (model.PaymentDate != null)
                    model.PaymentDateSt = Convert.ToDateTime(model.PaymentDate).ToString("dd/MM/yyyy");
                if (model.ChequeDate != null)
                    model.ChequeDateSt = Convert.ToDateTime(model.ChequeDate).ToString("dd/MM/yyyy");
                if (model.BankClearanceDate != null)
                    model.BankClearanceDateSt = Convert.ToDateTime(model.BankClearanceDate).ToString("dd/MM/yyyy");
                md.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(md);
        }
        // Get Servitex Charged Details
        public string GetOPaymentClearanceCharged(string saleid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            int sid = Convert.ToInt32(saleid);
            var payment = context.tblSPayments.Where(p => p.SaleID == sid && p.ClearanceCharge > 0).ToList();
            List<PaymentModel> md = new List<PaymentModel>();
            foreach (var pay in payment)
            {
                Mapper.CreateMap<tblSPayment, PaymentModel>();
                var model = Mapper.Map<tblSPayment, PaymentModel>(pay);
                if (model.PaymentDate != null)
                    model.PaymentDateSt = Convert.ToDateTime(model.PaymentDate).ToString("dd/MM/yyyy");
                if (model.ChequeDate != null)
                    model.ChequeDateSt = Convert.ToDateTime(model.ChequeDate).ToString("dd/MM/yyyy");
                if (model.BankClearanceDate != null)
                    model.BankClearanceDateSt = Convert.ToDateTime(model.BankClearanceDate).ToString("dd/MM/yyyy");
                md.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(md);
        }

        public string GetOPaymentLatePaymentCharged(string saleid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            int sid = Convert.ToInt32(saleid);

            var payment = context.LatePayments.Where(p => p.SaleID == sid).ToList();
            List<LatePaymentModel> md = new List<LatePaymentModel>();
            foreach (var pay in payment)
            {
                Mapper.CreateMap<LatePayment, LatePaymentModel>();
                var model = Mapper.Map<LatePayment, LatePaymentModel>(pay);
                if (model.DueDate != null)
                    model.DueDateSt = Convert.ToDateTime(model.DueDate).ToString("dd/MM/yyyy");
                if (model.CrDate != null)
                    model.CrDateSt = Convert.ToDateTime(model.CrDate).ToString("dd/MM/yyyy");
                md.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(md);
        }

        /// <summary>
        /// Property Transfer details from one cusotmer to another customer
        /// </summary>
        /// <param name="saleid"></param>
        /// <returns>json stirng </returns>
        public string GetOPaymentPropertyTransferCharged(string saleid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            int sid = Convert.ToInt32(saleid);

            var payment = context.PropertyTransfers.Where(p => p.SaleID == sid).ToList();
            List<PropertyTransferModel> md = new List<PropertyTransferModel>();
            foreach (var pay in payment)
            {
                Mapper.CreateMap<PropertyTransfer, PropertyTransferModel>();
                var model = Mapper.Map<PropertyTransfer, PropertyTransferModel>(pay);
                if (model.TransferDate != null)
                    model.TransferDateSt = Convert.ToDateTime(model.TransferDate).ToString("dd/MM/yyyy");
                md.Add(model);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(md);
        }

        public string GetOtherPaymentBySaleIDPaymentFor(string saleid, string paymentfor)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            int sid = Convert.ToInt32(saleid);
            var model = context.tblSPaymentOthers.Where(t => t.SaleID == sid && t.PaymentFor == paymentfor).ToList();
            List<OtherPaymentModel> md = new List<OtherPaymentModel>();
            foreach (var pay in model)
            {
                Mapper.CreateMap<tblSPaymentOther, OtherPaymentModel>();
                var md2 = Mapper.Map<tblSPaymentOther, OtherPaymentModel>(pay);
                md2.PaymentDateSt = md2.PaymentDate.Value.ToString("dd/MM/yyyy");
                if (md2.ChequeDate != null)
                    md2.ChequeDateSt = md2.ChequeDate.Value.ToString("dd/MM/yyyy");
                if (md2.BankClearanceDate != null)
                    md2.BankClearanceDateSt = md2.BankClearanceDate.Value.ToString("dd/MM/yyyy");
                md.Add(md2);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(md);
        }
        #endregion
    }
}