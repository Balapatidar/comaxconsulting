﻿using Comax.Web.Code;
using Comax.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Comax.Web.Controllers
{

    public class HomeController : BaseController
    {
        // [MyAuthorize]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
                return View();
            else return RedirectToAction("Login", "Account", new { area = "" });
        }
        [MyAuthorize]
        public ActionResult BookToSale(int id)
        {
            dbSBPEntities1 context = new dbSBPEntities1();
            var falt = context.tblSSaleFlats.Where(s => s.SaleID == id).FirstOrDefault();
            @ViewBag.SaleID = falt.SaleID;
            @ViewBag.FlatID = falt.FlatID;
            dbSBPEntities2 con = new dbSBPEntities2();
            var flat = con.tblSFlats.Where(f => f.FlatID == falt.FlatID).FirstOrDefault();
            @ViewBag.FlatName = flat.FlatName;
            @ViewBag.PlanName = con.PlanTypes.Where(p => p.PlanID == falt.PlanID).FirstOrDefault().PlanTypeName;
            return View(falt);
        }
        [MyAuthorize]
        public ActionResult Search()
        {
            DateTime datef = new DateTime();
            DateTime datet = new DateTime();
            // Date.
            datef = DateTime.Now.AddMonths(-1);
            datet = DateTime.Now;
            dbSBPEntities2 context = new dbSBPEntities2();
            Session["propertyName"] = "Property Name";
            var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.SaleDate >= datef && sale.SaleDate <= datet select new { sale = sale, FlatName = f.FlatName });

            List<FlatSaleModel> model = new List<FlatSaleModel>();
            foreach (var v in md)
            {
                model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
            }
            return View(model);

            //var model = context.tblSSaleFlats.Where(p => p.SaleDate >= datef && p.SaleDate <= datet).OrderBy(o => o.SaleID);
            //return View(model);
        }

        [HttpPost]
        public ActionResult Search(FormCollection collection)
        {
            try
            {
                string propertyName = collection["protypename"];
                string search = collection["searchby"];
                string propertyid = collection["PropertyID"];
                string propertySubTypeID = collection["PropertyTypeID"];
                string proSize = collection["PropertySizeID"];
                string datefrom = collection["datefrom"];
                string dateto = collection["dateto"];
                string searchtext = collection["searchtext"];
                DateTime datef = new DateTime();
                DateTime datet = new DateTime();

                // Date.
                if (datefrom != "" && dateto != "")
                {
                    datef = Convert.ToDateTime(datefrom);
                    datet = Convert.ToDateTime(dateto);
                }
                else
                {
                    datef = DateTime.Now.AddMonths(-1);
                    datet = DateTime.Now;
                }

                if (propertyid == "? undefined:undefined ?" || propertyid == "All" || propertyid == "") propertyid = "0";
                if (propertySubTypeID == "? undefined:undefined ?" || propertySubTypeID == "All" || propertySubTypeID == "") { propertySubTypeID = "0"; Session["propertyName"] = "Property Name"; }
                else { Session["propertyName"] = propertyName; }
                if (proSize == "" || proSize == "? undefined:undefined ?" || proSize == "All") proSize = "0";
                int pid = Convert.ToInt32(propertyid);
                int ptypeid = Convert.ToInt32(propertySubTypeID);
                int psize = Convert.ToInt32(proSize);
                dbSBPEntities2 context = new dbSBPEntities2();
                if (propertyid == "0") // All Properties
                {
                    if (search == "All")
                    {
                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.SaleDate >= datef && sale.SaleDate <= datet select new { sale = sale, FlatName = f.FlatName });
                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                        // By default showing last one month sales in all properties
                    }
                    else if (search == "SubType")
                    {
                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyTypeID == ptypeid && sale.PropertySizeID == psize select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);

                        //var model = context.tblSSaleFlats.Where(p => p.SaleDate >= datef && p.SaleDate <= datet).OrderBy(o => o.SaleID);
                        //return View(model);
                    }
                    else if (search == "FlatName")
                    {


                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where f.FlatName.Contains(searchtext) select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);


                    }
                    else if (search == "Customer Name")
                    {
                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where (sale.FName + " " + sale.LName).Contains(searchtext) select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                    else if (search == "BookingDate")
                    {

                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.BookingDate >= dtFrom && sale.BookingDate <= dtTo select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                    else if (search == "SaleDate")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                    else if (search == "This Month")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datef);
                        DateTime dtTo = Convert.ToDateTime(datet);

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                    else if (search == "Last 7 Days")
                    {

                        DateTime dtFrom = DateTime.Now.AddDays(-7);
                        DateTime dtTo = DateTime.Now;

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                }
                else // Search by Property id
                {
                    if (search == "All")
                    {
                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                    else if (search == "SubType")
                    {
                        if (ptypeid != 0)
                        {
                            if (psize == 0)
                            {
                                var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid select new { sale = sale, FlatName = f.FlatName });
                                List<FlatSaleModel> model = new List<FlatSaleModel>();
                                foreach (var v in md)
                                {
                                    model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                                }
                                return View(model);
                            }
                            else
                            {
                                var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && sale.PropertySizeID == psize select new { sale = sale, FlatName = f.FlatName });
                                List<FlatSaleModel> model = new List<FlatSaleModel>();
                                foreach (var v in md)
                                {
                                    model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                                }
                                return View(model);
                            }
                        }
                        else
                        {

                            if (psize == 0)
                            {
                                var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid select new { sale = sale, FlatName = f.FlatName });
                                List<FlatSaleModel> model = new List<FlatSaleModel>();
                                foreach (var v in md)
                                {
                                    model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                                }
                                return View(model);
                            }
                            else
                            {
                                var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid select new { sale = sale, FlatName = f.FlatName });
                                List<FlatSaleModel> model = new List<FlatSaleModel>();
                                foreach (var v in md)
                                {
                                    model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                                }
                                return View(model);
                            }

                        }
                    }
                    else if (search == "FlatName")
                    {
                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid && f.FlatName.Contains(searchtext) select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                    else if (search == "Customer Name")
                    {
                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid && (sale.FName + " " + sale.LName).Contains(searchtext) select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                    else if (search == "BookingDate")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid && sale.BookingDate >= dtFrom && sale.BookingDate <= dtTo select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                    else if (search == "SaleDate")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid && sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                    else if (search == "This Month")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datef);
                        DateTime dtTo = Convert.ToDateTime(datet);

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid && sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                    else if (search == "Last 7 Days")
                    {
                        DateTime dtFrom = DateTime.Now.AddDays(-7);
                        DateTime dtTo = DateTime.Now;

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyID == pid && sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            model.Add(new FlatSaleModel { FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return View(model);
                    }
                }
                return View();
            }
            catch (Exception ex)
            {
                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return View();
            }
        }
        [MyAuthorize]
        public ActionResult Booking()
        {
            // Default last one month booking
            DateTime dtf = DateTime.Now.AddMonths(-1);
            DateTime dtt = DateTime.Now;
            dbSBPEntities2 context = new dbSBPEntities2(); ViewBag.PropertyName = "PropertyName";
            Session["title"] = "Property Name";
            var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.BookingDate >= dtf && s.BookingDate <= dtt && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
            List<FlatSaleModel> model = new List<FlatSaleModel>();
            foreach (var v in md)
            {
                model.Add(new FlatSaleModel { FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
            }
            //var model = context.tblSSaleFlats.Where(p => p.BookingDate >= dtf && p.BookingDate <= dtt && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
            return View(model);
        }
        [HttpPost]
        public ActionResult Booking(FormCollection collection)
        {

            string ptypeName = collection["protypename"];
            string proeprtyid = collection["PropertyID"];
            string ptype = collection["PropertyTypeID"];
            string psize = collection["PropertySizeID"];
            string Search = collection["searchtext"];
            int pid = 0, ptyp = 0, psiz = 0;
            if (proeprtyid == "? undefined:undefined ?" || proeprtyid == "" || proeprtyid == "All") proeprtyid = ""; else pid = Convert.ToInt32(proeprtyid);
            if (ptype == "? undefined:undefined ?" || ptype == "" || ptype == "All") { ptype = ""; Session["title"] = "Property Name"; }
            else { ptyp = Convert.ToInt32(ptype); Session["title"] = ptypeName; }
            if (psize == "? undefined:undefined ?" || psize == "" || psize == "All") psize = ""; else psiz = Convert.ToInt32(psize);
            dbSBPEntities2 context = new dbSBPEntities2();

            if (proeprtyid == "")
            {
                // Search nothing.
                Failure = "No Property record found.";
            }
            else if (ptype != "" && psize == "")
            {
                // Search with type.
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyTypeID == ptyp && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    model.Add(new FlatSaleModel { FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                // var model = context.tblSSaleFlats.Where(p => p.PropertyTypeID == ptyp && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                return View(model);
            }
            else if (ptype != "" && psize != "")
            {
                //Search with property type and size.
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyTypeID == ptyp && s.PropertySizeID == psiz && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    model.Add(new FlatSaleModel { FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                // var model = context.tblSSaleFlats.Where(p => p.PropertyTypeID == ptyp && p.PropertySizeID == psiz && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                return View(model);
            }
            else if (ptype != "" && Search != "")
            {
                dbSBPEntities2 cont = new dbSBPEntities2();
                var flt = cont.tblSFlats.Where(ff => ff.PID == pid && ff.FlatName.Contains(Search)).FirstOrDefault();
                if (flt != null)
                {
                    // Search with only property type and proeprty name
                    var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyTypeID == ptyp && s.FlatID == flt.FlatID && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                    List<FlatSaleModel> model = new List<FlatSaleModel>();
                    foreach (var v in md)
                    {
                        model.Add(new FlatSaleModel { FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                    }
                    // var model = context.tblSSaleFlats.Where(p => p.PropertyTypeID == ptyp && p.FlatID == flt.FlatID && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                    return View(model);
                }
                else
                {
                    Failure = "Property Name not found.";
                }
            }
            else if (proeprtyid != "" && Search != "")
            {
                //Search With property id and property name.
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyID == pid && f.FlatName.Contains(Search) && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    model.Add(new FlatSaleModel { FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                //  var model = context.tblSSaleFlats.Where(p => p.PropertyID == pid && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                return View(model);
            }
            else if (proeprtyid == "" && Search != "")
            {
                //Search With property name.
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where f.FlatName.Contains(Search) && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    model.Add(new FlatSaleModel { FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                //  var model = context.tblSSaleFlats.Where(p => p.PropertyID == pid && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                return View(model);
            }
            else if (proeprtyid != "")
            {
                //Search With property id.
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyID == pid && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    model.Add(new FlatSaleModel { FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                //  var model = context.tblSSaleFlats.Where(p => p.PropertyID == pid && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                return View(model);
            }
            else
            {
                // seaerch default
                DateTime dtf = DateTime.Now.AddMonths(-1);
                DateTime dtt = DateTime.Now;
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.BookingDate >= dtf && s.BookingDate <= dtt && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    model.Add(new FlatSaleModel { FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                //  var model = context.tblSSaleFlats.Where(p => p.BookingDate <= dtf && p.BookingDate >= dtt && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                return View(model);
            }
            DateTime df = DateTime.Now.AddMonths(-1);
            DateTime dt = DateTime.Now;

            var mmd = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.BookingDate >= df && s.BookingDate <= dt && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
            List<FlatSaleModel> mdl = new List<FlatSaleModel>();
            foreach (var v in mmd)
            {
                mdl.Add(new FlatSaleModel { FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
            }
            var d = context.tblSSaleFlats.Where(p => p.BookingDate <= df && p.BookingDate >= dt && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
            return View(mdl);
        }
        [MyAuthorize]
        public ActionResult Summary(int id)
        {
            ViewBag.ID = id;
            return View();
        }
        public ActionResult AccessDenied()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ExportSummary(FormCollection collection)
        {
            string htmlText = collection["hidhtml"];
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
            "attachment;filename=Receipts.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            //GridApprove.AllowPaging = false;
            //GridApprove.DataSource = (DataTable)ViewState["dtapprove"];
            //GridApprove.DataBind();
            //GridApprove.RenderControl(hw);
            hw.Write(htmlText);
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            System.IO.File.WriteAllText(Server.MapPath("~/PDF/SummaryExport.doc"), htmlText);
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return View();
        }

        public ActionResult SummaryPrint(int id, string id2)
        {
            ViewBag.ID2 = id2;
            ViewBag.ID = id;
            return View();
        }
        #region Json Service

        public string BookingSearch(string ptypeName, string proeprtyid, string ptype, string psize, string Search)
        {
            //string ptypeName = collection["protypename"];
            //string proeprtyid = collection["PropertyID"];
            //string ptype = collection["PropertyTypeID"];
            //string psize = collection["PropertySizeID"];
            //string Search = collection["searchtext"];
            int pid = 0, ptyp = 0, psiz = 0;
            if (proeprtyid == "? undefined:undefined ?" || proeprtyid == "" || proeprtyid == "All") proeprtyid = ""; else pid = Convert.ToInt32(proeprtyid);
            if (ptype == "? undefined:undefined ?" || ptype == "" || ptype == "All") { ptype = ""; Session["title"] = "Property Name"; }
            else { ptyp = Convert.ToInt32(ptype); Session["title"] = ptypeName; }
            if (psize == "? undefined:undefined ?" || psize == "" || psize == "All") psize = ""; else psiz = Convert.ToInt32(psize);
            dbSBPEntities2 context = new dbSBPEntities2();

            DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
            dtinfo.DateSeparator = "/";
            dtinfo.ShortDatePattern = "dd/MM/yyyy";
            if (proeprtyid == "")
            {
                // Search nothing.
                Failure = "No Property record found.";
            }
            else if (ptype != "" && psize == "")
            {
                // Search with type.
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyTypeID == ptyp && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    string bdate = "";
                    if (v.Sale.BookingDate != null)
                        bdate = Convert.ToDateTime(v.Sale.BookingDate).ToString("dd/MM/yyyy");
                    model.Add(new FlatSaleModel { BookingDateSt = bdate, FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                // var model = context.tblSSaleFlats.Where(p => p.PropertyTypeID == ptyp && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                //  return Json(new { SList = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyTypeID == ptyp && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName }) }, JsonRequestBehavior.AllowGet);
            }
            else if (ptype != "" && psize != "")
            {
                //Search with property type and size.
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyTypeID == ptyp && s.PropertySizeID == psiz && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    string bdate = "";
                    if (v.Sale.BookingDate != null)
                        bdate = Convert.ToDateTime(v.Sale.BookingDate).ToString("dd/MM/yyyy");
                    model.Add(new FlatSaleModel { BookingDateSt = bdate, FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                // var model = context.tblSSaleFlats.Where(p => p.PropertyTypeID == ptyp && p.PropertySizeID == psiz && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                //return Json(new { SList = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyTypeID == ptyp && s.PropertySizeID == psiz && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName }) }, JsonRequestBehavior.AllowGet);
            }
            else if (ptype != "" && Search != "")
            {
                dbSBPEntities2 cont = new dbSBPEntities2();
                var flt = cont.tblSFlats.Where(f => f.PID == pid && f.FlatName.Contains(Search)).FirstOrDefault();
                if (flt != null)
                {
                    // Search with only property type and proeprty name
                    var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyTypeID == ptyp && s.FlatID == flt.FlatID && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                    List<FlatSaleModel> model = new List<FlatSaleModel>();
                    foreach (var v in md)
                    {
                        string bdate = "";
                        if (v.Sale.BookingDate != null)
                            bdate = Convert.ToDateTime(v.Sale.BookingDate).ToString("dd/MM/yyyy");
                        model.Add(new FlatSaleModel { BookingDateSt = bdate, FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                    }
                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    // var model = context.tblSSaleFlats.Where(p => p.PropertyTypeID == ptyp && p.FlatID == flt.FlatID && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                    //  return Json(new { SList = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyTypeID == ptyp && s.FlatID == flt.FlatID && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName }) }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Failure = "Property Name not found.";
                }
            }
            else if (proeprtyid != "" && Search != "")
            {
                //Search With property id and property name.
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyID == pid && f.FlatName.Contains(Search) && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    string bdate = "";
                    if (v.Sale.BookingDate != null)
                        bdate = Convert.ToDateTime(v.Sale.BookingDate).ToString("dd/MM/yyyy");
                    model.Add(new FlatSaleModel { BookingDateSt = bdate, FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                //  var model = context.tblSSaleFlats.Where(p => p.PropertyID == pid && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                //return Json(new { SList = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyID == pid && f.FlatName.Contains(Search) && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName }) }, JsonRequestBehavior.AllowGet);
            }
            else if (proeprtyid == "" && Search != "")
            {
                //Search With property name.
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where f.FlatName.Contains(Search) && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    string bdate = "";
                    if (v.Sale.BookingDate != null)
                        bdate = Convert.ToDateTime(v.Sale.BookingDate).ToString("dd/MM/yyyy");
                    model.Add(new FlatSaleModel { BookingDateSt = bdate, FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                //  var model = context.tblSSaleFlats.Where(p => p.PropertyID == pid && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                //return Json(new { SList = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where f.FlatName.Contains(Search) && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName }) }, JsonRequestBehavior.AllowGet);
            }
            else if (proeprtyid != "")
            {
                //Search With property id.
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyID == pid && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    string bdate = "";
                    if (v.Sale.BookingDate != null)
                        bdate = Convert.ToDateTime(v.Sale.BookingDate).ToString("dd/MM/yyyy");
                    model.Add(new FlatSaleModel { BookingDateSt = bdate, FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                //  var model = context.tblSSaleFlats.Where(p => p.PropertyID == pid && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                //return Json(new { SList = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.PropertyID == pid && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName }) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // seaerch default
                DateTime dtf = DateTime.Now.AddMonths(-1);
                DateTime dtt = DateTime.Now;
                var md = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.BookingDate >= dtf && s.BookingDate <= dtt && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
                List<FlatSaleModel> model = new List<FlatSaleModel>();
                foreach (var v in md)
                {
                    string bdate = "";
                    if (v.Sale.BookingDate != null)
                        bdate = Convert.ToDateTime(v.Sale.BookingDate).ToString("dd/MM/yyyy");
                    model.Add(new FlatSaleModel { BookingDateSt = bdate, FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                //  var model = context.tblSSaleFlats.Where(p => p.BookingDate <= dtf && p.BookingDate >= dtt && p.PaymentFor.Contains("Booking Payment")).OrderByDescending(o => o.SaleID);
                //return Json(new { SList = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.BookingDate >= dtf && s.BookingDate <= dtt && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName }) }, JsonRequestBehavior.AllowGet);
            }
            DateTime df = DateTime.Now.AddMonths(-1);
            DateTime dt = DateTime.Now;

            var mmd = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where s.BookingDate >= df && s.BookingDate <= dt && s.PaymentFor.Contains("Booking Payment") select new { Sale = s, FlatName = f.FlatName });
            List<FlatSaleModel> mdl = new List<FlatSaleModel>();
            foreach (var v in mmd)
            {
                string bdate = "";
                if (v.Sale.BookingDate != null)
                    bdate = Convert.ToDateTime(v.Sale.BookingDate).ToString("dd/MM/yyyy");
                mdl.Add(new FlatSaleModel { BookingDateSt = bdate, FlatID = v.Sale.FlatID, PropertyID = v.Sale.PropertyID, FName = v.Sale.FName, LName = v.Sale.LName, BookingDate = v.Sale.BookingDate, BookingAmount = v.Sale.BookingAmount, Remarks = v.Sale.Remarks, SaleID = v.Sale.SaleID, FlatName = v.FlatName });
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(mdl);
        }

        public string PropertySearch(string propertyName, string search, string propertyid, string propertySubTypeID, string proSize, string datefrom, string dateto, string searchtext)
        {
            try
            {
                DateTime datef = new DateTime();
                DateTime datet = new DateTime();

                // Date.
                if (datefrom != "" && dateto != "")
                {
                    datef = Convert.ToDateTime(datefrom);
                    datet = Convert.ToDateTime(dateto);
                }
                else
                {
                    datef = DateTime.Now.AddMonths(-1);
                    datet = DateTime.Now;
                }

                if (propertyid == "? undefined:undefined ?" || propertyid == "All" || propertyid == "") propertyid = "0";
                if (propertySubTypeID == "? undefined:undefined ?" || propertySubTypeID == "All" || propertySubTypeID == "") { propertySubTypeID = "0"; Session["propertyName"] = "Property Name"; }
                else { Session["propertyName"] = propertyName; }
                if (proSize == "" || proSize == "? undefined:undefined ?" || proSize == "All") proSize = "0";
                int pid = Convert.ToInt32(propertyid);
                int ptypeid = Convert.ToInt32(propertySubTypeID);
                int psize = Convert.ToInt32(proSize);
                dbSBPEntities2 context = new dbSBPEntities2();
                if (propertyid == "0") // All Properties
                {
                    if (search == "All")
                    {
                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.SaleDate >= datef && sale.SaleDate <= datet && f.Status == 1 select new { sale = sale, FlatName = f.FlatName });
                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            if (v.sale.PaymentFor == "Booking Payment")
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                        // By default showing last one month sales in all properties
                    }
                    else if (search == "SubType")
                    {
                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.PropertyTypeID == ptypeid && sale.PropertySizeID == psize && f.Status == 1 select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);

                        //var model = context.tblSSaleFlats.Where(p => p.SaleDate >= datef && p.SaleDate <= datet).OrderBy(o => o.SaleID);
                        //return View(model);
                    }
                    else if (search == "FlatName")
                    {
                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where f.FlatName.Contains(searchtext) && f.Status == 1 select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "Customer Name")
                    {
                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where (sale.FName + " " + sale.LName).Contains(searchtext) && f.Status == 1 select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "BookingDate")
                    {

                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.BookingDate >= dtFrom && sale.BookingDate <= dtTo && f.Status == 1 select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "SaleDate")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo && f.Status == 1 select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "This Month")
                    {
                        DateTime dtFrom = DateTime.Now.AddMonths(-1);
                        DateTime dtTo = DateTime.Now;

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo && f.Status == 1 select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "Last 7 Days")
                    {

                        DateTime dtFrom = DateTime.Now.AddDays(-7);
                        DateTime dtTo = DateTime.Now;

                        var md = (from sale in context.tblSSaleFlats join f in context.tblSFlats on sale.FlatID equals f.FlatID where sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo && f.Status == 1 select new { sale = sale, FlatName = f.FlatName });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.sale.FName + " " + v.sale.LName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                }
                else // Search by Property id
                {
                    if (search == "All")
                    {
                        var md = (from sale in context.tblSSaleFlats
                                  join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                  join C in context.Customers on sale.CustomerID equals C.CustomerID
                                  where sale.PropertyID == pid && f.Status == 1
                                  select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.CustomerName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "SubType")
                    {
                        if (ptypeid != 0)
                        {
                            if (psize == 0)
                            {
                                var md = (from sale in context.tblSSaleFlats
                                          join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                          join C in context.Customers on sale.CustomerID equals C.CustomerID
                                          where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && f.Status == 1
                                          select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });
                                List<FlatSaleModel> model = new List<FlatSaleModel>();
                                foreach (var v in md)
                                {
                                    string bdate = "";
                                    if (v.sale.SaleDate != null)
                                        bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                                    model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.CustomerName), PropertyID = v.sale.PropertyID });
                                }
                                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            }
                            else
                            {
                                var md = (from sale in context.tblSSaleFlats
                                          join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                          join C in context.Customers on sale.CustomerID equals C.CustomerID
                                          where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && sale.PropertySizeID == psize && f.Status == 1
                                          select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });
                                List<FlatSaleModel> model = new List<FlatSaleModel>();
                                foreach (var v in md)
                                {
                                    string bdate = "";
                                    if (v.sale.SaleDate != null)
                                        bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                                    model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.CustomerName), PropertyID = v.sale.PropertyID });
                                }
                                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            }
                        }
                        else
                        {

                            if (psize == 0)
                            {
                                var md = (from sale in context.tblSSaleFlats
                                          join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                          join C in context.Customers on sale.CustomerID equals C.CustomerID
                                          where sale.PropertyID == pid && f.Status == 1
                                          select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });
                                List<FlatSaleModel> model = new List<FlatSaleModel>();
                                foreach (var v in md)
                                {
                                    string bdate = "";
                                    if (v.sale.SaleDate != null)
                                        bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                                    model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.CustomerName), PropertyID = v.sale.PropertyID });
                                }
                                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            }
                            else
                            {
                                var md = (from sale in context.tblSSaleFlats
                                          join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                          join C in context.Customers on sale.CustomerID equals C.CustomerID
                                          where sale.PropertyID == pid && sale.PropertyTypeID == ptypeid && f.Status == 1
                                          select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });
                                List<FlatSaleModel> model = new List<FlatSaleModel>();
                                foreach (var v in md)
                                {
                                    string bdate = "";
                                    if (v.sale.SaleDate != null)
                                        bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                                    model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.CustomerName), PropertyID = v.sale.PropertyID });
                                }
                                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                            }

                        }
                    }
                    else if (search == "FlatName")
                    {
                        var md = (from sale in context.tblSSaleFlats
                                  join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                  join C in context.Customers on sale.CustomerID equals C.CustomerID
                                  where sale.PropertyID == pid && f.FlatName.Contains(searchtext) && f.Status == 1
                                  select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = v.CustomerName, PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "Customer Name")
                    {
                        var md = (from sale in context.tblSSaleFlats
                                  join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                  join C in context.Customers on sale.CustomerID equals C.CustomerID
                                  where sale.PropertyID == pid && (C.FName + " " + C.LName).Contains(searchtext) && f.Status == 1
                                  select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.CustomerName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "BookingDate")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);

                        var md = (from sale in context.tblSSaleFlats
                                  join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                  join C in context.Customers on sale.CustomerID equals C.CustomerID
                                  where sale.PropertyID == pid && sale.BookingDate >= dtFrom && sale.BookingDate <= dtTo && f.Status == 1
                                  select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.CustomerName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "SaleDate")
                    {
                        DateTime dtFrom = Convert.ToDateTime(datefrom);
                        DateTime dtTo = Convert.ToDateTime(dateto);

                        var md = (from sale in context.tblSSaleFlats
                                  join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                  join C in context.Customers on sale.CustomerID equals C.CustomerID
                                  where sale.PropertyID == pid && sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo && f.Status == 1
                                  select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.CustomerName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "This Month")
                    {
                        DateTime dtFrom = DateTime.Now.AddMonths(-1);
                        DateTime dtTo = DateTime.Now;

                        var md = (from sale in context.tblSSaleFlats
                                  join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                  join C in context.Customers on sale.CustomerID equals C.CustomerID
                                  where sale.PropertyID == pid && sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo && f.Status == 1
                                  select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.CustomerName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                    else if (search == "Last 7 Days")
                    {
                        DateTime dtFrom = DateTime.Now.AddDays(-7);
                        DateTime dtTo = DateTime.Now;

                        var md = (from sale in context.tblSSaleFlats
                                  join f in context.tblSFlats on sale.FlatID equals f.FlatID
                                  join C in context.Customers on sale.CustomerID equals C.CustomerID
                                  where sale.PropertyID == pid && sale.SaleDate >= dtFrom && sale.SaleDate <= dtTo && f.Status == 1
                                  select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });

                        List<FlatSaleModel> model = new List<FlatSaleModel>();
                        foreach (var v in md)
                        {
                            string bdate = "";
                            if (v.sale.SaleDate != null)
                                bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                            model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = (v.CustomerName), PropertyID = v.sale.PropertyID });
                        }
                        return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                    }
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
            catch (Exception ex)
            {
                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }

        public string PropertySearchByApoove(string status, string search, string propertyid)
        {
            try
            {

                int pid = Convert.ToInt32(propertyid);
                dbSBPEntities2 context = new dbSBPEntities2();
                if (status == "All")
                {

                    var md = (from sale in context.tblSSaleFlats
                              join f in context.tblSFlats on sale.FlatID equals f.FlatID
                              join C in context.Customers on sale.CustomerID equals C.CustomerID
                              where sale.PropertyID == pid && f.FlatName.Contains(search) && f.Status == 1
                              select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });

                    List<FlatSaleModel> model = new List<FlatSaleModel>();
                    foreach (var v in md)
                    {
                        string bdate = "";
                        if (v.sale.SaleDate != null)
                            bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                        model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = v.CustomerName, PropertyID = v.sale.PropertyID, propertySatuts = v.sale.PropertyStatus });
                    }
                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);

                }
                else
                {

                    var md = (from sale in context.tblSSaleFlats
                             join f in context.tblSFlats on sale.FlatID equals f.FlatID
                             join C in context.Customers on sale.CustomerID equals C.CustomerID
                              where sale.PropertyID == pid && f.FlatName.Contains(search) && f.Status == 1 && sale.PropertyStatus == status
                             select new { sale = sale, FlatName = f.FlatName, CustomerName = (C.FName + " " + C.LName) });

                    List<FlatSaleModel> model = new List<FlatSaleModel>();
                    foreach (var v in md)
                    {
                        string bdate = "";
                        if (v.sale.SaleDate != null)
                            bdate = Convert.ToDateTime(v.sale.SaleDate).ToString("dd/MM/yyyy");
                        model.Add(new FlatSaleModel { SaleID = v.sale.SaleID, BookingDateSt = bdate, FlatName = v.FlatName, FlatID = v.sale.FlatID, SaleRate = v.sale.SaleRate, Remarks = v.sale.Remarks, SaleDate = v.sale.SaleDate, FName = v.CustomerName, PropertyID = v.sale.PropertyID, propertySatuts = v.sale.PropertyStatus });
                    }
                    return Newtonsoft.Json.JsonConvert.SerializeObject(model);
                }
            }
            catch (Exception ex)
            {
                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }




        #endregion
    }
}