﻿using Comax.Web.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Comax.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            if (!Roles.RoleExists("Admin"))
            {
                Roles.CreateRole("Admin");
            }
            if (!Roles.RoleExists("Operation"))
            {
                Roles.CreateRole("Operation");
            }
            if (!Roles.RoleExists("Marketing"))
            {
                Roles.CreateRole("Marketing");
            }
            if (!Roles.RoleExists("Office"))
            {
                Roles.CreateRole("Office");
            }
            if (!Roles.RoleExists("Corporate"))
            {
                Roles.CreateRole("Corporate");
            }
        }
        protected void Application_Error()
        {
            // get last exception
            Exception exception = HttpContext.Current.Server.GetLastError();

            if (exception != null)
            {
                Helper helper = new Helper();
                helper.LogException(exception);
            }
        }
    }
}