﻿var myApp = angular.module('APropertyApp', []);
myApp.controller('APropertyController', function ($scope, $http,$filter) {
    $('#loading').show();
    var orderBy = $filter('orderBy')
    $scope.PropertyInit = function () {


        $http.get('/flat/GetPropertyList/').success(function (response) { $scope.Properties = response; });
        var PID = $("#PropertyID").val();
        $http({
            method: 'Get',
            url: '/Admin/Property/SearchProjectProperty',
            params: { pid: PID }
        }).success(function (data, status, headers, config) {
            $scope.MyProperties = data;
        });
    }
    $scope.SearchProperties = function () {
        $scope.Error = "";

        var PID = $("#PropertyID").val();
        $http({
            method: 'Get',
            url: '/Admin/Property/SearchProjectProperty',
            params: { pid: PID }
        }).success(function (data, status, headers, config) {
            $scope.MyProperties = data;
        });
    }

    $scope.orderMyProperties = function (predicate, reverse) {
        $scope.MyProperties = orderBy($scope.MyProperties, predicate, reverse);
    };
    $('#loading').hide();
})