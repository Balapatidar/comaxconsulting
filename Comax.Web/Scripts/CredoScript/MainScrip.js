﻿
var myApp = angular.module('CredoApp', []);
//Defining a Controller 
myApp.controller('FlatController', function ($scope, $http) {

    $("#CPLType").hide();
    $scope.gettower = function () {
        var PID = $scope.property;

        if (PID) {
            $http({
                method: 'POST',
                url: '/flat/GetTower/',
                data: JSON.stringify({ PID: PID })
            }).success(function (data, status, headers, config) {
                $scope.block = data;
            }).error(function (data, status, headers, config) {
                $scope.message = 'Unexpected Error';
            });
            document.getElementById("tabId").value = 1;
        }
        else {
            $scope.states = null;
        }
    }
    $scope.getflat = function () {
        var BID = $scope.tower;

        if (BID) {
            $http({
                method: 'POST',
                url: '/flat/GetFlatByBId/',
                data: JSON.stringify({ BID: BID })
            }).success(function (data, status, headers, config) {
                $scope.Flat = data;
            }).error(function (data, status, headers, config) {
                $scope.message = 'Unexpected Error';
            });
        }
        else {
            $scope.Flat = null;
        }
    }


    $scope.SaveCustomer = function () {

        var vli = ValidateSaveCustomer();
        if (vli == false) {
            $('#myModal').modal('show');
        }
        else {
            $('#loading').show();
            $('#btnSubmit').hide();
            //Defining $http service for creating a person
            if (typeof $scope.newCust != "undefined") {
                $scope.newCust.SrtSaleDate = $("#saleDate").val();
                $scope.newCust.DateOfBirth = $("#AppDOB").val();
                $scope.newCust.CoDOB = $("#CoAppDOB").val();
                $scope.newCust.SecCoDOB = $("#SecCoDOB").val();
                $scope.newCust.AppFName = $("#fNmae").val();
                $scope.newCust.AppMName = $("#mName").val();
                $scope.newCust.AppLName = $("#lName").val();
            }
            else
            {
                $scope.newCust.SrtSaleDate = $("#saleDate").val();
                $scope.newCust.DateOfBirth = $("#AppDOB").val();
                $scope.newCust.CoDOB = $("#CoAppDOB").val();
                $scope.newCust.SecCoDOB = $("#SecCoDOB").val();
                $scope.newCust.AppFName = $("#fNmae").val();
                $scope.newCust.AppMName = $("#mName").val();
                $scope.newCust.AppLName = $("#lName").val();
            }
            $scope.newCust.PropertyID = $("#PropertyID").find(":selected").val();
            $http({
                method: 'POST',
                url: '/flat/SaveCustomer',
                data: JSON.stringify($scope.newCust),
                headers: { 'Content-Type': 'application/JSON' }
            }).success(function (data) { //Showing success message $scope.status = "The Person Saved Successfully!!!";
                //Updating persons Model
                alert("Customer has been saved successfully");
                GetSaleDetailByFlatId();
                $('#loading').hide();
                $('#btnSubmit').show();
            }).error(function (error) {
                //Showing error message
                $scope.status = 'Unable to save Customer: ' + error.message;
                $('#loading').hide();
                $('#btnSubmit').show();
            });
        }
    }

    GetSaleDetailByFlatId = function () {
        var flatId = $scope.newCust.FlatID;
        debugger;
        if (flatId) {
            $http({
                method: 'POST',
                url: '/flat/GetSaleDetailByFlatId/',
                data: JSON.stringify({ flatId: flatId })
            }).success(function (data, status, headers, config) {
                debugger;
                $scope.Sale = data;
            }).error(function (data, status, headers, config) {
                debugger;
                $scope.message = 'Unexpected Error';
            });
        }
        else {
            $scope.Sale = null;
        }
        document.getElementById("stepid").value = 1;
        if (flatId != '' || flatId != null) {
            document.getElementById("flatid").value = flatId;
        }
    }

    $scope.PaymentForChange = function () {

    }
    // Change Property Name and fill propertyType.
    $scope.PropertyName = function () {
        var PID = $("#PropertyID").find(":selected").val();
        $http({
            method: 'Get',
            url: '/flat/GetPropertyTypeList/',
            params: { pid:PID }
        }).success(function (data, status, headers, config) {
            $scope.PropertyTypes = data;
        });
        $("#PropertySizeID").find('option')
    .remove()
    .end()
    .append('<option value=""></option>')
        //$http.get('/flat/GetPropertyTypeList/').success(function (response) { $scope.PropertyTypes = response; });
    }

    // Change Proeprty Type and fill property size
    $scope.PropertyType = function () {
        $http({
            method: 'Get',
            url: '/Admin/Property/GetPropertySizeListByPTypeID/',
            params: { propertyid: $scope.newCust.PropertyTypeID }
        }).success(function (data, status, headers, config) {
            $scope.PropertySizes = data;
        });
    }
    // Get all Property list for dropdownlist
    $http.get('/flat/GetPropertyList/').success(function (response) { $scope.Properties = response; });

    $scope.IsFlatFound = function () {
        var PID = $("#PropertyID").find(":selected").val();

        $http({
            method: 'Get',
            url: '/flat/IsFlatExists',
            params: { flatname: $scope.newCust.FlatName, pid: PID }
        }).success(function (data) {
            if (data == "Yes") { alert("This flat is already registered"); $scope.Error = "This flat is already registered"; }
            else { $scope.Error = ""; }
        })
    }

    function ValidateSaveCustomer() {
        var vl = true;
        var message = "";
        if ($("#PaymentFor").find(":selected").text() == "") {
            vl = false;
            message += "Select Booking/Sale Type. <br/>";
        }
        if ($("#PlanType").find(":selected").text() == "") {
            vl = false;
            message += "Select Plan Type of property.<br/>";
        }
        if ($("#PropertyID").find(":selected").text() == "") {
            vl = false;
            message += "Select Property.<br/>";
        }
        if ($("#PropertyTypeID").find(":selected").text() == "") {
            vl = false;
            message += "Select Property Type.<br/>";
        }
        if ($("#PropertySizeID").find(":selected").text() == "") {
            vl = false;
            message += "Select Property Size.<br/>";
        }
        if ($("#FlatName").val() == "") {
            vl = false;
            message += "Insert Property Name.<br/>";
        }
        if ($("#fNmae").val() == "") {
            vl = false;
            message += "Insert Name of Applicant.<br/>";
        }
        if ($("#PanNo").val() == "") {
            vl = false;
            message += "Insert Pan Card No.<br/>";
        }
        if ($("#saleDate").val() == "") {
            vl = false;
            message += "Please insert Date.";
        }

        $("#ErrorMessage").html(message);
        return vl;
    }

    $scope.checkandFillDetail = function () {
        document.getElementById("stepid").value = 1;


        if (document.getElementById("HdnTab").value == "true") {
            window.open("../Payment/Index", "_self");

        } else {
            var activeTAB = $(".steps").find(".active").text();

            if (document.getElementById("stepid").value == 1) {
                document.getElementById("stepid").value = "";
                PlanType = document.getElementById("PlanType").value;
                if (PlanType == '2' || PlanType == '1' || PlanType == '4') {
                    document.getElementById("drpinatallment").disabled = true;
                    document.getElementById("drpinterval").disabled = true;
                }
                else {
                    document.getElementById("drpinatallment").disabled = false;
                    document.getElementById("drpinterval").disabled = false;
                }

                $.ajax({
                    type: 'POST',
                    url: 'GetSaleDetailByFlatId', // we are calling json method
                    dataType: 'json',
                    success: function (flatdetail) {

                        document.getElementById("lblFlatNo").innerHTML = flatdetail[0].FlatNo;
                        document.getElementById("FlatName").innerHTML = flatdetail[0].FlatNo;
                        document.getElementById("lblSaleDate").innerHTML = flatdetail[0].SaleDate;

                        if (activeTAB.indexOf("2Step") > 0) {

                            if (flatdetail[1].IsPaymentDetails != null && flatdetail[1].IsPaymentDetails == "Yes") {
                                document.getElementById("FlatName").innerHTML = flatdetail[0].FlatNo;
                                document.getElementById("CustomeName").innerHTML = flatdetail[0].CustomerName;
                                document.getElementById("SalePrice").innerHTML = flatdetail[2].paymentInstallmentList[0].TotalAmount;

                                $("#DueAmount").val(flatdetail[2].paymentInstallmentList[0].DueAmount);
                                var ddate = new Date(flatdetail[2].paymentInstallmentList[0].DueDate);//parseInt(value.DueDate.substr(6)));// value.DueDate;
                                // ddate = flatdetail[2].paymentInstallmentList[0].DueDate;
                                var mth = ddate.getMonth() + 1;
                                if (mth < 10) mth = "0" + mth;
                                $("#DueDate").val(ddate.getDate() + "/" + mth + "/" + ddate.getFullYear());

                                $("#ddlPayment").empty();
                                var iCount = 1;
                                $.each(flatdetail[2].paymentInstallmentList, function (key, value) {
                                    if (iCount == 1) {
                                        $("#ddlPayment").append($("<option selected></option>").val
                                        (value.InstallmentID).html(value.InstallmentNumber));
                                    } else {
                                        $("#ddlPayment").append($("<option></option>").val
                                            (value.InstallmentID).html(value.InstallmentNumber));
                                    }
                                    iCount = iCount + 1;
                                });
                                $("#ddlPayment").attr("disabled", "disabled");
                                $("#DueAmount").attr("disabled", "disabled");
                                $("#DueDate").attr("disabled", "disabled");
                            }
                        }
                    },
                    error: function (ex) {
                        alert('Failed to retrieve states.' + ex);
                    }
                });

            }
        }
        return true;
    }


    $('#loading').hide();
});


function EnalbleButton() {
    $(".row .centre_sec").css('display', '');
    $(".row").find(".centre_sec").css('display', '');
}

function calculatebsp(value, id) {
    var txtID = id;
    var initialBSP = id.split('_')[1];
    id = id.split('_')[0];
    var Plsvalue;
    var plsAmountvalue = parseFloat(0);
    var Plcval = document.getElementById(id).value;
    if (Plcval != "") {
        Plsvalue = document.getElementById("txtplcprice").value;
        plsAmountvalue = ((parseFloat(Plsvalue) * Plcval) / 100);
    }

    // if (value != initialBSP) {
    var val = document.getElementById(txtID).value;
    if (val != "") {
        var damt = document.getElementById("txtsaleprice").value;
        var dueamount = (((parseFloat(damt) * val) / 100) + parseFloat(plsAmountvalue));

        document.getElementById("bspdueamount" + id).innerHTML = parseFloat(dueamount).toFixed(2);
        document.getElementById("bspdueamount" + id).value = parseFloat(dueamount).toFixed(2);
    }
    else {
        document.getElementById("bspdueamount" + id).innerHTML = dueamount = document.getElementById("bspbaseamount" + id).innerHTML;
    }
    document.getElementById("bsptotal").innerHTML = "";
    var total = 0;
    $('.dueamount').each(function () {
        total = parseFloat(document.getElementById(this.id).innerHTML) + parseFloat(total);
        // alert(total);
    });
    document.getElementById("bsptotal").innerHTML = total.toFixed(0);
    // }
}



function calculatePLC(value, id) {




    var txtID = id;
    var initialBSP = id.split('_')[1];
    id = id.split('_')[0];
    var Plsvalue;
    var plsAmountvalue = 0;
    var Plcval = document.getElementById(id).value;
    if (Plcval != "") {
        Plsvalue = document.getElementById("txtplcprice").value;
        plsAmountvalue = ((parseFloat(Plsvalue) * Plcval) / 100);
    }

    // if (value != initialBSP) {


    var test = document.getElementsByClassName("bsppercentage_" + id);

    if (test != "") {
        var val = test[0].value;
        var damt = document.getElementById("txtsaleprice").value;
        var dueamount = (((parseFloat(damt) * val) / 100) + parseFloat(plsAmountvalue));

        document.getElementById("bspdueamount" + id).innerHTML = parseFloat(dueamount).toFixed(2);
        document.getElementById("bspdueamount" + id).value = parseFloat(dueamount).toFixed(2);
    }
    else {
        document.getElementById("bspdueamount" + id).innerHTML = dueamount = document.getElementById("bspbaseamount" + id).innerHTML;
    }
    document.getElementById("bsptotal").innerHTML = "";
    var total = 0;
    $('.dueamount').each(function () {
        total = parseFloat(document.getElementById(this.id).innerHTML) + parseFloat(total);
        // alert(total);
    });
    document.getElementById("bsptotal").innerHTML = total.toFixed(0);




    //var txtID = id;
    //var initialBSP = id.split('_')[1];
    //id = id.split('_')[0];
    //// if (value != initialBSP) {
    //var val = document.getElementById(txtID).value;
    //if (val != "") {



    //    var damt = document.getElementById("txtplcprice").value;
    //    var dueamount = (parseFloat(damt) * val) / 100;
    //    dueamount = (parseFloat(dueamount) + parseFloat(document.getElementById("bspdueamount" + id).innerHTML));
    //    document.getElementById("bspdueamount" + id).innerHTML = parseFloat(dueamount).toFixed(2);
    //    document.getElementById("bspdueamount" + id).value = parseFloat(dueamount).toFixed(2);
    //}
    //else {
    //    document.getElementById("bspdueamount" + id).innerHTML = dueamount = document.getElementById("bspbaseamount" + id).innerHTML;
    //}
    //document.getElementById("bsptotal").innerHTML = "";
    //var total = 0;
    //$('.dueamount').each(function () {
    //    total = parseFloat(document.getElementById(this.id).innerHTML) + parseFloat(total);
    //    // alert(total);
    //});
    //document.getElementById("bsptotal").innerHTML = total.toFixed(0);
    // }
}




function GetPaymentInstallment() {
    $http({
        method: 'Get',
        url: 'GetPaymentInstallment'
    }).success(function (data, status, headers, config) {
        $scope.payments = data;
    }).error(function (data, status, headers, config) {
        $scope.message = 'Unexpected Error';
    });
}


function PlanTypeChange(obj) {
    var sv = $(obj).val();
    var ps = $(obj).find(":selected").text();
    if (sv == '3') {
        $("#normaldiv").show();
    }
    else {
        $("#normaldiv").hide();
    }
    $("#hidPlanType").val(ps)
    $("#lblPlanName").text(ps);
}
function PaymentModeChange(obj) {
    var selectedValue = $(obj).val();
    if (selectedValue == '1' || selectedValue == '7') {
        $("#divCheque").hide();
    } else {

        $.ajax({
            url: 'GetBanks',
            type: 'post',
            success: function (states) {
                // states is your JSON array
                var $select = $('#BankName');
                $.each(states, function (i, state) {
                    $('<option>', {
                        value: state.BankID
                    }).html(state.BankName).appendTo($select);
                });
            }
        });
        $("#divCheque").show();
    }
}

function GetInstallments(saledate, saleprice, ddlInstallment, ddlInterval, PlanType, plcPrice) {
    var vli = ValidatePaymentGen();
    if (vli == false) {
        $('#myModal').modal('show');
    }
    else {
        $('#loading').show();
        saledate = document.getElementById(saledate).innerHTML;
        saleprice = document.getElementById(saleprice).value;
        ddlInstallment = document.getElementById(ddlInstallment).value;
        ddlInterval = document.getElementById(ddlInterval).value;
        PlanType = document.getElementById(PlanType).value;
        var PlanTypeID = document.getElementById('hidPlanType').value;
        plcPrice = document.getElementById(plcPrice).value;
        if (PlanType == 3) {
            //  ddlInstallment = document.getElementById("property").value;
        }
        $.ajax({
            url: 'GetInstallments',
            type: 'post',
            dataType: 'json',
            data: '{saledate: "' + saledate + '",saleprice: "' + saleprice + '",ddlInstallment: "' + ddlInstallment + '",ddlInterval: "' + ddlInterval + '",PlanType: "' + PlanType + '",plcPrice:"' + plcPrice + '", saleID:"",flatid:"",PlanTypeID:"' + PlanTypeID + '" }',
            contentType: 'application/json',
        }).success(function (result) {
            alert(result)
            $('#loading').hide();
            window.location.href = urlPath + '/';
        }).error(function (err) {
            $('#Divsave').show();
            $('#loading').hide();
            if (err.responseText == "success") {
                alert("Data is deleted successfully.");
                pageload();
            }
            else {
                document.getElementById("divinstallment").innerHTML = err.responseText;
            }
        }).then(function () {
            $('#loading').hide();
        })
    }
}
function ValidatePaymentGen() {
    var vl = true;
    var message = "";
    if ($("#txtsaleprice").val() == "") {
        vl = false;
        message += "Insert Sale Price of Property. <br/>";
    }
    $("#ErrorMessage").html(message);
    return vl;
}
function SaveInstallments(saledate, saleprice, ddlInstallment, ddlInterval, PlanType) {
    $('#loading').show();
    getid();
    saledate = document.getElementById(saledate).innerHTML;

    saleprice = document.getElementById(saleprice).value;

    var pplprice = document.getElementById("txtplcprice").value;


    ddlInstallment = document.getElementById(ddlInstallment).value;
    ddlInterval = document.getElementById(ddlInterval).value;
    EventName = document.getElementById("EventId").value;
    var bspPercentage = "";
    $('.bsppercentage').each(function () {
        bspPercentage += document.getElementById(this.id).value + ",";
    });
    $('.dueamount').each(function () {
        document.getElementById("EventAmount").value += parseFloat(document.getElementById(this.id).innerHTML) + ",";
    });
    var evName = document.getElementById("EventName").value;

    $.ajax({
        url: 'SaveInstallments',
        type: 'post',
        dataType: 'json',
        data: '{saledate: "' + saledate + '",saleprice: "' + saleprice + '", salepriceword:"", ddlInstallment: "' + ddlInstallment + '",ddlInterval: "' + ddlInterval + '",EventName:"' + EventName + '",bspPercentage:"' + bspPercentage + '",saleID:"",flatID:"",EvName:"' + evName + '",Amount:"' + document.getElementById("EventAmount").value + '",PPLPrice:"' + pplprice + '" }',
        contentType: 'application/json',
        success: function (result) {
            alert("Data is added successfully.");
            document.getElementById("HdnTab").value = "true";
            $(".row .centre_sec").css('display', 'none');
            $(".row").find(".centre_sec").css('display', 'none');
        },
        error: function (err) {
            if (err.responseText == "success") {
                alert("Data is added successfully.");
                document.getElementById("HdnTab").value = "true";
                $(".row .centre_sec").css('display', 'none');
                $(".row").find(".centre_sec").css('display', 'none');
                pageload();
                GetInstallmetnDetailsBySaleId(ddlInterval);
                $('#loading').hide();
            }
            else {
                $(".row .centre_sec").css('display', 'none');
                $(".row").find(".centre_sec").css('display', 'none');
                document.getElementById("HdnTab").value = "true";
                document.getElementById("divinstallment").innerHTML = err.responseText;
                GetInstallmetnDetailsBySaleId(ddlInterval);
                $('#loading').hide();
            }
        },
        complete: function () {
            $('#loading').hide();
        }
    });
}
function GetInstallmetnDetailsBySaleId(SaleId) {
    $.ajax({
        url: 'GetInstallmetnDetailsBySaleId',
        type: 'post',
        dataType: 'json',
        data: '{FlatId: "' + SaleId + '" }',
        contentType: 'application/json',
        success: function (result) {
            window.location.href = urlPath + '/';
        },
        error: function (err) {
            if (err.responseText == "success") {
                //alert("Data is deleted successfully.");
                document.getElementById("divMainInstallment").innerHTML = err.responseText;
            }
            else {
                document.getElementById("divMainInstallment").innerHTML = err.responseText;
            }
        },
        complete: function () {
        }
    });
}
function getid() {
    document.getElementById("EventId").value = "";
    $('.dropdwn').each(function () {
        if (this.value) {
            document.getElementById("EventId").value += this.value + ",";
            document.getElementById("EventName").value += $(this).find(":selected").text() + ",";
        }
    });
}




function Test() {
    $.ajax({
        method: 'POST',
        url: '/flat/InsertPaymentDetails',
        data: { InstallmentNo: $("#ddlPayment").find(":selected").text(), DueAmount: $("#DueAmount").val(), DueDate: $("#DueDate").val(), PaymentMode: $("#PaymentMode").val(), ChequeNo: $("#ChequeNo").val(), ChequeDate: $("#chequeDate").html(), BankName: $("#BankName").val(), BankBranch: $("#BankBranch").val(), Remarks: $("#Remarks").val(), PayDate: $("#paymentDate").val(), Amtrcvdinwrds: $("#PaymentAmountInWords").val(), ReceivedAmount: $("#ReceivedAmount").val(), IsPrint: $("#chkPrint").is(":checked"), IsEmailSent: $("#chkSendEmail").is(":checked"), EmailTo: $("#txtSendEmail").val() },
    }).success(function (data) { //Showing success message $scope.status = "The Person Saved Successfully!!!";
        alert("Payment Details has been saved successfully!");
        $("#ReceivedAmount").val("");
        $("#PaymentAmountInWords").val("");
        $("#paymentDate").val("");
        $("#txtSendEmail").val("");
        $("#Remarks").val("");
        $scope.Error = 'Unable to save Payment Details: ';
    }).error(function (error) {
        //Showing error message
        alert("Payment Details has been saved successfully !!!");
        $scope.Error = 'Unable to save Payment Details: ' + error.message;
    });
}

function Test1() {
    $.ajax({
        method: 'POST',
        url: '/flat/InsertPaymentDetailData',
        data: { InstallmentNo: $("#ddlPayment").find(":selected").text(), DueAmount: $("#DueAmount").val(), DueDate: $("#DueDate").html(), ReceivedAmount: $("#ReceivedAmount").val(), PayDate: $("#paymentDate").html() },
    }).success(function (data) { //Showing success message $scope.status = "The Person Saved Successfully!!!";
        alert("Payment Details has been saved successfully!");

    }).error(function (error) {
        //Showing error message
        //   $scope.status = 'Unable to save Payment Details: ' + error.message;
    });
}

var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];


function inWords(num) {

    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return;
    var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'Only ' : '';
    $('#PaymentAmountInWords').val(str)
}

function inWordsSaleRateInWords(num) {
    var str = NoToWord(num);
    $("#Saleinword").html(str);
    $('#SaleRateInWords').val(str)
}
function inWordsBookingRateInWords(num) {
    var str = NoToWord(num);
    $("#BookingInword").html(str);
    $('#BookingAmountWord').val(str)
}
function NoToWord(num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return;
    var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'Only ' : '';
    return str;
}

function AmountPer(ins) {
    alert(ins);
}

