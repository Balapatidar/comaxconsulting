﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Comax.Web.Startup))]
namespace Comax.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
