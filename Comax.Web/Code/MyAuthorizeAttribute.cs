﻿using Comax.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Comax.Web.Code;
using System.Collections;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Hosting;
using AutoMapper;

namespace Comax.Web.Code
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class MyAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var request = httpContext.Request;
            string controller = request.RequestContext.RouteData.Values["controller"].ToString();
            string action = request.RequestContext.RouteData.Values["action"].ToString();

            dbSBPEntities2 context = new dbSBPEntities2();
            string username = System.Security.Principal.GenericPrincipal.Current.Identity.Name;
            //var model = (from md in context.ModuleLists
            //             join us in context.UserAccesses on md.ModuleListID equals us.ModuleListID
            //             where md.Controller == controller && md.ActionName == action && us.UserName == username 
            //             select new { Dev = md }).ToList();
            var model = context.UserAccesses.Where(md => md.ControllerName == controller && md.ActionName == action && md.UserName == username).ToList();
            if (model.Count == 0)
            {
                httpContext.Response.Redirect("~/Home/AccessDenied");
                return true;
            }
            else
            {
                return true;
            }
        }
    }
}