﻿using Comax.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Comax.Web.Code
{
    public class DocumentService
    {
        public int SaveDocument(Agreement ag)
        {
            using (dbSBPEntities2 context = new dbSBPEntities2())
            {
                var model = context.Agreements.Where(g => g.SaleID == ag.SaleID).FirstOrDefault();
                if(model==null)
                { 
                context.Agreements.Add(ag);
                int  i = context.SaveChanges();
                return i;
                }
                else
                {
                    context.Agreements.Add(ag);
                    context.Entry(ag).State = EntityState.Modified;
                    int i = context.SaveChanges();
                    return i;
                }
            }
        }
    }
}