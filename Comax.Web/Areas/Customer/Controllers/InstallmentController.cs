﻿using Comax.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Comax.Web.Areas.Customer.Models;
using System.Globalization;
using Comax.Web.Code;

namespace Comax.Web.Areas.Customer.Controllers
{

    public class InstallmentController : Controller
    {
        DataFunctions obj = new DataFunctions();
        dbSBPEntities1 SbpEntity = new dbSBPEntities1();
        // GET: Customer/Installment
        [MyAuthorize]
        public ActionResult Event()
        {
            return View();
        }

        // Search flat 
        public JsonResult GetFlatList(string flatname, string pid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var Flat = obj.GetDataTable("Select * from tblsflat where FlatName='" + flatname + "' and PID='" + pid + "' and Status=1");
            var v = Flat.AsEnumerable().ToList();

            return Json(new { Result = (from i in v select new { FlatID = i["FlatID"], FlatName = i["FlatName"] }) }, JsonRequestBehavior.AllowGet);
        }
        // GET Payment 
        public JsonResult GetPayment(string saleid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var payment = obj.GetDataTable("Select * from tblSPayment where Saleid='" + saleid + "'");
            var v = payment.AsEnumerable().ToList();
            return Json(new { Payments = (from p in v select new { TransactionID = p["TransactionID"], InstallmentNo = p["InstallmentNo"], PaymentNo = p["PaymentNo"], PaymentDate = p["PaymentDate"], TotalAmount = p["TotalAmount"], Amount = p["Amount"] }) }, JsonRequestBehavior.AllowGet);
        }

        // GET Total Payment 
        public JsonResult GetTotalPayment(string saleid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var payment = obj.GetDataTable("select sum (DueAmount) as TotalAmount from tblSInstallmentDetail where saleid='" + saleid + "'");
            var v = payment.AsEnumerable().ToList();
            return Json(new { TotalPaid = v[0]["TotalAmount"] }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFlatSale(string flatid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            //var payment = obj.GetDataTable("Select * from tblssaleflat where FlatID='" + flatid + "'");
            //var v = payment.AsEnumerable().ToList();
            var payment = obj.GetDataTable("Select s.SaleID,PlanType.PlanTypeName,s.FlatID,s.Aggrement,s.SaleDate,s.SaleRate,s.ServiceTaxPer,s.ServiceTaxAmount,s.BookingAmount,s.CustomerID,s.PropertyID,s.PropertyTypeID,s.PropertySizeID,s.PlanID,s.PaymentFor, c.* from tblssaleflat s inner join Customer c on s.CustomerID=c.CustomerID  INNER JOIN PlanType  ON s.PlanID = PlanType.PlanID where s.FlatID='" + flatid + "'");
            var v = payment.AsEnumerable().ToList();
            return Json(new { Sale = (from s in v select new { SaleID = s["SaleID"], SaleRate = s["SaleRate"], ServiceTaxPer = s["ServiceTaxPer"], ServiceTaxAmount = s["ServiceTaxAmount"], SaleDate = s["SaleDate"], BookingAmount = s["BookingAmount"], CustomerName = s["AppTitle"] + " " + s["FName"] + " " + s["MName"] + " " + s["LName"], PName = s["PName"], Mobile = s["MobileNo"], EmailID = s["EmailID"], CustomerID = s["CustomerID"], PlanID = s["PlanID"],PlanName=s["PlanTypeName"] }) }, JsonRequestBehavior.AllowGet);
        }
        // GET all Installments
        public string GetInstallment(string saleid)
        {
            List<tblSSaleFlat> lstPropertyDetails = new List<tblSSaleFlat>();
            var installment = obj.GetDataTable("select  T2.EventName,T1.* from tblSInstallmentDetail T1 " +
           " left join tblEventMaster T2 on T1.EventId=T2.EventId " +
           " where SaleID='" + saleid + "'");
            var v = installment.AsEnumerable().ToList();
            List<EventModel> em = new List<EventModel>();
            foreach (var s in v)
            {
                DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
                dtinfo.ShortDatePattern = "dd/MM/yyyy";
                dtinfo.DateSeparator = "/";
                string ddate = "";
                if (s["DueDate"] != null)
                {
                    string dt = s["DueDate"].ToString();
                    if (dt == "") ddate = dt;
                    else
                        ddate = Convert.ToDateTime(s["DueDate"].ToString()).ToString("dd/MM/yyyy");
                }
                string mdate = "";
                if (s["ModifyDate"] != null)
                {
                    string dt = s["ModifyDate"].ToString();
                    if (dt == "") mdate = dt;
                    else
                        mdate = Convert.ToDateTime(s["ModifyDate"].ToString()).ToString("dd/MM/yyyy");
                }
                em.Add(new EventModel { InstallmentID = Convert.ToInt32(s["InstallmentID"]), DueAmount = Convert.ToDecimal(s["DueAmount"]), TotalAmount = Convert.ToDecimal(s["TotalAmount"]), InstallmentNo = s["InstallmentNo"].ToString(), EventName = s["EventName"].ToString(), DueDate = ddate, ModifyDate = mdate });
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(em);
            //return Json(s,JsonRequestBehavior.AllowGet);
            //   var vv= new { Installments = (from s in v select new { InstallmentID = s["InstallmentID"], InstallmentNo = s["InstallmentNo"], EventName = s["EventName"], DueDate = s["DueDate"], DueAmount = s["DueAmount"], ModifyDate = s["ModifyDate"], TotalAmount = s["TotalAmount"] }) };
            // return Json(new { Installments = (from s in v select new { InstallmentID = s["InstallmentID"], InstallmentNo = s["InstallmentNo"], EventName = s["EventName"], DueDate = s["DueDate"], DueAmount = s["DueAmount"], ModifyDate = s["ModifyDate"], TotalAmount = s["TotalAmount"] }) }, JsonRequestBehavior.AllowGet);
        }

        public string GetInstallmentByInstallId(int InstallmentID, string ModifyDate)
        {
            DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
            dtinfo.ShortDatePattern = "dd/MM/yyyy";
            dtinfo.DateSeparator = "/";
            DateTime dt = new DateTime();
            if (ModifyDate != null && ModifyDate != "")
            {
                dt = Convert.ToDateTime(ModifyDate, dtinfo);
                obj.ExecuteNonQuery("update tblSInstallmentDetail set DueDate='" + dt + "' where InstallmentID='" + InstallmentID + "'");
                return "Event Completion date updated.";
            }
            else
                return "Invalid Date format.";
        }
        public string GetInstallmetnDetailsByFlatId(string saleid)
        {


            StringBuilder l_StringBuilder = new StringBuilder();


            // DataSet dsSet = obj.GetDataSet("select * From tblsinstallmentdetail where  FlatID='" + id + "'");

            //if (dsSet != null && dsSet.Tables.Count > 0)
            //{
            //    var SaleId = dsSet.Tables[0].Rows[0]["SaleId"].ToString();

            var list = (SbpEntity.GetInstallMentDetailsBySaleId(saleid).ToList());
            //if (Session["PlanType"].ToString() == "1")
            //{
            var downplayPlanEevnts = "";

            l_StringBuilder.Append("<table class='table table-bordered table-striped particular_tbl'>");
            l_StringBuilder.Append("<thead><tr><th>InstallmentNo</th><th>Due Date</th><th>ModifiedDate</th><th>Due Amount</th></tr></thead><tbody>");
            if (list.Count > 0)
            {
                decimal dueAmount;
                string dueDate = String.Empty;
                decimal TotalAmount = 0;

                for (int i = 0; i < list.Count; i++)
                {
                    dueAmount = decimal.Parse(list[i].DueAmount.ToString());
                    downplayPlanEevnts = Convert.ToString(list[i].InstallmentNo.ToString());
                    if (!String.IsNullOrEmpty(Convert.ToString(list[i].DueDate)))
                    {
                        l_StringBuilder.Append("<tr><td>" + downplayPlanEevnts.ToString() + "</td></td><td>" + obj.Text_IndianDateFormat(list[i].DueDate.ToString()) + "</td><td><input type='text' name='txtDueDate' value='" + "" + "' ></td><td>" + dueAmount + "</td></td><tr>");
                    }
                    else
                    {
                        l_StringBuilder.Append("<tr><td>" + downplayPlanEevnts.ToString() + "</td></td><td>" + dueDate + "</td><td>" + dueAmount + "</td><tr>");
                    }

                    TotalAmount = TotalAmount + dueAmount;
                }
                l_StringBuilder.Append("<tr><td></td><td></td><td>Total</td><td><b>" + TotalAmount + "</b></td><tr>");
            }
            l_StringBuilder.Append("</tbody>");
            //}
            return l_StringBuilder.ToString();


        }
    }
}