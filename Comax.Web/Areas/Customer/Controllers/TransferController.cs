﻿using Comax.Web.Code;
using Comax.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Comax.Web.Areas.Customer.Controllers
{
    
    public class TransferController : Controller
    {
        DataFunctions obj = new DataFunctions(); clsNW1 clsnew = new clsNW1();
        dbSBPEntities1 SbpEntity = new dbSBPEntities1();
        DataTable dtEvent;
        // GET: Customer/Transfer
       [MyAuthorize]
        public ActionResult Index(int id)
        {
            ViewBag.ID = id;
            return View();
        }
        [MyAuthorize]
        public ActionResult RefundProperty(int id)
        {
            ViewBag.ID = id;
            return View();
        }

        #region
        public string SaveCustomer(FlatSale newCust)
        {
            using (dbSBPEntities2 SbpEntity = new dbSBPEntities2())
            {
                try
                {
                    if (newCust.AppTitle == null) newCust.AppTitle = "";
                    if (newCust.AppFName == null) newCust.AppFName = "";
                    if (newCust.AppMName == null) newCust.AppMName = "";
                    if (newCust.AppLName == null) newCust.AppLName = "";
                    if (newCust.CoTitle == null) newCust.CoTitle = "";
                    if (newCust.CoFName == null) newCust.CoFName = "";
                    if (newCust.CoMName == null) newCust.CoMName = "";
                    if (newCust.CoLName == null) newCust.CoLName = "";

                    DateTime dt = DateTime.ParseExact(newCust.SrtSaleDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    newCust.BookingDate = Convert.ToDateTime(dt);
                    int i = SbpEntity.INSERT_RBookTransferFlat(newCust.FlatName, newCust.SaleID, newCust.SaleDate, newCust.BookingAmount, newCust.AppFName, newCust.AppMName, newCust.AppLName, newCust.Title,
                    newCust.PName, newCust.Address1, newCust.Address2, newCust.City, newCust.Distt, newCust.State, newCust.Country, newCust.AppPAN, newCust.MobileNo, newCust.DateOfBirth,
                    newCust.CoFName, newCust.CoMName, newCust.CoLName, newCust.CoAddress1,
                    newCust.CoAddress2, newCust.CoCity, newCust.CoState, newCust.CoCountry, newCust.CoPAN, newCust.CoMobileNo, newCust.AlternateMobile, newCust.LandLine,
                    newCust.EmailID, newCust.AlternateEmail, newCust.LoanAmount, newCust.LienField, newCust.BankID, newCust.IsPAN,
                    newCust.IsPhoto, newCust.IsAddressPf, newCust.Type, newCust.CustomerID, newCust.SecCoFName, newCust.SecCoMName, newCust.SecCoLName, newCust.SecCoAddress1,
                    newCust.SecCoAddress2, newCust.SecCoCity, newCust.SecCoState, newCust.SecCoCountry, newCust.SecCoMobileNo, newCust.SecCoPAN,
                    newCust.CoTitle, newCust.CoPName, newCust.SecCoTitle, newCust.SecCoPName, newCust.IsConstruction, newCust.AppTitle, newCust.CoAppTitle, newCust.SecCoAppTitle, newCust.IsRationCard, newCust.IsDrivingLicence, newCust.IsVoterCard, newCust.IsPassport, newCust.PhotoImagePath, newCust.AddPfImagePath, newCust.DVImagePath, newCust.VoterCardImagePath, newCust.PassportImagePath,
                    newCust.RationCardImagePath, newCust.Remarks, newCust.CoDOB, newCust.SecCoDOB, newCust.PANImagePath, newCust.CoIsPAN, newCust.CoIsPhoto, newCust.CoIsAddressPf, newCust.CoIsRationCard, newCust.CoIsDrivingLicence, newCust.CoIsVoterCard, newCust.CoIsPassport, newCust.CoPhotoImagePath, newCust.CoAddPfImagePath, newCust.CoDVImagePath, newCust.CoVoterCardImagePath, newCust.CoPhotoImagePath,
                    newCust.CoRationCardImagePath, newCust.CoPANImagePath, newCust.SecCoIsPAN, newCust.SecCoIsPhoto, newCust.CoIsAddressPf, newCust.SecCoIsRationCard, newCust.SecCoIsDrivingLicence, newCust.SecCoIsVoterCard, newCust.SecCoIsPassport, newCust.SecCoPhotoImagePath,
                    newCust.SecCoAddPfImagePath, newCust.SecCoDVImagePath, newCust.SecCoVoterCardImagePath, newCust.SecCoPhotoImagePath, newCust.SecCoRationCardImagePath, newCust.SecCoPhotoImagePath, newCust.BankBranch, newCust.PinCode, newCust.ExecutiveName, newCust.CoPinCode, newCust.SecCoPinCode, newCust.TransferAmount, newCust.affidavit, newCust.PropertyID, newCust.PaymentFor, newCust.BookingDate, newCust.PropertyTypeID, newCust.PropertySizeID, newCust.PlanID, User.Identity.Name,newCust.IsChangeInstallmentPlan);
                    if (i > 0)
                    {
                        long id = SbpEntity.tblSSaleFlats.Where(s => s.SaleID == newCust.SaleID).FirstOrDefault().CustomerID.Value;
                        PropertyTransfer pt = new PropertyTransfer();
                        pt.SaleID = newCust.SaleID;
                        pt.NewCustomerID = (int)id;
                        pt.OldCustomerID = newCust.OldCustomerID;
                        pt.OldPlanType = newCust.OldPlanType;
                        pt.NewPlanType = newCust.PlanName;
                        pt.TransferAmount = newCust.TransferAmount;
                        pt.TransferDate = Convert.ToDateTime(dt); 
                        pt.UserName = User.Identity.Name;
                        SbpEntity.PropertyTransfers.Add(pt);
                        SbpEntity.SaveChanges();
                        return Newtonsoft.Json.JsonConvert.SerializeObject("Yes");
                    }
                    else
                    {
                        return Newtonsoft.Json.JsonConvert.SerializeObject("No");
                    }
                }
                catch (Exception ex)
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject("No");
                }
            }
        }
        public string GetSaleIDByPName(string PID, string propertyname)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            int proid = Convert.ToInt32(PID);
            int sid = (from s in context.tblSSaleFlats join f in context.tblSFlats on s.FlatID equals f.FlatID where f.PID == proid && f.FlatName == propertyname && f.Status == 1 select s.SaleID).FirstOrDefault();
            return sid.ToString();
        }
        public string GetPropertyInfoByFlatID(string fid, string saleid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            int id = Convert.ToInt32(fid);
            int selid = Convert.ToInt32(saleid);
            Dictionary<string, string> dic = new Dictionary<string, string>();
            string fname = context.tblSFlats.Where(f => f.FlatID == id).FirstOrDefault().FlatName;
            var fmodel = context.tblSSaleFlats.Where(s => s.SaleID == selid).FirstOrDefault();
            int pid = (int)fmodel.PropertyID;
            int tid = (int)fmodel.PropertyTypeID;
            int sid = (int)fmodel.PropertySizeID;
            int plnid = (int)fmodel.PlanID;
            dic.Add("PlanID", fmodel.PlanID.Value.ToString());
            string planname = context.PlanTypes.Where(p => p.PlanID == plnid).FirstOrDefault().PlanTypeName;
            dic.Add("PlanName", planname);
            dic.Add("FlatName", fname);
            string pname = context.tblSProperties.Where(p => p.PID == pid).FirstOrDefault().PName;
            dic.Add("PropertyName", pname);
            dic.Add("PID", pid.ToString());

            string ptype = context.tblSPropertyTypes.Where(p => p.PropertyTypeID == tid).FirstOrDefault().PType;
            dic.Add("PropertyType", ptype);
            var psize = context.tblSPropertySizes.Where(p => p.PropertySizeID == sid).FirstOrDefault();
            string size = psize.Size.Value.ToString() + " " + psize.Unit;
            dic.Add("PropertySize", size);

            return Newtonsoft.Json.JsonConvert.SerializeObject(dic);
        }

        public string GetInstallments(string saledate, string saleprice, int ddlInstallment, int ddlInterval, int PlanType, string plcPrice, string saleID, string flatid, string PlanTypeID)
        {
            if (saleID != "")
            {
                Session["SaleID"] = saleID;
            }
            if (flatid != "")
            {
                Session["FlatId"] = flatid;
            }
            // Session["SaleID"] = "3948";
            Session["PlanType"] = PlanType;
            StringBuilder l_StringBuilder = new StringBuilder();
            if (PlanType == 1)
            {
                #region Down Payment

                Session["table"] = null;
                NormalDownPaymentPlan(saledate, saleprice, ddlInstallment, ddlInstallment, PlanType, plcPrice);

                DataTable tb = new DataTable();
                if (Session["table"] != null)
                {
                    tb = Session["table"] as DataTable;
                    if (tb.Rows.Count > 0)
                    {
                        l_StringBuilder.Append("<table class='table table-bordered table-striped particular_tbl'>");
                        l_StringBuilder.Append("<thead><tr><th>Installment No.</th><th>Due Date</th><th>Amount</th></tr></thead><tbody>");

                        double total = 0;
                        int k = 0;
                        for (int j = 0; j < tb.Rows.Count; j++)
                        {

                            l_StringBuilder.Append("<tr><td>" + tb.Rows[j]["InstallmentNo"] + "</td><td>" + tb.Rows[j]["DueDate"] + "</td></td><td class='dueamount' id='bspdueamount" + k + "'>" + tb.Rows[j]["DueAmount"] + "</td><td id='bspbaseamount" + k + "' style='display:none'>" + tb.Rows[j]["DueAmount"] + "</td><tr>");
                            total += double.Parse(Convert.ToString(tb.Rows[j]["DueAmount"]));
                        }
                        l_StringBuilder.Append("<td colspan='2'>Total</td><td><b>" + total.ToString() + "</b></td></tr></table>");

                    }
                }


                #endregion Normal
            }
            else if (PlanType == 2)
            {
                Session["table"] = null;
                #region Construction plan
                ConstructionPlan(saledate, saleprice, ddlInstallment, ddlInterval, PlanType, plcPrice, PlanTypeID);
                ConstructionEvent();
                DataTable tb = new DataTable();
                if (Session["table"] != null)
                {
                    tb = Session["table"] as DataTable;
                    if (tb.Rows.Count > 0)
                    {
                        l_StringBuilder.Append("<table class='table table-bordered table-striped particular_tbl'>");
                        l_StringBuilder.Append("<thead><tr><th>Installment No.</th><th>Event</th><th>Due Date</th><th style='width:60px'>BSP %</th><th style='width:105px'>PLC % (If Any)</th><th>Amount</th></tr></thead><tbody>");

                        DateTime DueDate;
                        string SDate = "";
                        double total = 0;
                        int k = 0;
                        /*Balkrishna */
                        DueDate = Convert.ToDateTime(obj.Text_IndianDateFormat(Convert.ToString(saledate)));
                        for (int j = 0; j < tb.Rows.Count; j++)
                        {
                            k++;
                            if (j == 0)
                            {
                                tb.Rows[j]["DueDate"] = obj.DB_IndianDateFormat(DueDate.ToShortDateString());
                            }
                            else if (j == 1)
                            {                                                                          /* Add By Balkrishna */
                                DueDate = Convert.ToDateTime(DueDate.AddDays(60).ToString());
                                SDate = obj.DB_IndianDateFormat(DueDate.ToShortDateString());
                                tb.Rows[j]["DueDate"] = SDate;
                            }
                            else
                            {
                                DueDate = Convert.ToDateTime(DueDate.AddMonths(4).ToString());                 /*Balkrishna */
                                SDate = obj.DB_IndianDateFormat(DueDate.ToShortDateString());
                                tb.Rows[j]["DueDate"] = SDate;
                            }
                            if (k >= 4)
                            {
                                tb.Rows[j]["DueDate"] = "";
                            }
                            l_StringBuilder.Append("<tr><td>" + tb.Rows[j]["InstallmentNo"] + "</td><td>" + GetEventDetails(Convert.ToString(tb.Rows[j]["InstallmentNo"])) + "</td><td><input type='text' name='txtDueDate' value='" + tb.Rows[j]["DueDate"] + "' ></td><td  style='width:60px'><input id='" + k + "_" + tb.Rows[j]["BSP"] + "' type='text'  style='width:60px' class='bsppercentage' name='txtBsp' onblur='calculatebsp(this.value,this.id)' value='" + tb.Rows[j]["BSP"] + "' ></td><td><input id='" + k + "' type='text'  style='width:60px' class='plcpercentage' name='txtplc' onblur='calculatePLC(this.value,this.id)' value='" + tb.Rows[j]["PLC"] + "' ></td><td class='dueamount' id='bspdueamount" + k + "'>" + tb.Rows[j]["DueAmount"] + "</td><td id='bspbaseamount" + k + "' style='display:none'>" + tb.Rows[j]["DueAmount"] + "</td><tr>");
                            total += double.Parse(Convert.ToString(tb.Rows[j]["DueAmount"]));
                        }
                        l_StringBuilder.Append("<td colspan='5'>Total</td><td><b>" + total.ToString() + "</b></td></tr></table>");

                    }
                }
                #endregion construction plan
            }
            else if (PlanType == 3)
            {
                #region NormalPlan
                Hashtable ht = new Hashtable();
                ht.Add("FlatId", Session["FlatId"]);
                DataTable dt = new DataTable();
                dt.Columns.Add("DueDate", typeof(string));
                dt.Columns.Add("Event", typeof(string));
                dt.Columns.Add("AmountPer", typeof(decimal));
                dt.Columns.Add("Amount", typeof(decimal));

                l_StringBuilder.Append("<table class='table table-bordered table-striped particular_tbl'>");
                //l_StringBuilder.Append("<thead><tr><th>Due Date</th><th>Event</th><th>%</th><th>Amount</th></tr></thead><tbody>");
                l_StringBuilder.Append("<thead><tr><th>Installment No.</th><th>Event</th><th>Due Date</th><th style='width:60px'> BSP %</th><th style='width:105px'> PLC % (If Any)</th><th>Amount</th></tr></thead><tbody>");
                double total = Convert.ToDouble(saleprice);
                double Gtotal = 0;
                decimal bookingAmount = 500000;
                DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
                dtinfo.DateSeparator = "/";
                dtinfo.ShortDatePattern = "dd/MM/yyyy";
                DateTime DueDate = Convert.ToDateTime(saledate, dtinfo);
                for (int i = 0; i < ddlInstallment; i++)
                {
                    DataRow row = dt.NewRow();
                    row["DueDate"] = DueDate.AddMonths(i * ddlInterval).ToString("dd/MM/yyyy");
                    string InstallmentNo = "";
                    if (i == 0)
                    {
                        row["Event"] = "Booking Amount"; InstallmentNo = "Booking Amount";
                    }
                    else if (i == ddlInstallment - 1)
                    {
                        row["Event"] = "Possession Amount"; InstallmentNo = "Possession Amount";
                    }
                    else { row["Event"] = i; InstallmentNo = i.ToString(); }

                    if (i == 0)
                        row["Amount"] = bookingAmount;
                    else
                    {
                        row["Amount"] = (Convert.ToDecimal(saleprice) - bookingAmount) / (ddlInstallment - 1);
                    }
                    if (ddlInstallment == 1) // DevideByZero
                        row["AmountPer"] = 100;
                    else if (i == 0)
                        row["AmountPer"] = Convert.ToDecimal(saleprice) / bookingAmount;
                    else
                        row["AmountPer"] = (100 - (Convert.ToDecimal(saleprice) / bookingAmount)) / (ddlInstallment - 1);
                    // row["AmountPer"] = Convert.ToDecimal(saleprice) / ((Convert.ToDecimal(saleprice) - bookingAmount) / (ddlInstallment - 1));
                    dt.Rows.Add(row);
                    l_StringBuilder.Append("<tr><td>" + InstallmentNo + "</td><td>" + InstallmentNo + "</td><td><input type='text'  name='txtDueDate' value='" + row["DueDate"] + "' ></td><td  style='width:60px'><input id='" + i + "_" + row["AmountPer"] + "'  type='text'  style='width:60px' class='bsppercentage' name='txtBsp' onblur='calculatebsp(this.value,this.id)' value='" + row["AmountPer"] + "' ></td><td><input id='" + i + "' type='text'  readonly = 'readonly'  style='width:105px' class='plcpercentage' name='txtxPLC'  value='' ></td><td class='dueamount' id='bspdueamount" + i + "'>" + row["Amount"] + "</td><td id='bspbaseamount" + i + "' style='display:none'>" + row["Amount"] + "</td><tr>");

                    Gtotal = Gtotal + double.Parse(Convert.ToString(row["Amount"]));
                }
                Session["table"] = dt;
                l_StringBuilder.Append("<td colspan='3'>Total</td><td><b>" + total.ToString() + "</b></td></tr></tbody>");

                #endregion
            }
            else if (PlanType == 4 || PlanType == 5)
            {
                Session["table"] = null;
                #region Combo Plan
                ComboPlan(saledate, saleprice, ddlInstallment, ddlInterval, PlanType, plcPrice);
                //  ComboEvent();
                DataTable tb = new DataTable();
                if (Session["table"] != null)
                {
                    tb = Session["table"] as DataTable;
                    if (tb.Rows.Count > 0)
                    {
                        l_StringBuilder.Append("<table class='table table-bordered table-striped particular_tbl'>");
                        l_StringBuilder.Append("<thead><tr><th>Installment No.</th><th>Event</th><th>Due Date</th><th style='width:60px'> BSP %</th><th style='width:105px'> PLC % (If Any)</th><th>Amount</th></tr></thead><tbody>");

                        double total = 0;
                        int k = 0;
                        for (int j = 0; j < tb.Rows.Count; j++)
                        {
                            k++;
                            if (k >= 2)
                            {
                                tb.Rows[j]["DueDate"] = "";
                            }
                            if (j >= 0 && j < 3)
                            {
                                l_StringBuilder.Append("<tr><td>" + tb.Rows[j]["InstallmentID"] + "</td><td>" + GetComboEvents(Convert.ToString(tb.Rows[j]["InstallmentNo"]), Convert.ToString(tb.Rows[j]["InstallmentID"])) + "</td><td><input type='text'  name='txtDueDate' value='" + tb.Rows[j]["DueDate"] + "' ></td><td  style='width:60px'><input id='" + k + "_" + tb.Rows[j]["BSP"] + "'  type='text'  style='width:60px' class='bsppercentage' name='txtBsp' onblur='calculatebsp(this.value,this.id)' value='" + tb.Rows[j]["BSP"] + "' ></td><td><input id='" + k + "' type='text'  readonly = 'readonly'  style='width:105px' class='plcpercentage' name='txtxPLC'  value='" + tb.Rows[j]["PLC"] + "' ></td><td class='dueamount' id='bspdueamount" + k + "'>" + tb.Rows[j]["DueAmount"] + "</td><td id='bspbaseamount" + k + "' style='display:none'>" + tb.Rows[j]["DueAmount"] + "</td><tr>");
                            }
                            else
                            {
                                l_StringBuilder.Append("<tr><td>" + tb.Rows[j]["InstallmentID"] + "</td><td>" + GetComboEvents(Convert.ToString(tb.Rows[j]["InstallmentNo"]), Convert.ToString(tb.Rows[j]["InstallmentID"])) + "</td><td><input type='text' name='txtDueDate' value='" + tb.Rows[j]["DueDate"] + "' ></td><td  style='width:60px'><input id='" + k + "_" + tb.Rows[j]["BSP"] + "' type='text'  style='width:60px' class='bsppercentage' name='txtBsp' onblur='calculatebsp(this.value,this.id)' value='" + tb.Rows[j]["BSP"] + "' ></td><td><input id='" + k + "' type='text'  style='width:105px' class='plcpercentage' name='txtxPLC'  value='" + tb.Rows[j]["PLC"] + "' ></td><td class='dueamount' id='bspdueamount" + k + "'>" + tb.Rows[j]["DueAmount"] + "</td><td id='bspbaseamount" + k + "' style='display:none'>" + tb.Rows[j]["DueAmount"] + "</td><tr>");
                            }
                            total += double.Parse(Convert.ToString(tb.Rows[j]["DueAmount"]));
                        }
                        l_StringBuilder.Append("<td></td><td></td><td></td><td></td>Total<td></td><td><b id='bsptotal'>" + total.ToString() + "</b></td></tr></tbody></table>");
                    }
                }
                #endregion construction plan
            }
            return l_StringBuilder.ToString();
        }
        protected void ConstructionPlan(string saledate, string saleprice, int ddlInstallment, int ddlInterval, int PlanType, string plcPrice, string PlanTypeID)
        {
            Session["table"] = null;
            setTable();
            DateTime DueDate;
            decimal DueAmount;
            decimal TotalAmount;
            decimal BalanceAmount;
            decimal BookingAmt;
            decimal TotalPLC = !string.IsNullOrWhiteSpace(plcPrice) ? Convert.ToDecimal(plcPrice) : 0;
            //------------------
            Session["TableEvent"] = "";
            Hashtable ht = new Hashtable();
            if (Session["TowerID"] != null)
            {
                ht.Add("TowerID", Convert.ToInt32(Session["TowerID"].ToString()));
            }
            dtEvent = obj.GetDataTableFromProcedure("ViewEventByTowerID", ht);
            ddlInstallment = dtEvent.Rows.Count;
            if (dtEvent.Rows.Count > 0)
            {
                Session["TableEvent"] = dtEvent;
            }
            //-------------------
            DueDate = Convert.ToDateTime(obj.Text_IndianDateFormat(saledate));
            DueAmount = Math.Round(Convert.ToDecimal(saleprice), 0);

            TotalAmount = DueAmount;
            BalanceAmount = DueAmount;

            int j;
            int c = Convert.ToInt32(ddlInstallment);
            DataTable dtPayment = obj.GetDataTable("select SaleID from tblSPaymentDetail where RecordStatus=0 and SaleID=" + Convert.ToInt32(Session["SaleID"].ToString()));
            DataTable dt = new DataTable();
            dt = obj.GetDataTable("select InstallmentID,InstallmentNo,FlatID,SaleID,convert(varchar(50),DueDate,103) as DueDate,ceiling(round(DueAmount,0)) as DueAmount,DueAmtInWords,ServiceTaxAmount,ceiling(round(TotalAmount,0)) as TotalAmount,TotalAmtInWords,PayStatus,RecordStatus,EventID from tblSInstallmentDetail,BSP where RecordStatus=0 and SaleID=" + Convert.ToInt32(Session["SaleID"].ToString()) + " Order by InstallmentID asc");
            if (dt.Rows.Count == 0)
            {
                float cas2 = 20.00f, cas3 = 10.00f;
                decimal tamt = 0;
                BookingAmt = 500000;
                float cas1 = (float)BookingAmt * 100 / (float)TotalAmount;
                cas3 = (100 - cas1) / 9;
                for (int i = 1; i <= ddlInstallment; i++)
                {
                    switch (i)
                    {
                        case 1:
                            DueAmount = BookingAmt;
                            addRow("Booking Amount ", Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, cas1); tamt = tamt + DueAmount;
                            break;
                        case 2:
                            DueAmount = Convert.ToDecimal((float)TotalAmount * cas3 / 100f);
                            addRow((i - 1).ToString(), Convert.ToInt32(Session["SaleID"]), DueAmount, cas3); tamt = tamt + DueAmount;
                            break;
                        case 3:
                            DueAmount = Convert.ToDecimal(((float)TotalAmount * cas3 / 100));
                            addRow((i - 1).ToString(), Convert.ToInt32(Session["SaleID"]), DueAmount, cas3); tamt = tamt + DueAmount;
                            break;
                        case 4:
                            DueAmount = Convert.ToDecimal(((float)TotalAmount * cas3 / 100));
                            addRow((i - 1).ToString(), Convert.ToInt32(Session["SaleID"]), DueAmount, cas3); tamt = tamt + DueAmount;
                            break;
                        case 5:
                            DueAmount = Convert.ToDecimal(((float)TotalAmount * cas3 / 100));
                            addRow((i - 1).ToString(), Convert.ToInt32(Session["SaleID"]), DueAmount, cas3); tamt = tamt + DueAmount;
                            break;
                        case 6:
                            DueAmount = Convert.ToDecimal(((float)TotalAmount * cas3 / 100));
                            addRow((i - 1).ToString(), Convert.ToInt32(Session["SaleID"]), DueAmount, cas3); tamt = tamt + DueAmount;
                            break;
                        case 7:
                            DueAmount = Convert.ToDecimal(((float)TotalAmount * cas3 / 100));
                            addRow((i - 1).ToString(), Convert.ToInt32(Session["SaleID"]), DueAmount, cas3); tamt = tamt + DueAmount;
                            break;
                        case 8:
                            DueAmount = Convert.ToDecimal(((float)TotalAmount * cas3 / 100));
                            addRow((i - 1).ToString(), Convert.ToInt32(Session["SaleID"]), DueAmount, cas3); tamt = tamt + DueAmount;
                            break;
                        case 9:
                            DueAmount = Convert.ToDecimal(((float)TotalAmount * cas3 / 100));
                            addRow((i - 1).ToString(), Convert.ToInt32(Session["SaleID"]), DueAmount, cas3); tamt = tamt + DueAmount;
                            break;
                        case 10:
                            DueAmount = Convert.ToDecimal(((float)TotalAmount * cas3 / 100)); tamt = tamt + DueAmount;
                            decimal sp = Math.Round(Convert.ToDecimal(saleprice), 0);
                            if (sp < tamt) { DueAmount = DueAmount - (tamt - sp); }
                            addRow("Possession Amount", Convert.ToInt32(Session["SaleID"]), DueAmount, cas3);
                            break;
                    }
                    c = c + 1;
                }
            }
            else
            {
                Session["table"] = dt;
            }
        }
        protected void ComboPlan(string saledate, string saleprice, int ddlInstallment, int ddlInterval, int PlanType, string plcTotal)
        {
            Session["table"] = null;
            setTable();
            DateTime DueDate;
            decimal DueAmount;
            decimal TotalAmount;
            decimal BalanceAmount;
            decimal BookingAmt;
            decimal TotalPLC = !String.IsNullOrWhiteSpace(plcTotal) ? Convert.ToDecimal(plcTotal) : 0;
            Session["TableEvent"] = "";
            DueDate = Convert.ToDateTime(obj.Text_IndianDateFormat(saledate));
            DueAmount = Math.Round(Convert.ToDecimal(saleprice), 0);
            TotalAmount = DueAmount;
            BalanceAmount = DueAmount;
            int j;
            DataTable dtPayment = obj.GetDataTable("select SaleID from tblSPaymentDetail where RecordStatus=0 and SaleID=" + Convert.ToInt32(Session["SaleID"].ToString()));
            DataTable dt = new DataTable();
            dt = obj.GetDataTable("select InstallmentID,InstallmentNo,FlatID,SaleID,convert(varchar(50),DueDate,103) as DueDate,ceiling(round(DueAmount,0)) as DueAmount,DueAmtInWords,ServiceTaxAmount,ceiling(round(TotalAmount,0)) as TotalAmount,TotalAmtInWords,PayStatus,RecordStatus,EventID from tblSInstallmentDetail where RecordStatus=0 and SaleID=" + Convert.ToInt32(Session["SaleID"].ToString()) + " Order by InstallmentID asc");
            Hashtable ht1 = new Hashtable();
            ht1.Add("FlatId", Convert.ToInt32(Session["FlatId"].ToString()));
            dtEvent = obj.GetDataTableFromProcedure("GetEventsByBlockComboPlan", ht1);
            if (dtEvent.Rows.Count > 0)
            {
                Session["TableEvent"] = dtEvent;
            }
            if (dtEvent.Rows.Count > 0)
            {
                float cas1 = 0.00f, cas2 = 20.00f, cas3 = 10.00f;
                if (PlanType == 5)
                {
                    cas1 = 50;
                    cas2 = 50f / 9f;
                    cas3 = cas2;
                    BookingAmt = TotalAmount / 2;
                }
                else
                {
                    cas1 = 500000 * 100 / (float)TotalAmount;
                    cas2 = (100 - cas1) / 9;
                    cas3 = cas2;
                    BookingAmt = 500000;
                }
                addRowCombo("Booking Amount", "Booking Amount", Convert.ToInt32(Session["SaleID"]), BookingAmt, DueDate, "0", cas1, 0);
                for (Int32 i = 0; i < dtEvent.Rows.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            DueAmount = Math.Round((Convert.ToDecimal((float)TotalAmount * cas2 / 100)), 2);
                            // DueAmount = DueAmount - BookingAmt;
                            addRowCombo(Convert.ToString(dtEvent.Rows[i]["EventOrder"]), Convert.ToString(dtEvent.Rows[i]["EventName"]), Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, Convert.ToString(dtEvent.Rows[i]["EventOrder"]), cas2);
                            break;
                        case 1:
                            // DueAmount = Math.Round((TotalAmount * Convert.ToInt32(dtEvent.Rows[i]["AmtPer"]) / 100), 0) - Math.Round((TotalAmount * 20 / 100), 0);
                            DueAmount = Math.Round((Convert.ToDecimal((float)TotalAmount * cas3 / 100)), 2);
                            addRowCombo(Convert.ToString(dtEvent.Rows[i]["EventOrder"]), Convert.ToString(dtEvent.Rows[i]["EventName"]), Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, Convert.ToString(dtEvent.Rows[i]["EventOrder"]), cas3);
                            break;
                        case 2:
                            DueAmount = Math.Round((Convert.ToDecimal((float)TotalAmount * cas3 / 100)), 2);
                            addRowCombo(Convert.ToString(dtEvent.Rows[i]["EventOrder"]), Convert.ToString(dtEvent.Rows[i]["EventName"]), Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, Convert.ToString(dtEvent.Rows[i]["EventOrder"]), cas3);
                            break;
                        case 3:
                            DueAmount = Math.Round((Convert.ToDecimal((float)TotalAmount * cas3 / 100)), 2);
                            addRowCombo(Convert.ToString(dtEvent.Rows[i]["EventOrder"]), Convert.ToString(dtEvent.Rows[i]["EventName"]), Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, Convert.ToString(dtEvent.Rows[i]["EventOrder"]), cas3);
                            break;
                        case 4:
                            DueAmount = Math.Round((Convert.ToDecimal((float)TotalAmount * cas3 / 100)), 2);
                            if (TotalPLC > 0)
                            {
                                DueAmount = DueAmount + TotalPLC;
                                addRowCombo(Convert.ToString(dtEvent.Rows[i]["EventOrder"]), Convert.ToString(dtEvent.Rows[i]["EventName"]), Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, Convert.ToString(dtEvent.Rows[i]["EventOrder"]), cas3, 100);
                            }
                            else
                            {
                                addRowCombo(Convert.ToString(dtEvent.Rows[i]["EventOrder"]), Convert.ToString(dtEvent.Rows[i]["EventName"]), Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, Convert.ToString(dtEvent.Rows[i]["EventOrder"]), cas3);
                            }
                            break;
                        case 5:
                            DueAmount = Math.Round((Convert.ToDecimal((float)TotalAmount * cas3 / 100)), 2);
                            addRowCombo(Convert.ToString(dtEvent.Rows[i]["EventOrder"]), Convert.ToString(dtEvent.Rows[i]["EventName"]), Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, Convert.ToString(dtEvent.Rows[i]["EventOrder"]), cas3);
                            break;
                        case 6:
                            DueAmount = Math.Round((Convert.ToDecimal((float)TotalAmount * cas3 / 100)), 2);
                            addRowCombo(Convert.ToString(dtEvent.Rows[i]["EventOrder"]), Convert.ToString(dtEvent.Rows[i]["EventName"]), Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, Convert.ToString(dtEvent.Rows[i]["EventOrder"]), cas3);
                            break;
                        case 7:
                            DueAmount = Math.Round((Convert.ToDecimal((float)TotalAmount * cas3 / 100)), 2);
                            addRowCombo(Convert.ToString(dtEvent.Rows[i]["EventOrder"]), Convert.ToString(dtEvent.Rows[i]["EventName"]), Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, Convert.ToString(dtEvent.Rows[i]["EventOrder"]), cas3);
                            break;
                        case 8:
                            DueAmount = Math.Round((Convert.ToDecimal((float)TotalAmount * cas3 / 100)), 2);
                            addRowCombo(Convert.ToString(dtEvent.Rows[i]["EventOrder"]), Convert.ToString(dtEvent.Rows[i]["EventName"]), Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate, Convert.ToString(dtEvent.Rows[i]["EventOrder"]), cas3);
                            break;
                    }
                }
            }
        }
        protected void NormalDownPaymentPlan(string saledate, string saleprice, int ddlInstallment, int ddlInterval, int PlanType, string plcTotal)
        {
            Session["table"] = null;
            setTable();
            DateTime DueDate;
            decimal DueAmount;
            decimal TotalAmount;
            decimal BalanceAmount;
            decimal BookingAmt;
            decimal TotalPLC = !String.IsNullOrWhiteSpace(plcTotal) ? Convert.ToDecimal(plcTotal) : 0;
            Session["TableEvent"] = "";
            DueDate = Convert.ToDateTime(obj.Text_IndianDateFormat(saledate));
            DueAmount = Math.Round(Convert.ToDecimal(saleprice), 0);
            TotalAmount = DueAmount;
            BalanceAmount = DueAmount;
            int j;
            BookingAmt = 500000;
            DataTable dtPayment = obj.GetDataTable("select SaleID from tblSPaymentDetail where RecordStatus=0 and SaleID=" + Convert.ToInt32(Session["SaleID"].ToString()));
            DataTable dt = new DataTable();
            dt = obj.GetDataTable("select InstallmentID,InstallmentNo,FlatID,SaleID,convert(varchar(50),DueDate,103) as DueDate,ceiling(round(DueAmount,0)) as DueAmount,DueAmtInWords,ServiceTaxAmount,ceiling(round(TotalAmount,0)) as TotalAmount,TotalAmtInWords,PayStatus,RecordStatus,EventID from tblSInstallmentDetail where RecordStatus=0 and SaleID=" + Convert.ToInt32(Session["SaleID"].ToString()) + " Order by InstallmentID asc");
            addRowNormalDown("Booking Amount", "Booking Amount", Convert.ToInt32(Session["SaleID"]), BookingAmt, DueDate, 1.ToString());
            DueAmount = Math.Round(Math.Round((TotalAmount * 20 / 100), 2) - BookingAmt);
            addRowNormalDown("1", "At the time of Allotement", Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate.AddDays(15).Date, 2.ToString());
            DueAmount = Math.Round(Math.Round((TotalAmount * 90 / 100), 2) - Math.Round((TotalAmount * 20 / 100), 2));
            if (TotalPLC > 0)
            {
                DueAmount = Math.Round(DueAmount + Convert.ToDecimal(TotalPLC));
            }
            addRowNormalDown("2", "Within 60 days of booking (PLC Added If Any) ", Convert.ToInt32(Session["SaleID"]), DueAmount, DueDate.AddDays(60).Date, 3.ToString());
            DueAmount = Math.Round((TotalAmount * 10 / 100), 0);

            addRowNormalDown("3", "At the time of Possession", Convert.ToInt32(Session["SaleID"]), DueAmount, new DateTime().Date, 4.ToString());
        }
        private void addRowNormalDown(string InsId, string insNo, Int32 SaleID, decimal DueAmount, DateTime DueDate, string EventOrder, int BSPAmount = 0, Int64 PLCAmount = 0)
        {
            try
            {
                DataTable tb = Session["table"] as DataTable;
                DataRow dr = tb.NewRow();
                dr["InstallmentID"] = InsId;
                dr["InstallmentNo"] = insNo;
                dr["SaleID"] = SaleID;
                if (new DateTime().Date != DueDate)
                {
                    dr["DueDate"] = obj.DB_IndianDateFormat(DueDate.ToShortDateString());
                }
                dr["DueAmount"] = Math.Round(DueAmount, 0);
                dr["TotalAmount"] = Math.Round(DueAmount, 0);
                dr["PayStatus"] = 0;
                dr["EventID"] = Convert.ToInt32(EventOrder);

                if (BSPAmount != 0)
                {
                    dr["BSP"] = BSPAmount;
                }

                if (PLCAmount != 0)
                {
                    dr["PLC"] = PLCAmount;
                }

                tb.Rows.Add(dr);
                Session["table"] = tb;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }
        private void addRowCombo(string InsId, string insNo, Int32 SaleID, decimal DueAmount, DateTime DueDate, string EventOrder, float BSPAmount = 0, Int64 PLCAmount = 0)
        {
            try
            {
                DataTable tb = Session["table"] as DataTable;
                DataRow dr = tb.NewRow();
                dr["InstallmentID"] = InsId;
                dr["InstallmentNo"] = insNo;
                dr["SaleID"] = SaleID;
                dr["DueDate"] = obj.DB_IndianDateFormat(DueDate.ToShortDateString());
                dr["DueAmount"] = Math.Round(DueAmount, 2);
                dr["TotalAmount"] = Math.Round(DueAmount, 2);
                dr["PayStatus"] = 0;
                dr["EventID"] = Convert.ToInt32(EventOrder);

                if (BSPAmount != 0)
                {
                    dr["BSP"] = BSPAmount;
                }
                if (PLCAmount != 0)
                {
                    dr["PLC"] = PLCAmount;
                }

                tb.Rows.Add(dr);
                Session["table"] = tb;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }
        private void addRow(string insNo, Int32 SaleID, decimal DueAmount, float BSP = 0, decimal PLC = 0)
        {
            try
            {
                DataTable tb = Session["table"] as DataTable;
                DataRow dr = tb.NewRow();
                dr["InstallmentID"] = 0;
                dr["InstallmentNo"] = insNo;
                dr["SaleID"] = SaleID;
                dr["DueAmount"] = Math.Round(DueAmount, 2);
                dr["TotalAmount"] = Math.Round(DueAmount, 2);
                dr["PayStatus"] = 0;
                dr["EventID"] = 0;
                if (BSP > 0)
                {
                    dr["BSP"] = BSP;
                }
                if (PLC > 0)
                {
                    dr["PLC"] = PLC;
                }
                tb.Rows.Add(dr);
                Session["table"] = tb;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }
        private void addRow(string insNo, Int32 SaleID, decimal DueAmount, DateTime DueDate, float BSP = 0)
        {
            try
            {
                DataTable tb = Session["table"] as DataTable;
                DataRow dr = tb.NewRow();
                dr["InstallmentID"] = 0;
                dr["InstallmentNo"] = insNo;
                dr["SaleID"] = SaleID;
                dr["DueDate"] = obj.DB_IndianDateFormat(DueDate.ToShortDateString());
                dr["DueAmount"] = Math.Round(DueAmount, 2);
                dr["TotalAmount"] = Math.Round(DueAmount, 2);
                dr["PayStatus"] = 0;
                dr["EventID"] = 0;
                if (BSP > 0)
                {
                    dr["BSP"] = BSP;
                }

                tb.Rows.Add(dr);
                Session["table"] = tb;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }
        public void ComboEvent()
        {
            Session["TableEvent"] = "";
            Hashtable ht = new Hashtable();
            ht.Add("FlatId", Convert.ToInt32(Session["FlatId"].ToString()));
            dtEvent = obj.GetDataTableFromProcedure("GetEventsByBlockComboPlan", ht);
            if (dtEvent.Rows.Count > 0)
            {
                Session["TableEvent"] = dtEvent;
            }

        }
        public void ConstructionEvent()
        {
            Session["TableEvent"] = "";
            Hashtable ht = new Hashtable();
            if (Session["TowerID"] != null)
            {
                ht.Add("TowerID", Convert.ToInt32(Session["TowerID"].ToString()));
            }
            dtEvent = obj.GetDataTableFromProcedure("ViewEventByTowerID", ht);
            if (dtEvent.Rows.Count > 0)
            {
                Session["TableEvent"] = dtEvent;
            }

        }
        private StringBuilder GetComboEvents(string Event, string InstallmentNumber)
        {
            StringBuilder ddl = new StringBuilder();

            if (InstallmentNumber == "Booking Amount")
            {
                ddl.Append("Booking Amount");
                return ddl;
            }
            DataTable dt = new DataTable();
            if (Session["TableEvent"] != null)
            {
                ddl.Append("<select id='Events' class='dropdwn'>");
                dt = (DataTable)Session["TableEvent"];
                for (Int32 i = 0; i < dt.Rows.Count; i++)
                {

                    string EventName = Convert.ToString(dt.Rows[i]["EventName"]);
                    if (EventName != Event)
                        ddl.Append("<option value='" + Convert.ToString(dt.Rows[i]["EventId"]) + "'>" + EventName + "</option>");
                    else
                        ddl.Append("<option value='" + Convert.ToString(dt.Rows[i]["EventId"]) + "'>" + EventName + "</option>");
                }
                ddl.Append("</select>");
            }
            return ddl;
        }
        private StringBuilder GetEventDetails(string Event)
        {
            StringBuilder ddl = new StringBuilder();
            if (Event.Length > 5)
            {
                ddl.Append(Event);
                return ddl;
            }
            DataTable dt = new DataTable();
            if (Session["TableEvent"] != null)
            {
                ddl.Append("<select id='Events' class='dropdwn'>");
                dt = (DataTable)Session["TableEvent"];
                for (Int32 i = 0; i < dt.Rows.Count; i++)
                {
                    string EventName = Convert.ToString(dt.Rows[i]["EventName"]);
                    if (EventName != Event)
                        ddl.Append("<option value='" + Convert.ToString(dt.Rows[i]["EventId"]) + "'>" + EventName + "</option>");
                    else
                        ddl.Append("<option value='" + Convert.ToString(dt.Rows[i]["EventId"]) + "'>" + Event + "</option>");
                }
                ddl.Append("</select>");
            }
            return ddl;
        }
        private void setTable()
        {
            try
            {
                DataTable tb = new DataTable();
                tb.Columns.Add(new DataColumn("InstallmentID"));
                tb.Columns.Add(new DataColumn("InstallmentNo"));
                tb.Columns.Add(new DataColumn("SaleID"));
                tb.Columns.Add(new DataColumn("DueDate"));
                tb.Columns.Add(new DataColumn("DueAmount"));
                tb.Columns.Add(new DataColumn("TotalAmount"));
                tb.Columns.Add(new DataColumn("PayStatus"));
                tb.Columns.Add(new DataColumn("EventID"));
                tb.Columns.Add(new DataColumn("BSP"));
                tb.Columns.Add(new DataColumn("PLC"));
                Session["table"] = tb;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        public void SaveInstallments(string saledate, string saleprice, string salepriceword, int ddlInstallment, int ddlInterval, string EventName, string bspPercentage, string saleID, string flatID, string EvName, string Amount)
        {
            if (saleID != "")
                Session["SaleID"] = saleID;
            if (flatID != "")
                Session["FlatID"] = flatID;
            //By Dev: Update Sale Price and Sale details on installment generation click
            //Strat
            DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
            dtinfo.DateSeparator = "/";
            dtinfo.ShortDatePattern = "dd/MM/yyyy";
            DateTime dt2 = DateTime.Now;
            if (saledate != "") dt2 = Convert.ToDateTime(saledate, dtinfo);
            bool b = obj.ExecuteNonQuery("update tblssaleFlat set SaleRate='" + saleprice + "', SaleRateInWords='" + salepriceword + "', PaymentFor='Sale',SaleDate='" + dt2 + "', Status=1 where SaleID='" + Session["SaleID"].ToString() + "'");
            // End
            StringBuilder l_StringBuilder = new StringBuilder();
            string PlanType = "";
            if (PlanType == "")
                PlanType = Convert.ToString(Session["PlanType"]);
            if (PlanType != "1")
            {
                bspPercentage = bspPercentage.Replace("undefined", "");
                if (bspPercentage.Length > 0)
                {
                    bspPercentage = bspPercentage.Substring(0, bspPercentage.Length - 1);
                }
            }
            Amount = Amount.Replace("undefined,", "");
            Amount = Amount.TrimEnd(',');
            string[] amounts = Amount.Split(',');
            EvName = EvName.Replace("undefined,", "");
            EvName = EvName.TrimEnd(',');
            string[] EvNames = EvName.Split(',');

            Session["saleprice"] = saleprice;
            string[] bspPer = bspPercentage.Split(',');

            if (PlanType == "1")
            {
                DataTable tb = new DataTable();
                if (Session["table"] != null)
                {
                    tb = Session["table"] as DataTable;
                }
                if (tb.Rows.Count > 0)
                {
                    string[] EventIdd = EventName.Split(',');
                    int j = 0;
                    for (int a = 0; a < tb.Rows.Count; a++)
                    {
                        string InstallmentID = Convert.ToString(tb.Rows[a]["InstallmentID"]);
                        string InstallmentNo = Convert.ToString(tb.Rows[a]["InstallmentNo"]);
                        if (a > 0)
                        {
                            InstallmentNo = a.ToString();
                        }
                        string DueAmount = Convert.ToString(tb.Rows[a]["DueAmount"]);
                        string EventID = Convert.ToString(tb.Rows[a]["EventID"]);
                        string TotalAmount = Convert.ToString(tb.Rows[a]["TotalAmount"]);
                        if (DueAmount != "")
                        {
                            Hashtable ht = new Hashtable();
                            ht.Add("DueAmount", DueAmount);
                            ht.Add("DueAmtInWords", clsnew.rupees(Convert.ToInt64(DueAmount)));

                            ht.Add("TotalAmount", saleprice);
                            ht.Add("TotalAmtInWords", clsnew.rupees(Convert.ToInt64(saleprice)));
                            ht.Add("SaleID", Convert.ToInt32(Session["SaleID"].ToString()));
                            ht.Add("InstallmentNo", InstallmentNo);
                            ht.Add("EventName", InstallmentNo);

                            if (!String.IsNullOrEmpty(Convert.ToString(tb.Rows[a]["DueDate"])))
                            {
                                ht.Add("DueDate", obj.Text_IndianDateFormat(Convert.ToString(tb.Rows[a]["DueDate"])));
                            }
                            ht.Add("FlatID", Convert.ToInt32(Session["FlatID"].ToString()));
                            if (Session["TowerID"] != null)
                            {
                                ht.Add("TowerID", Convert.ToInt32(Session["TowerID"].ToString()));
                            }
                            if (InstallmentNo == "Booking Amount" || InstallmentNo == "Possession Amount")
                            {
                                ht.Add("Type", 0);
                            }
                            else
                            {
                                j++;
                                ht.Add("Type", 1);
                            }
                            ht.Add("Activity", "ADD");
                            if (obj.ExecuteProcedure("InsertInstallmentDetail", ht))
                            {
                            }
                        }
                    }
                }
            }
            else if (PlanType == "2")
            {
                DataTable tb = new DataTable();
                #region Construction Based Plan

                if (Session["table"] != null)
                {

                    tb = Session["table"] as DataTable;
                }
                if (tb.Rows.Count > 0)
                {
                    string[] EventIdd = EventName.Split(',');
                    int j = 0;
                    for (int a = 0; a < tb.Rows.Count; a++)
                    {
                        string InstallmentID = Convert.ToString(tb.Rows[a]["InstallmentID"]);
                        string InstallmentNo = Convert.ToString(tb.Rows[a]["InstallmentNo"]);
                        string DueAmount = Convert.ToString(tb.Rows[a]["DueAmount"]);
                        string EventID = Convert.ToString(tb.Rows[a]["EventID"]);
                        string TotalAmount = Convert.ToString(tb.Rows[a]["TotalAmount"]);
                        string DueDate = Convert.ToString(tb.Rows[a]["DueDate"]);
                        if (DueAmount != "")
                        {
                            Hashtable ht = new Hashtable();
                            ht.Add("DueAmount", amounts[a]);
                            ht.Add("DueAmtInWords", clsnew.rupees(Convert.ToDecimal(amounts[a])));

                            ht.Add("TotalAmount", amounts[a]);
                            ht.Add("TotalAmtInWords", clsnew.rupees(Convert.ToDecimal(amounts[a])));
                            ht.Add("SaleID", Convert.ToInt32(Session["SaleID"].ToString()));

                            if (DueDate != "")
                            {
                                ht.Add("DueDate", obj.Text_IndianDateFormat(Convert.ToString(tb.Rows[a]["DueDate"])));           /* Balkrishna  Add one Line By */
                            }

                            ht.Add("FlatID", Convert.ToInt32(Session["FlatID"].ToString()));
                            if (Session["TowerID"] != null)
                            {
                                ht.Add("TowerID", Convert.ToInt32(Session["TowerID"].ToString()));
                            }
                            if (InstallmentNo.Contains("Booking Amount") || InstallmentNo.Contains("Possession Amount"))
                            {
                                ht.Add("Type", 0);
                                ht.Add("InstallmentNo", InstallmentNo);
                                ht.Add("EventName", InstallmentNo);
                            }
                            else
                            {
                                if (!String.IsNullOrWhiteSpace(EventIdd[j]))
                                { ht.Add("EventID", Convert.ToInt32(EventIdd[j])); }
                                ht.Add("EventName", EvNames[j]);
                                ht.Add("InstallmentNo", EvNames[j]);
                                j++;
                                ht.Add("Type", 1);
                            }
                            ht.Add("Activity", "ADD");
                            ht.Add("Bsp", bspPer[a]);
                            if (obj.ExecuteProcedure("[InsertInstallmentDetail]", ht))
                            {
                            }
                        }
                    }
                }
                #endregion
            }
            else if (PlanType == "3")
            {
                #region Normal Mode
                DataTable dt = new DataTable();
                if (Session["table"] != null)
                    dt = (DataTable)Session["table"];
                //string EventName="";
                for (int a = 0; a < dt.Rows.Count; a++)
                {
                    string InstallmentID = Convert.ToString(a + 1);// Convert.ToString(dt.Rows[a]["InstallmentID"]);
                    EventName = Convert.ToString(dt.Rows[a]["Event"]);
                    string EventID = "0";
                    string InstallmentNo = Convert.ToString(dt.Rows[a]["Event"]);// Convert.ToString(dt.Rows[a]["InstallmentNo"]);
                    string DueDate = Convert.ToString(dt.Rows[a]["DueDate"]);
                    string DueAmount = Convert.ToString(dt.Rows[a]["Amount"]);
                    if (DueAmount != "")
                    {
                        Hashtable ht = new Hashtable();
                        if (DueDate != "")
                        {
                            ht.Add("DueDate", Convert.ToDateTime(DueDate, dtinfo));
                        }

                        ht.Add("DueAmount", amounts[a]);
                        ht.Add("DueAmtInWords", clsnew.rupees(Convert.ToInt64(amounts[a])));
                        // ht.Add("ServiceTaxAmount",ServiceTaxAmount.Text);
                        ht.Add("TotalAmount", amounts[a]);
                        ht.Add("TotalAmtInWords", clsnew.rupees(Convert.ToInt64(amounts[a])));
                        ht.Add("SaleID", Convert.ToInt32(Session["SaleID"].ToString()));
                        ht.Add("InstallmentNo", InstallmentNo);
                        ht.Add("FlatID", Convert.ToInt32(Session["FlatID"].ToString()));
                        ht.Add("Type", 0);
                        ht.Add("UserID", Convert.ToInt32("0"));
                        ht.Add("Activity", "Add");
                        ht.Add("EventName", EventName);
                        ht.Add("EventID", EventID);
                        ht.Add("FloorNo", 0);
                        ht.Add("TowerID", 0);
                        ht.Add("BID", 0);
                        ht.Add("BSP", bspPer[a]);
                        if (obj.ExecuteProcedure("InsertInstallmentDetailCLP", ht))
                        {
                        }
                    }
                }
                #endregion
            }
            else if (PlanType == "4" || PlanType == "5")
            {
                DataTable tb = new DataTable();
                #region Combo Plan

                if (Session["table"] != null)
                {
                    tb = Session["table"] as DataTable;
                }
                if (tb.Rows.Count > 0)
                {
                    string[] EventIdd = EventName.Split(',');
                    int j = 0;
                    for (int a = 0; a < tb.Rows.Count; a++)
                    {
                        string InstallmentID = Convert.ToString(tb.Rows[a]["InstallmentID"]);
                        string InstallmentNo = Convert.ToString(tb.Rows[a]["InstallmentNo"]);
                        string DueAmount = Convert.ToString(tb.Rows[a]["DueAmount"]);
                        string EventID = Convert.ToString(tb.Rows[a]["EventID"]);
                        string Eventorder = Convert.ToString(tb.Rows[a]["EventID"]);
                        string DueDate = "";
                        string TotalAmount = Convert.ToString(tb.Rows[a]["TotalAmount"]);
                        if (DueAmount != "")
                        {
                            Hashtable ht = new Hashtable();
                            if (InstallmentNo.Contains("Booking Amount"))
                            {
                                ht.Add("DueDate", obj.Text_IndianDateFormat(saledate));
                            }
                            if (DueDate != "")
                            {
                                ht.Add("DueDate", obj.Text_IndianDateFormat(DueDate));
                            }
                            ht.Add("DueAmount", amounts[a]);
                            ht.Add("DueAmtInWords", clsnew.rupees(Convert.ToDecimal(amounts[a])));
                            ht.Add("TotalAmount", amounts[a]);
                            ht.Add("TotalAmtInWords", clsnew.rupees(Convert.ToDecimal(amounts[a])));
                            ht.Add("SaleID", Convert.ToInt32(Session["SaleID"].ToString()));

                            ht.Add("FlatID", Convert.ToInt32(Session["FlatID"].ToString()));
                            if (Session["TowerID"] != null)
                            {
                                ht.Add("TowerID", Convert.ToInt32(Session["TowerID"].ToString()));
                            }
                            ht.Add("saleprice", saleprice);
                            ht.Add("InstallmentOrder", Eventorder);

                            if (InstallmentNo == "Booking Amount" || InstallmentNo == "Possession Amount")
                            {
                                ht.Add("Type", 0);
                                ht.Add("EventID", 0);
                                ht.Add("EventName", InstallmentNo);
                                ht.Add("InstallmentNo", InstallmentNo);
                            }
                            else
                            {
                                if (!String.IsNullOrWhiteSpace(EventIdd[j]))
                                { ht.Add("EventID", Convert.ToInt32(EventIdd[j])); }
                                ht.Add("EventName", EvNames[j]);
                                ht.Add("InstallmentNo", EvNames[j]);
                                j++;

                                ht.Add("Type", 1);
                            }
                            ht.Add("Activity", "ADD");
                            if (bspPer[a] != null && !String.IsNullOrWhiteSpace(bspPer[a]))
                            {
                                ht.Add("Bsp", bspPer[a]);
                            }
                            if (obj.ExecuteProcedure("InsertInstallmentDetailCombo", ht))
                            {
                            }
                        }
                    }
                }
                #endregion
            }
        }

        public string RefundPropertySave(string FlatID, string SaleID,string  Amount, string  Date,string  PayMode,string  ChequeNo,string ChequeDate,string BankName,string Branch,string Remarks,string FlatName)
        {
            using(dbSBPEntities2 context=new dbSBPEntities2())
            {
                int sid=Convert.ToInt32(SaleID);
                DateTime dt = new DateTime();
                DateTime cdt;
                DateTimeFormatInfo dtinfo=new DateTimeFormatInfo();
                dtinfo.DateSeparator="/";
                dtinfo.ShortDatePattern="dd/MM/yyyy";
                if(Date!=null && Date!="")
                    dt=Convert.ToDateTime(Date,dtinfo);
                int j=0;
                if (ChequeDate != null && ChequeDate != ""){
                    cdt = Convert.ToDateTime(ChequeDate, dtinfo);
                    j=1;
                }
                    
                else cdt = new DateTime();
                RefundProperty pro = new RefundProperty();
                pro.SaleID = sid;
                pro.RefundDate = dt;
                pro.PaymentMode = PayMode;
                pro.RefundAmount = Convert.ToDecimal(Amount);
                pro.BankName = BankName;
                pro.BranchName = Branch;
                pro.FlatName = FlatName;
                if(j==0)
                pro.ChequeDate = null;
                else
                    pro.ChequeDate = cdt;

                pro.ChequeNo = ChequeNo;

                context.RefundProperties.Add(pro);
                int i= context.SaveChanges();
                if (i > 0)
                {
                    int fid=Convert.ToInt32(FlatID);
                    // Update status =0;
                    var flat = context.tblSFlats.Where(f => f.FlatID == fid).FirstOrDefault();
                    flat.Status = 0;
                    context.tblSFlats.Add(flat);
                    i = context.SaveChanges();

                    // Update Status=0;
                    var sale = context.tblSSaleFlats.Where(s => s.SaleID == sid).FirstOrDefault();
                    sale.Status = 0;
                    context.tblSSaleFlats.Add(sale);
                    i= context.SaveChanges();
                    if (i > 0)
                    {
                        return "Yes";
                    }
                    else
                    return "No";
                }
                else
                    return "No";
            }
        }
        public string CheckRefundProperty(string SaleID)
        {
            try
            {

                dbSBPEntities2 context = new dbSBPEntities2();
                int sid = Convert.ToInt32(SaleID);
                var model = context.RefundProperties.Where(p => p.SaleID == sid).FirstOrDefault().SaleID;
                if (SaleID == null) return "No";
                else return "Yes";
            }
            catch(Exception ex)
            {
                return "No";
            }
        }
        #endregion
    }
}