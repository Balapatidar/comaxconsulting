﻿using AutoMapper;
using Comax.Web.Areas.Customer.Models;
using Comax.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Comax.Web.Areas.Customer.Controllers
{
    public class BrokerController : Controller
    {
        // GET: Customer/Broker  View Broker Payments
        [Authorize]
        public ActionResult AddNewBroker()
        {
            return View();
        }
        [Authorize]
        public ActionResult EditBroker(int id)
        {
            ViewBag.ID = id;
            return View();
        }
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult AttachBroker(int Id)
        {
            ViewBag.ID = Id;
            return View();
        }
        [Authorize]
        public ActionResult PayBroker(int Id)
        {
            ViewBag.ID = Id;
            return View();
        }
        [Authorize]
        public ActionResult ApprovePayment(int Id)
        {
            ViewBag.ID = Id;
            return View();
        }
        [Authorize]
        public ActionResult PaymentLadger(int Id)
        {
            ViewBag.ID = Id;
            return View();
        }

        #region Broker_Services

        public string SaveBroker(BrokerModel broker)
        {
            using (dbSBPEntities2 context = new dbSBPEntities2())
            {
                Mapper.CreateMap<BrokerModel, tblBroker>();
                var model = Mapper.Map<BrokerModel, tblBroker>(broker);
                context.tblBrokers.Add(model);
                model.RecordStatus = 1;
                model.CreateDate = DateTime.Now;
                int i = context.SaveChanges();
                if (i > 0)
                {
                    return "Yes";
                }
                else
                    return "No";
            }
        }

        public string SearchBroker(string search, string query)
        {
            if (search == "? undefined:undefined ?" || search == "All") search = "All";
            if (query == "? undefined:undefined ?" || search == "All") query = "";

            dbSBPEntities2 context = new dbSBPEntities2();
            List<BrokerModel> md = new List<BrokerModel>();
            DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
            dtinfo.DateSeparator = "/";
            dtinfo.ShortDatePattern = "dd/MM/yyyy";
            if (search == "All")
            {
                var model = context.tblBrokers.AsEnumerable();
                foreach (var m in model)
                {
                    Mapper.CreateMap<tblBroker, BrokerModel>();
                    var mdl = Mapper.Map<tblBroker, BrokerModel>(m);
                    if (m.CreateDate != null)
                        mdl.CreateDateSt = m.CreateDate.Value.ToString("dd/MM/yyyy");
                    md.Add(mdl);
                }
            }
            else if (search == "Name")
            {
                var model = context.tblBrokers.Where(br => br.BrokerName.Contains(query)).AsEnumerable();
                foreach (var m in model)
                {
                    Mapper.CreateMap<tblBroker, BrokerModel>();
                    var mdl = Mapper.Map<tblBroker, BrokerModel>(m);
                    if (m.CreateDate != null)
                        mdl.CreateDateSt = m.CreateDate.Value.ToString("dd/MM/yyyy");
                    md.Add(mdl);
                }
            }
            else if (search == "MobileNo")
            {
                var model = context.tblBrokers.Where(br => br.MobileNo.Contains(query)).AsEnumerable();
                foreach (var m in model)
                {
                    Mapper.CreateMap<tblBroker, BrokerModel>();
                    var mdl = Mapper.Map<tblBroker, BrokerModel>(m);
                    if (m.CreateDate != null)
                        mdl.CreateDateSt = m.CreateDate.Value.ToString("dd/MM/yyyy");
                    md.Add(mdl);
                }
            }
            else if (search == "PanNo")
            {
                var model = context.tblBrokers.Where(br => br.PanNo.Contains(query)).AsEnumerable();
                foreach (var m in model)
                {
                    Mapper.CreateMap<tblBroker, BrokerModel>();
                    var mdl = Mapper.Map<tblBroker, BrokerModel>(m);
                    if (m.CreateDate != null)
                        mdl.CreateDateSt = m.CreateDate.Value.ToString("dd/MM/yyyy");
                    md.Add(mdl);
                }
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(md);
        }

        public string GetAllBroker()
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            var model = context.tblBrokers.Where(br => br.RecordStatus == 1);
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }
        public string GetBrokerBySaleID(int saleid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            var md = (from bro in context.tblBrokers join bp in context.BrokerToProperties on bro.BrokerID equals bp.BrokerID where bp.SaleID == saleid select new { Broker = bro, BP = bp });
            List<BrokerToPropertyModel> model = new List<BrokerToPropertyModel>();
            foreach (var mdl in md)
            {
                string bdate = "";
                if (mdl.BP.Date != null)
                    bdate = mdl.BP.Date.Value.ToString("dd/MM/yyyy");
                model.Add(new BrokerToPropertyModel { Status = mdl.BP.Status, ApproveStatus = mdl.BP.ApproveStatus, BrokerAmount = mdl.BP.BrokerAmount, BrokerID = mdl.BP.BrokerID, BrokerName = mdl.Broker.BrokerName, BrokerToPropertyID = mdl.BP.BrokerToPropertyID, Createdate = mdl.BP.Createdate, Date = mdl.BP.Date, DateSt = bdate, FlatID = mdl.BP.FlatID, ModifyDate = mdl.BP.ModifyDate, PID = mdl.BP.PID, RecordStatus = mdl.BP.RecordStatus, Remarks = mdl.BP.Remarks, SaleID = mdl.BP.SaleID });
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }
        public string GetBrokerByBrokerToPropertyID(int pid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            var md = (from bro in context.tblBrokers join bp in context.BrokerToProperties on bro.BrokerID equals bp.BrokerID where bp.BrokerToPropertyID == pid select new { Broker = bro, BP = bp });
            List<BrokerToPropertyModel> model = new List<BrokerToPropertyModel>();
            foreach (var mdl in md)
            {
                string bdate = "";
                if (mdl.BP.Date != null)
                    bdate = mdl.BP.Date.Value.ToString("dd/MM/yyyy");
                model.Add(new BrokerToPropertyModel { ApproveStatus = mdl.BP.ApproveStatus, BrokerAmount = mdl.BP.BrokerAmount, BrokerID = mdl.BP.BrokerID, BrokerName = mdl.Broker.BrokerName, BrokerToPropertyID = mdl.BP.BrokerToPropertyID, Createdate = mdl.BP.Createdate, Date = mdl.BP.Date, DateSt = bdate, FlatID = mdl.BP.FlatID, ModifyDate = mdl.BP.ModifyDate, PID = mdl.BP.PID, RecordStatus = mdl.BP.RecordStatus, Remarks = mdl.BP.Remarks, SaleID = mdl.BP.SaleID });
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }
        public string GetBrokerByBrokerID(int bid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            var model = context.tblBrokers.Where(br => br.RecordStatus == 1 && br.BrokerID == bid).FirstOrDefault();
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }
        public string GetBrokersProperties(int brokerid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();

            if (brokerid == 0)
            {
                var md = (from bro in context.tblBrokers join bp in context.BrokerToProperties on bro.BrokerID equals bp.BrokerID join flat in context.tblSFlats on bp.FlatID equals flat.FlatID select new { Broker = bro, BP = bp, Pro = flat });
                List<BrokerToPropertyModel> model = new List<BrokerToPropertyModel>();
                foreach (var mdl in md)
                {
                    string bdate = "";
                    if (mdl.BP.Date != null)
                        bdate = mdl.BP.Date.Value.ToString("dd/MM/yyyy");
                    model.Add(new BrokerToPropertyModel { Status = mdl.BP.Status, ApproveStatus = mdl.BP.ApproveStatus, BrokerAmount = mdl.BP.BrokerAmount, BrokerID = mdl.BP.BrokerID, BrokerName = mdl.Broker.BrokerName, BrokerToPropertyID = mdl.BP.BrokerToPropertyID, Createdate = mdl.BP.Createdate, Date = mdl.BP.Date, DateSt = bdate, FlatID = mdl.BP.FlatID, ModifyDate = mdl.BP.ModifyDate, PID = mdl.BP.PID, RecordStatus = mdl.BP.RecordStatus, Remarks = mdl.BP.Remarks, SaleID = mdl.BP.SaleID, PropertyName = mdl.Pro.FlatName });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
            else
            {
                var md = (from bro in context.tblBrokers join bp in context.BrokerToProperties on bro.BrokerID equals bp.BrokerID join flat in context.tblSFlats on bp.FlatID equals flat.FlatID where bro.BrokerID == brokerid select new { Broker = bro, BP = bp, Pro = flat });
                List<BrokerToPropertyModel> model = new List<BrokerToPropertyModel>();
                foreach (var mdl in md)
                {
                    string bdate = "";
                    if (mdl.BP.Date != null)
                        bdate = mdl.BP.Date.Value.ToString("dd/MM/yyyy");
                    model.Add(new BrokerToPropertyModel { Status = mdl.BP.Status, ApproveStatus = mdl.BP.ApproveStatus, BrokerAmount = mdl.BP.BrokerAmount, BrokerID = mdl.BP.BrokerID, BrokerName = mdl.Broker.BrokerName, BrokerToPropertyID = mdl.BP.BrokerToPropertyID, Createdate = mdl.BP.Createdate, Date = mdl.BP.Date, DateSt = bdate, FlatID = mdl.BP.FlatID, ModifyDate = mdl.BP.ModifyDate, PID = mdl.BP.PID, RecordStatus = mdl.BP.RecordStatus, Remarks = mdl.BP.Remarks, SaleID = mdl.BP.SaleID, PropertyName = mdl.Pro.FlatName });
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }

        }
        public string GetBrokersPropertiesSearch(int brokerid, string Status)
        {
            List<BrokerToPropertyModel> model = new List<BrokerToPropertyModel>();
            dbSBPEntities2 context = new dbSBPEntities2();
            if (brokerid != 0)
            {
                if (Status == "All")
                {
                    var md = (from bro in context.tblBrokers join bp in context.BrokerToProperties on bro.BrokerID equals bp.BrokerID join flat in context.tblSFlats on bp.FlatID equals flat.FlatID where bro.BrokerID == brokerid select new { Broker = bro, BP = bp, Pro = flat });
                    foreach (var mdl in md)
                    {
                        string bdate = "";
                        if (mdl.BP.Date != null)
                            bdate = mdl.BP.Date.Value.ToString("dd/MM/yyyy");
                        model.Add(new BrokerToPropertyModel { Status = mdl.BP.Status, ApproveStatus = mdl.BP.ApproveStatus, BrokerAmount = mdl.BP.BrokerAmount, BrokerID = mdl.BP.BrokerID, BrokerName = mdl.Broker.BrokerName, BrokerToPropertyID = mdl.BP.BrokerToPropertyID, Createdate = mdl.BP.Createdate, Date = mdl.BP.Date, DateSt = bdate, FlatID = mdl.BP.FlatID, ModifyDate = mdl.BP.ModifyDate, PID = mdl.BP.PID, RecordStatus = mdl.BP.RecordStatus, Remarks = mdl.BP.Remarks, SaleID = mdl.BP.SaleID, PropertyName = mdl.Pro.FlatName });
                    }

                }
                else
                {
                    var md = (from bro in context.tblBrokers join bp in context.BrokerToProperties on bro.BrokerID equals bp.BrokerID join flat in context.tblSFlats on bp.FlatID equals flat.FlatID where bro.BrokerID == brokerid && bp.Status == Status select new { Broker = bro, BP = bp, Pro = flat });
                    foreach (var mdl in md)
                    {
                        string bdate = "";
                        if (mdl.BP.Date != null)
                            bdate = mdl.BP.Date.Value.ToString("dd/MM/yyyy");
                        model.Add(new BrokerToPropertyModel { Status = mdl.BP.Status, ApproveStatus = mdl.BP.ApproveStatus, BrokerAmount = mdl.BP.BrokerAmount, BrokerID = mdl.BP.BrokerID, BrokerName = mdl.Broker.BrokerName, BrokerToPropertyID = mdl.BP.BrokerToPropertyID, Createdate = mdl.BP.Createdate, Date = mdl.BP.Date, DateSt = bdate, FlatID = mdl.BP.FlatID, ModifyDate = mdl.BP.ModifyDate, PID = mdl.BP.PID, RecordStatus = mdl.BP.RecordStatus, Remarks = mdl.BP.Remarks, SaleID = mdl.BP.SaleID, PropertyName = mdl.Pro.FlatName });
                    }
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
            else
            {

                if (Status == "All")
                {
                    var md = (from bro in context.tblBrokers join bp in context.BrokerToProperties on bro.BrokerID equals bp.BrokerID join flat in context.tblSFlats on bp.FlatID equals flat.FlatID select new { Broker = bro, BP = bp, Pro = flat });
                    foreach (var mdl in md)
                    {
                        string bdate = "";
                        if (mdl.BP.Date != null)
                            bdate = mdl.BP.Date.Value.ToString("dd/MM/yyyy");
                        model.Add(new BrokerToPropertyModel { Status = mdl.BP.Status, ApproveStatus = mdl.BP.ApproveStatus, BrokerAmount = mdl.BP.BrokerAmount, BrokerID = mdl.BP.BrokerID, BrokerName = mdl.Broker.BrokerName, BrokerToPropertyID = mdl.BP.BrokerToPropertyID, Createdate = mdl.BP.Createdate, Date = mdl.BP.Date, DateSt = bdate, FlatID = mdl.BP.FlatID, ModifyDate = mdl.BP.ModifyDate, PID = mdl.BP.PID, RecordStatus = mdl.BP.RecordStatus, Remarks = mdl.BP.Remarks, SaleID = mdl.BP.SaleID, PropertyName = mdl.Pro.FlatName });
                    }

                }
                else
                {
                    var md = (from bro in context.tblBrokers join bp in context.BrokerToProperties on bro.BrokerID equals bp.BrokerID join flat in context.tblSFlats on bp.FlatID equals flat.FlatID where bp.Status == Status select new { Broker = bro, BP = bp, Pro = flat });
                    foreach (var mdl in md)
                    {
                        string bdate = "";
                        if (mdl.BP.Date != null)
                            bdate = mdl.BP.Date.Value.ToString("dd/MM/yyyy");
                        model.Add(new BrokerToPropertyModel { Status = mdl.BP.Status, ApproveStatus = mdl.BP.ApproveStatus, BrokerAmount = mdl.BP.BrokerAmount, BrokerID = mdl.BP.BrokerID, BrokerName = mdl.Broker.BrokerName, BrokerToPropertyID = mdl.BP.BrokerToPropertyID, Createdate = mdl.BP.Createdate, Date = mdl.BP.Date, DateSt = bdate, FlatID = mdl.BP.FlatID, ModifyDate = mdl.BP.ModifyDate, PID = mdl.BP.PID, RecordStatus = mdl.BP.RecordStatus, Remarks = mdl.BP.Remarks, SaleID = mdl.BP.SaleID, PropertyName = mdl.Pro.FlatName });
                    }
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
        }
        public string GetPaidAmountToBrokerByBrokerID(int brokerID)
        {
            if (brokerID != 0)
            {
                dbSBPEntities2 context = new dbSBPEntities2();
                var model = context.BrokerPayments.Where(br => br.BrokerID == brokerID).ToList();
                List<BrokerPaymentModel> bmodel = new List<BrokerPaymentModel>();
                foreach (var md in model)
                {
                    var pro = context.tblSFlats.Where(f => f.FlatID == md.FlatID).FirstOrDefault();

                    Mapper.CreateMap<BrokerPayment, BrokerPaymentModel>();
                    var mdl = Mapper.Map<BrokerPayment, BrokerPaymentModel>(md);
                    mdl.PropertyName = "";
                    if (md.PaidDate != null)
                        mdl.PaymentDateSt = md.PaidDate.Value.ToString("dd/MM/yyyy");
                    if (md.ChequeDate != null)
                        mdl.ChequeDateSt = md.ChequeDate.Value.ToString("dd/MM/yyyy");
                    bmodel.Add(mdl);
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(bmodel);
            }
            else
            {
                dbSBPEntities2 context = new dbSBPEntities2();
                var model = context.BrokerPayments.ToList();
                List<BrokerPaymentModel> bmodel = new List<BrokerPaymentModel>();
                foreach (var md in model)
                {
                    var pro = context.tblSFlats.Where(f => f.FlatID == md.FlatID).FirstOrDefault();

                    Mapper.CreateMap<BrokerPayment, BrokerPaymentModel>();
                    var mdl = Mapper.Map<BrokerPayment, BrokerPaymentModel>(md);
                    mdl.PropertyName = "";
                    if (md.PaidDate != null)
                        mdl.PaymentDateSt = md.PaidDate.Value.ToString("dd/MM/yyyy");
                    if (md.ChequeDate != null)
                        mdl.ChequeDateSt = md.ChequeDate.Value.ToString("dd/MM/yyyy");
                    bmodel.Add(mdl);
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(bmodel);
            }
        }
        public string GetPaidAmountToBrokerSearch(int brokerID, string Status)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            List<BrokerPaymentModel> bmodel = new List<BrokerPaymentModel>();

            if (Status == "All")
            {
                var model = context.BrokerPayments.Where(br => br.BrokerID == brokerID).ToList();
                foreach (var md in model)
                {
                    var pro = context.tblSFlats.Where(f => f.FlatID == md.FlatID).FirstOrDefault();

                    Mapper.CreateMap<BrokerPayment, BrokerPaymentModel>();
                    var mdl = Mapper.Map<BrokerPayment, BrokerPaymentModel>(md);
                    mdl.PropertyName = pro.FlatName;
                    if (md.PaidDate != null)
                        mdl.PaymentDateSt = md.PaidDate.Value.ToString("dd/MM/yyyy");
                    if (md.ChequeDate != null)
                        mdl.ChequeDateSt = md.ChequeDate.Value.ToString("dd/MM/yyyy");
                    bmodel.Add(mdl);
                }
            }
            else
            {
                var model = context.BrokerPayments.Where(br => br.BrokerID == brokerID && br.Status == Status).ToList();
                foreach (var md in model)
                {
                    var pro = context.tblSFlats.Where(f => f.FlatID == md.FlatID).FirstOrDefault();

                    Mapper.CreateMap<BrokerPayment, BrokerPaymentModel>();
                    var mdl = Mapper.Map<BrokerPayment, BrokerPaymentModel>(md);
                    mdl.PropertyName = pro.FlatName;
                    if (md.PaidDate != null)
                        mdl.PaymentDateSt = md.PaidDate.Value.ToString("dd/MM/yyyy");
                    if (md.ChequeDate != null)
                        mdl.ChequeDateSt = md.ChequeDate.Value.ToString("dd/MM/yyyy");
                    bmodel.Add(mdl);
                }
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(bmodel);
        }
        public string AttachBrokerToProperty(string brokerid, string amount, string date, string remarks, int PropertyID, int FlatID, string SaleID)
        {
            using (dbSBPEntities2 context = new dbSBPEntities2())
            {
                DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
                dtinfo.ShortDatePattern = "dd/MM/yyyy";
                dtinfo.DateSeparator = "/";
                int bid = Convert.ToInt32(brokerid);
                int sid = Convert.ToInt32(SaleID);
                BrokerToProperty bp = new BrokerToProperty();
                bp.BrokerID = bid;
                bp.ApproveStatus = 0;
                bp.BrokerAmount = Convert.ToDecimal(amount);
                bp.Createdate = DateTime.Now;
                bp.Date = Convert.ToDateTime(date, dtinfo);
                bp.SaleID = sid;
                bp.Remarks = remarks;
                bp.FlatID = FlatID;
                bp.PID = PropertyID;
                bp.Status = "Pending";

                context.BrokerToProperties.Add(bp);
                int i = context.SaveChanges();
                if (i > 0)
                    return "Yes";
                else
                    return "No";
            }
        }
        public string AttachBrokerToPropertyUpdate(int brokerToPropertyID, string brokerid, string amount, string date, string remarks, int PropertyID, int FlatID, string SaleID)
        {
            using (dbSBPEntities2 context = new dbSBPEntities2())
            {
                DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
                BrokerToProperty bp = context.BrokerToProperties.Where(b => b.BrokerToPropertyID == brokerToPropertyID).FirstOrDefault();

                dtinfo.ShortDatePattern = "dd/MM/yyyy";
                dtinfo.DateSeparator = "/";
                int bid = Convert.ToInt32(brokerid);
                int sid = Convert.ToInt32(SaleID);
                bp.BrokerID = bid;
                bp.ApproveStatus = 0;
                bp.BrokerAmount = Convert.ToDecimal(amount);
                bp.Createdate = DateTime.Now;
                bp.Date = Convert.ToDateTime(date, dtinfo);
                bp.SaleID = sid;
                bp.Remarks = remarks;
                bp.FlatID = FlatID;
                bp.PID = PropertyID;

                context.BrokerToProperties.Add(bp);
                context.Entry(bp).State = EntityState.Modified;
                int i = context.SaveChanges();
                if (i > 0)
                    return "Yes";
                else
                    return "No";
            }
        }
        public string DeleteBrokerToPropertyID(int pid)
        {
            using (dbSBPEntities2 context = new dbSBPEntities2())
            {
                DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
                BrokerToProperty bp = context.BrokerToProperties.Where(b => b.BrokerToPropertyID == pid).FirstOrDefault();

                context.BrokerToProperties.Add(bp);
                context.Entry(bp).State = EntityState.Deleted;
                int i = context.SaveChanges();
                if (i > 0)
                    return "Yes";
                else
                    return "No";
            }
        }
        public string PaymentBrokerToProperty(int FlatID, int SaleID, int BrokerID, string PaidDate, string AmountPiad, int PID, string PaymentMode, string ChequeNo, string ChequeDate, string BankName, string BranchName, string Remarks)
        {
            using (dbSBPEntities2 context = new dbSBPEntities2())
            {
                DateTimeFormatInfo dtinfo = new DateTimeFormatInfo();
                dtinfo.ShortDatePattern = "dd/MM/yyyy";
                dtinfo.DateSeparator = "/";
                BrokerPayment bp = new BrokerPayment();
                bp.FlatID = FlatID;
                bp.SaleID = SaleID;
                bp.BrokerID = BrokerID;
                bp.PaidDate = Convert.ToDateTime(PaidDate, dtinfo);
                bp.AmountPaid = Convert.ToDecimal(AmountPiad);
                bp.PID = PID;
                bp.PaymentMode = PaymentMode;
                bp.ChequeNo = ChequeNo;
                if (ChequeDate != "")
                    bp.ChequeDate = Convert.ToDateTime(ChequeDate, dtinfo);
                bp.BankName = BankName;
                bp.BankBranch = BranchName;
                bp.Remarks = Remarks;
                bp.CreateDate = DateTime.Now;
                bp.RecordStatus = 0;
                bp.Status = "Pending";

                context.BrokerPayments.Add(bp);
                int i = context.SaveChanges();
                if (i > 0)
                    return "Yes";
                else
                    return "No";
            }
        }


        public string UpdatePropertyStatus(int saleid, string status)
        {
            try
            {
                using (var ctx = new dbSBPEntities2())
                {
                    var stud = (from s in ctx.tblSSaleFlats
                                where s.SaleID == saleid
                                select s).FirstOrDefault();

                    stud.PropertyStatus = status;
                    ctx.SaveChanges();
                    return "Sucess";
                }



            }
            catch (Exception ex)
            {

                return "Fail";
            }
        }

        #endregion
    }
}