﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Comax.Web.Areas.Admin.Models
{
    public class PropertySizeModel
    {
        public int PropertySizeID { get; set; }
        public Nullable<int> PropertyTypeID { get; set; }
        public Nullable<int> Size { get; set; }
        public string Unit { get; set; }
    }
}