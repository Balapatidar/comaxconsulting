﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Comax.Web.Areas.Admin.Models
{
    public class PropertyTypeModel
    {
        public int PropertyTypeID { get; set; }
        public Nullable<int> PID { get; set; }
        public string PType { get; set; }
        public Nullable<int> Size { get; set; }
        public string Unit { get; set; }
        public Nullable<decimal> ServiceTax { get; set; }
    }
}