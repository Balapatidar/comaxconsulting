﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Comax.Web.Areas.Admin.Models
{
    public class UserModel
    {
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Property { get; set; }
    }
}