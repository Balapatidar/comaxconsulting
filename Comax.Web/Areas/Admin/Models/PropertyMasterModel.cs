﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Comax.Web.Areas.Admin.Models
{
    public class PropertyMasterModel
    {
        public int PID { get; set; }
        public string PName { get; set; }
        public Nullable<int> NofBlock { get; set; }
        public Nullable<int> NofTower { get; set; }
        public string PStatus { get; set; }
        public Nullable<int> NofFlat { get; set; }
        public Nullable<bool> IsBlock { get; set; }
        public Nullable<bool> IsTower { get; set; }
        public string CompanyName { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string ReceiptPrefix { get; set; }
        public Nullable<int> RecordStatus { get; set; }
        public string OfficeAddress { get; set; }
        public string District { get; set; }
        public string Village { get; set; }
        public string Tehsil { get; set; }
        public string Jurisdiction { get; set; }
        public string PossessionDate { get; set; }
        public string STReceiptNo { get; set; }
        public string execution { get; set; }
        public string smcreceiptno { get; set; }
        public string ifmcreceiptno { get; set; }
        public string transferreceiptno { get; set; }
        public string AuthoritySign { get; set; }
    }
}