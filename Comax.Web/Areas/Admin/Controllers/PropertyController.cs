﻿using AutoMapper;
using Comax.Web.Areas.Admin.Models;
using Comax.Web.Code;
using Comax.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Comax.Web.Areas.Admin.Controllers
{
    public class PropertyController : BaseController
    {
        DataFunctions obj = new DataFunctions();
        // GET: Admin/Property
        [MyAuthorize]
        public ActionResult Index()
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            var model = context.tblSProperties.OrderByDescending(o => o.PID);
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            string s=collection["PropertyID"];
           // string ss = collection["PropertyTypeID"];
            if(s!="? undefined:undefined ?")
            {
                int i=Convert.ToInt32(s);
                dbSBPEntities2 context = new dbSBPEntities2();
                var model = context.tblSProperties.Where(pro=>pro.PID==i).OrderByDescending(o => o.PID);
                return View(model);
            }
            else
            {
                dbSBPEntities2 context = new dbSBPEntities2();
                var model = context.tblSProperties.OrderByDescending(o => o.PID);
                return View(model);
            }
        }

        public string SearchProjectProperty(string pid)
        {
            string s = pid;
            // string ss = collection["PropertyTypeID"];
            if (s != "? undefined:undefined ?" && s!=null)
            {
                int i = Convert.ToInt32(s);
                dbSBPEntities2 context = new dbSBPEntities2();
                var model = context.tblSProperties.Where(pro => pro.PID == i).OrderByDescending(o => o.PID);
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
            else
            {
                dbSBPEntities2 context = new dbSBPEntities2();
                var model = context.tblSProperties.OrderByDescending(o => o.PID);
                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
        }

       // [Authorize(Roles = "Admin")]
        [MyAuthorize]
        public ActionResult AddProperty()
        {
            return View();
        }
        //[Authorize(Roles = "Admin")]
        [MyAuthorize]
        public ActionResult AddPropertyType()
        {
            return View();
        }
      //  [Authorize(Roles = "Admin")]
        [MyAuthorize]
        public ActionResult Edit(int id)
        {
            ViewBag.ID = id;
            dbSBPEntities2 context = new dbSBPEntities2();
            var v = context.tblSProperties.Where(p => p.PID == id).FirstOrDefault();
            return View(v);
        }
        [HttpPost]
        public ActionResult Edit(int id,tblSProperty model, FormCollection collection)
        {
            using(dbSBPEntities2 context=new dbSBPEntities2())
            {
                try
                {
                    context.Entry(model).State = EntityState.Modified;
                    int i = context.SaveChanges();
                    if(i==1)
                    {
                        Success = "Property Information updated successfully.";
                    }
                    else
                    {
                        Failure = "Please provide correct information.";
                    }
                }
                catch(Exception ex)
                {
                    Failure = "Please provide correct information.";
                    Helper h = new Helper();
                    h.LogException(ex);
                }
                ViewBag.ID = id;
                var vv = context.tblSProperties.Where(p => p.PID == id).FirstOrDefault();
                return View(vv);
            }
           
        }
        [MyAuthorize]
        public ActionResult Details(int id)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            var v= context.tblSProperties.Where(p => p.PID == id).FirstOrDefault();
            return View(v);
        }
        #region JSONServices
        public HttpResponseMessage AddNewProperty(PropertyMasterModel property)
        {
            using (dbSBPEntities2 context = new dbSBPEntities2())
            {
                Mapper.CreateMap<PropertyMasterModel, tblSProperty>();
                var pro = Mapper.Map<PropertyMasterModel, tblSProperty>(property);
                pro.IsBlock = true;
                pro.NofFlat = 0;
                pro.IsBlock = false;
                pro.IsTower = false;
                pro.STReceiptNo = "0";
                pro.transferreceiptno = "0";
                pro.smcreceiptno = "0";
                pro.ifmcreceiptno = "0";
                pro.IsTower = true; pro.NofFlat = 0; pro.PStatus = "Active"; pro.RecordStatus = 0;
                context.tblSProperties.Add(pro);
                context.SaveChanges();
                return new HttpResponseMessage(HttpStatusCode.Created);
            }
        }

        public HttpResponseMessage SavePropertySize(PropertySizeModel protype)
        {
            using (dbSBPEntities2 context = new dbSBPEntities2())
            {
                Mapper.CreateMap<PropertySizeModel, tblSPropertySize>();
                var pro = Mapper.Map<PropertySizeModel, tblSPropertySize>(protype);
                context.tblSPropertySizes.Add(pro);
                context.SaveChanges();
                return new HttpResponseMessage(HttpStatusCode.Created);
            }
        }

        public HttpResponseMessage AddNewPropertyType(PropertyTypeModel protype)
        {
            using (dbSBPEntities2 context = new dbSBPEntities2())
            {
                Mapper.CreateMap<PropertyTypeModel, tblSPropertyType>();
                var pro = Mapper.Map<PropertyTypeModel, tblSPropertyType>(protype);
                context.tblSPropertyTypes.Add(pro);
                context.SaveChanges();
                return new HttpResponseMessage(HttpStatusCode.Created);
            }
        }

        public string GetPropertyByPID(string propertyid)
        {
            int pid = Convert.ToInt32(propertyid);
            dbSBPEntities2 context = new dbSBPEntities2();
            var propery = (from pr in context.tblSProperties join pt in context.tblSPropertyTypes on pr.PID equals pt.PID join ps in context.tblSPropertySizes on pt.PropertyTypeID equals ps.PropertyTypeID where pr.PID==pid select new { Pro = pr, PType = pt, PSize = ps });
            //var pro = context.tblSProperties.Where(p => p.PID == pid).FirstOrDefault();
            return Newtonsoft.Json.JsonConvert.SerializeObject(propery);
        }

        // get property size 
        public string GetPropertyByPIDSize(string propertyid, string PropertyTypeID, string PropertySizeID)
        {
            int pid = Convert.ToInt32(propertyid);
            int ptid = Convert.ToInt32(PropertyTypeID);
            int psid = Convert.ToInt32(PropertySizeID);
            dbSBPEntities2 context = new dbSBPEntities2();
            var propery = (from pr in context.tblSProperties join pt in context.tblSPropertyTypes on pr.PID equals pt.PID join ps in context.tblSPropertySizes on pt.PropertyTypeID equals ps.PropertyTypeID where pr.PID == pid && pt.PropertyTypeID == ptid && ps.PropertySizeID == psid select new { Pro = pr, PType = pt, PSize = ps });
            //var pro = context.tblSProperties.Where(p => p.PID == pid).FirstOrDefault();
            return Newtonsoft.Json.JsonConvert.SerializeObject(propery);
        }


        // Get PRoperty Types data
        public JsonResult GetPropertyTypes(int propertyid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
          //  var v= context.tblSPropertyTypes.Where(p => p.PID == propertyid).OrderBy(o => o.PropertyTypeID).ToList();
        // var v = (from s in context.tblSPropertyTypes join ee in context.tblSProperties on s.PID equals ee.PID where s.PID == propertyid select new { PropertyTypeID = s.PropertyTypeID, PType = s.PType, Size = s.Size, PropertyID = s.PID, Unit = s.Unit, ee.PName });
            return Json(new{Ptypes=(from s in context.tblSPropertyTypes join ee in context.tblSProperties on s.PID equals ee.PID where s.PID == propertyid  select new{PropertyTypeID=s.PropertyTypeID,PType=s.PType,Size=s.Size,PropertyID=s.PID,Unit=s.Unit, ee.PName})}, JsonRequestBehavior.AllowGet);
        }
        
        // Get PRoperty Size data
        public JsonResult GetPropertySizeList(int propertyid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            //  var v= context.tblSPropertyTypes.Where(p => p.PID == propertyid).OrderBy(o => o.PropertyTypeID).ToList();
            // var v = (from s in context.tblSPropertyTypes join ee in context.tblSProperties on s.PID equals ee.PID where s.PID == propertyid select new { PropertyTypeID = s.PropertyTypeID, PType = s.PType, Size = s.Size, PropertyID = s.PID, Unit = s.Unit, ee.PName });
            return Json(new { PSizes = (from s in context.tblSPropertyTypes join ee in context.tblSProperties on s.PID equals ee.PID join si in context.tblSPropertySizes on  s.PropertyTypeID equals si.PropertyTypeID  where s.PID == propertyid select new { PropertyTypeID = s.PropertyTypeID, PropertySizeID=si.PropertySizeID, PType = s.PType, Size = si.Size+" "+si.Unit, PropertyID = s.PID, Unit = si.Unit, PropertyName=ee.PName }) }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPropertySizeListByPTypeID(int propertyid)
        {
            dbSBPEntities2 context = new dbSBPEntities2();
            //  var v= context.tblSPropertyTypes.Where(p => p.PID == propertyid).OrderBy(o => o.PropertyTypeID).ToList();
            // var v = (from s in context.tblSPropertyTypes join ee in context.tblSProperties on s.PID equals ee.PID where s.PID == propertyid select new { PropertyTypeID = s.PropertyTypeID, PType = s.PType, Size = s.Size, PropertyID = s.PID, Unit = s.Unit, ee.PName });
            return Json(new { PSizes = (from s in context.tblSPropertyTypes join ee in context.tblSProperties on s.PID equals ee.PID join si in context.tblSPropertySizes on s.PropertyTypeID equals si.PropertyTypeID where si.PropertyTypeID == propertyid select new { PropertyTypeID = s.PropertyTypeID, PropertySizeID = si.PropertySizeID, PType = s.PType, Size = si.Size + " " + si.Unit, PropertyID = s.PID, Unit = si.Unit, PropertyName = ee.PName }) }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        protected void SaveProperty(string PName)
        {
            Hashtable htProperty = new Hashtable();
            htProperty.Add("PName", PName);
            if (obj.ExecuteProcedure("Insert_Property", htProperty))
            {
            }
        }
    }
}