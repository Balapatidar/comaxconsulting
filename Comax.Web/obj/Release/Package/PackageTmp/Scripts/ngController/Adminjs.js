﻿var myApp = angular.module('PropertyApp', []);
myApp.controller('PropertyController', function ($scope, $http) {
    $('#loading').show();
    $scope.AddProperty = function () {

        var vli = ValidateAddProperty();
        if (vli == false) {
            $('#myModal').modal('show');
        }
        else {
            if (typeof $scope.property != "undefined") {
            }
            $("#loading").show();
            $scope.property.PossessionDate = $("#PossessionDate").val();
            $http({
                method: 'POST',
                url: '/Admin/Property/AddNewProperty',
                data: JSON.stringify($scope.property),
                headers: { 'Content-Type': 'application/JSON' }
            }).success(function (data) {
                $scope.status = "The Property Saved Successfully!!!";
                alert("Property has been saved successfully");
                $("#loading").hide();
            }).error(function (error) {
                //Showing error message
                $scope.status = 'Unable to save property: ' + error.message;
                $("#loading").hide();
            });
        }
    }
    $http.get('/Admin/Security/GetPropertyList/').success(function (response) { $scope.Properties = response; });
    //$http.get('/flat/GetPropertyList/').success(function (response) { $scope.Properties = response; });
    $scope.PropertyChange = function () {
        $("#loading").show();
        $http({
            method: 'Get',
            url: '/Admin/Property/GetPropertyTypes',
            params: { propertyid: $scope.protype.PID }
        }).success(function (data, status, headers, config) {
            $scope.PropertyTypes = data;
            $http({
                method: 'Get',
                url: '/Admin/Property/GetPropertySizeList',
                params: { propertyid: $scope.protype.PID }
            }).success(function (data, status, headers, config) {
                $scope.PropertySizes = data;
                $("#loading").hide();
            });
        });
    }
    $scope.AddPropertyType = function () {
        var vli = ValidateAddPropertyType();
        if (vli == false) {
            $('#myModal').modal('show');
        }
        else {
            $("#loading").show();

            $http({
                method: 'POST',
                url: '/Admin/Property/AddNewPropertyType',
                data: JSON.stringify($scope.protype),
                headers: { 'Content-Type': 'application/JSON' }
            }).success(function (data) {
                $scope.status = "The Property type Saved Successfully!!!";

                $http({
                    method: 'Get',
                    url: '/Admin/Property/GetPropertyTypes',
                    params: { propertyid: $scope.protype.PID }
                }).success(function (data, status, headers, config) {
                    $scope.PropertyTypes = data;
                });
                $("#loading").hide();
                alert("Property Type has been saved successfully");
            }).error(function (error) {
                //Showing error message
                $("#loading").hide();
                $scope.status = 'Unable to save property type: ' + error.message;
            });
        }
    }
    $scope.AddPropertySize = function () {
        var vli = ValidateAddPropertySize();
        if (vli == false) {
            $('#myModal').modal('show');
        }
        else {
            $("#loading").show();

            $http({
                method: 'POST',
                url: '/Admin/Property/SavePropertySize',
                data: JSON.stringify($scope.protype),
                headers: { 'Content-Type': 'application/JSON' }
            }).success(function (data) { //Showing success message $scope.status = "The Person Saved Successfully!!!";
                //Updating persons Model
                $("#loading").hide();

                alert("Property Size has been saved successfully");
                // GetSaleDetailByFlatId();


            }).error(function (error) {
                $("#loading").hide();
                //Showing error message
                $scope.status = 'Unable to save Customer: ' + error.message;
            });
        }
    }

    $scope.SearchProperty=function()
    {
        $scope.Error = "";

        var PID = $("#PropertyID").val();
        $("#loading").show();

        $http({
            method: 'Get',
            url: '/Admin/Property/SearchProjectProperty',
            params: {pid: PID }
        }).success(function (data, status, headers, config) {
            $scope.MyProperties = data;
            $("#loading").hide();

        });
    }
    // Change Property Name and fill propertyType.
    $scope.PropertyName = function () {
        var PID = $("#PropertyID").find(":selected").val();

        $http({
            method: 'Get',
            url: '/flat/GetPropertyTypeList/',
            params: { pid: PID }
        }).success(function (data, status, headers, config) {
            $scope.PropertyTypes = data;
        });
        //$http.get('/flat/GetPropertyTypeList/').success(function (response) { $scope.PropertyTypes = response; });
    }
    function ValidateAddProperty() {
        var vl = true;
        var message = "";
        if ($("#CompanyName").val() == "") {
            vl = false;
            message += "Insert Company Name. <br/>";
        }
        if ($("#OfficeAddress").val() == "") {
            vl = false;
            message += "Insert Office Address. <br/>";
        }
        if ($("#PName").val() == "") {
            vl = false;
            message += "Insert Property Name.<br/>";
        }

        if ($("#NofBlock").val() == "") {
            vl = false;
            message += "Please insert no of blocks in property.<br/>";
        }
        if ($("#ReceiptPrefix").val() == "") {
            vl = false;
            message += "Please insert Property Prefix for Receipt Generation.<br/>";
        }
        if ($("#Address").val() == "") {
            vl = false;
            message += "Please insert Address of property.<br/>";
        }
        if ($("#Location").val() == "") {
            vl = false;
            message += "Please insert Location of property.<br/>";
        }
        
        //if ($("#Jurisdiction").val() == "") {
        //    vl = false;
        //    message += "Please insert Amount in words.";
        //}
        //if ($("#execution").val() == "") {
        //    vl = false;
        //    message += "Please insert Total Amount. <br/>";
        //}
        //if ($("#Village").val() == "") {
        //    vl = false;
        //    message += "Please insert Total Amount. <br/>";
        //}
        //if ($("#Tehsil").val() == "") {
        //    vl = false;
        //    message += "Please insert Total Amount. <br/>";
        //}
        //if ($("#District").val() == "") {
        //    vl = false;
        //    message += "Please insert Total Amount. <br/>";
        //}
        if ($("#PossessionDate").val() == "") {
            vl = false;
            message += "Please insert Possession Date. <br/>";
        }
        $("#ErrorMessage").html(message);
        return vl;
    }
    function ValidateAddPropertyType() {
        var vl = true;
        var message = "";
    
        if ($('#PID :selected').text() == "") {
            vl = false;
            message += "Select Property Name. <br/>";
        }
       
        if ($("#PType").val() == "") {
            vl = false;
            message += "Please insert Property Type.<br/>";
        }

       
       
        $("#ErrorMessage").html(message);
        return vl;
    }
    function ValidateAddPropertySize() {
        var vl = true;
        var message = "";
        if ($('#PID :selected').text() == "") {
            vl = false;
            message += "Select Property Name. <br/>";
        }

        if ($('#PropertyTypeID :selected').text() == "") {
            vl = false;
            message += "Please Select Property Type.<br/>";
        }

        if ($("#Size").val() == "") {
            vl = false;
            message += "Please insert Property Size.<br/>";
        }
        if ($('#Unit :selected').text() == "") {
            vl = false;
            message += "Please Select Unit of size.<br/>";
        }
        $("#ErrorMessage").html(message);
        return vl;
    }
    $('#loading').hide();
});