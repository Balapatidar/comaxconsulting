﻿
var myApp = angular.module('CredoApp', []);
//Defining a Controller 
myApp.controller('FlatController', function ($scope, $http) {
 
    $http({ method: 'GET', url: '/flat/getproperty' }).success(function (data) {
        debugger;
        $scope.properties = data;
    });
    
    $scope.getCustomer = function () {
        var PID = $scope.property;
        debugger;
        if (PID) {
            $http({
                method: 'POST',
                url: '/flat/GetCustomerByPropertyId/',
                data: JSON.stringify({ PID: PID })
            }).success(function (data, status, headers, config) {
                $scope.customer = data;
            }).error(function (data, status, headers, config) {
                $scope.message = 'Unexpected Error';
            });
        }
        else {
            $scope.states = null;
        }
    }
    $('#loading').hide();
});