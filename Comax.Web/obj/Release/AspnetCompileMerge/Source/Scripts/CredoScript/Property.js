﻿var myApp = angular.module('CredoApp', []);
//Defining a Controller 
myApp.controller('PropertyController', function ($scope, $http) {
    $('#loading').show();
    $scope.AddProperty = function () {
        //Defining $http service for creating a person

        if (typeof $scope.property != "undefined") {
            $scope.property.PName = $("#PName").val();
        }

        $http({
            method: 'POST',
            url: '/Property/SaveProperty',
            data: JSON.stringify($scope.property),
            headers: { 'Content-Type': 'application/JSON' }
        }).success(function (data) { //Showing success message $scope.status = "The Person Saved Successfully!!!";
            //Updating persons Model
            alert("Property has been saved successfully");
            // GetSaleDetailByFlatId();

        }).error(function (error) {
            //Showing error message
            $scope.status = 'Unable to save Customer: ' + error.message;
        });

    }

    $scope.AddPropertySize=function()
    {
        $http({
            method: 'POST',
            url: '/Property/SavePropertySize',
            data: JSON.stringify($scope.property),
            headers: { 'Content-Type': 'application/JSON' }
        }).success(function (data) { //Showing success message $scope.status = "The Person Saved Successfully!!!";
            //Updating persons Model
            alert("Property Size has been saved successfully");
            // GetSaleDetailByFlatId();

        }).error(function (error) {
            //Showing error message
            $scope.status = 'Unable to save Customer: ' + error.message;
        });
    }
    
    //// Get all Property list for dropdownlist
    $http.get('/flat/GetPropertyList/').success(function (response) { $scope.Properties = response; });
    $('#loading').hide();
});