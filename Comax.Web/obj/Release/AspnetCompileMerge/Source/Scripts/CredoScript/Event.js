﻿var myApp = angular.module('CredoApp', []);
//Defining a Controller 


myApp.controller('InstallmentController', function ($scope, $http) {
    // Get all Property list for dropdownlist
    $scope.Error = "";

    $http.get('/flat/GetPropertyList/').success(function (response) { $scope.Properties = response; });

    $scope.SearchFlat = function () {
        $('#loading').show();
        $scope.Error = "";
        var FName = $("#FlatID").val();// $scope.Flat.flatname;
        var PID = $("#PropertyID").find(":selected").val();
        $http({
            method: 'Get',
            url: '/Customer/Installment/GetFlatList',
            params: { flatname: FName, pid: PID }
        }).success(function (data, status, headers, config) {
            $scope.Flat = data;
            if (data.Result.length == 0) {
                $scope.Error = "Property flat not found.";
                return "";
                $("#divinfo").hide();
            }
            $("#divinfo").show();

            var FID = $scope.Flat.Result[0].FlatID;
           $scope.Flat.flatname=FName;
            $http({
                method: 'Get',
                url: '/Customer/Installment/GetFlatSale',
                params: { flatid: FID }
            }).success(function (data, status, headers, config) {
                $scope.Sale = data;
                if (data.Sale[0].SaleDate != null) {
                    var ddate = new Date(parseInt(data.Sale[0].SaleDate.substr(6)));// value.DueDate;
                    var mth = ddate.getMonth() + 1;
                    if (mth < 10) mth = "0" + mth;
                    $scope.SaleDate = ddate.getDate() + "/" + mth + "/" + ddate.getFullYear();
                }
                //if ($scope.Sale.Sale[0].PlanID == 2) {

                $http({  // 
                    method: 'Get',
                    url: '/Customer/Installment/GetPayment',
                    params: { saleid: $scope.Sale.Sale[0].SaleID }
                }).success(function (data, status, headers, config) {
                    $scope.Payment = data;
                    // Get total receive amount
                    $http({
                        method: 'Get',
                        url: '/Customer/Installment/GetTotalPayment',
                        params: { saleid: $scope.Sale.Sale[0].SaleID }
                    }).success(function (data, status, headers, config) {
                        $scope.Amount = data;
                        $http({
                            method: 'Get',
                            url: '/Customer/Installment/GetInstallment',
                            params: { saleid: $scope.Sale.Sale[0].SaleID }
                        }).success(function (response) {
                            $scope.Install = response;
                            $('#loading').hide();
                        })
                    })
                })
                //}
                //else {
                //    alert("Property plan not have construction type.");
                //    $('#loading').hide();
                //}
               
            })
        }).then(function () {
            $('#loading').hide();
        });

    }

    $scope.SearchFlat123 = function (installid, Mdate) {
        $.ajax({
            method: 'POST',
            url: '/Customer/Installment/GetInstallmentByInstallId',
            data: {InstallmentID:installid,ModifyDate: Mdate},
        }).success(function (result) {
                //  window.location.href = urlPath + '/';
                alert(result);
            }).error(function (err) {
                alert(err.responseText);
                if (err.responseText == "success") {
                } else {

                }
        })
    }


    GetSaleDetailByFlatId = function () {
        var flatId = $scope.newCust.FlatID;
        debugger;
        if (flatId) {
            $http({
                method: 'POST',
                url: '/flat/GetSaleDetailByFlatId/',
                data: JSON.stringify({ flatId: flatId })
            }).success(function (data, status, headers, config) {
                debugger;
                $scope.Sale = data;
            }).error(function (data, status, headers, config) {
                debugger;
                $scope.message = 'Unexpected Error';
            });
        }
        else {
            $scope.Sale = null;
        }
        document.getElementById("stepid").value = 1;
        if (flatId != '' || flatId != null) {
            document.getElementById("flatid").value = flatId;
        }
    }



    $scope.GetUpdateInstallment = function () {
        var InstallmentID = $scope.InstallmentID;
        var ModifiedDate = $scope.ins.ModifyDate;
        if (InstallmentID) {
            $http({
                method: 'POST',
                url: '/Customer/Installment/GetInstallmentByInsallId',
                data: JSON.stringify({ InstallmentID: InstallmentID, ModifiedDate: ModifiedDate })
            }).success(function (data, status, headers, config) {
                alert("Customer has been saved successfully");
            })
        }
    }
    $('#loading').hide();
});